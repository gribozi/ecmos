# ************************************************************
# Sequel Pro SQL dump
# Версия 4541
#
# http://www.sequelpro.com/
# https://github.com/sequelpro/sequelpro
#
# Адрес: 127.0.0.1 (MySQL 5.7.19)
# Схема: ecmos
# Время создания: 2018-02-02 22:38:09 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Дамп таблицы contact
# ------------------------------------------------------------

DROP TABLE IF EXISTS `contact`;

CREATE TABLE `contact` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `adresslng` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT '',
  `adressltg` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT '',
  `email_to` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `smfb` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT '',
  `smin` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT '',
  `smvk` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT '',
  `smyt` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT '',
  `active` bit(1) NOT NULL,
  `ordering` int(4) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `contact` WRITE;
/*!40000 ALTER TABLE `contact` DISABLE KEYS */;

INSERT INTO `contact` (`id`, `adresslng`, `adressltg`, `email_to`, `phone`, `smfb`, `smin`, `smvk`, `smyt`, `active`, `ordering`)
VALUES
	(1,'30.742149353027344','46.4378568950242','info@ecmos.com.ua','+38 096 053 03 06','http://fb.com','http://fb.com','http://vk.com','http://yt.com',b'1',0);

/*!40000 ALTER TABLE `contact` ENABLE KEYS */;
UNLOCK TABLES;


# Дамп таблицы contact_content
# ------------------------------------------------------------

DROP TABLE IF EXISTS `contact_content`;

CREATE TABLE `contact_content` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `info_address` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT '',
  `info_title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT '',
  `info_working` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT '',
  `language_id` int(11) NOT NULL,
  `contact_id` int(11) NOT NULL,
  `info_phones` text COLLATE utf8mb4_unicode_ci,
  PRIMARY KEY (`id`),
  KEY `FKbr1or6lcfkl22o0m5nre39u3h` (`language_id`),
  KEY `FKg2bri0xk2948pu5i58xvidl3u` (`contact_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `contact_content` WRITE;
/*!40000 ALTER TABLE `contact_content` DISABLE KEYS */;

INSERT INTO `contact_content` (`id`, `info_address`, `info_title`, `info_working`, `language_id`, `contact_id`, `info_phones`)
VALUES
	(1,'Украина, Одесская областьс. Еремеевка, ул. Молодежная','ЭКМОС ГРУПП','Пн - Пт 10.00 - 18.00',1,1,'Отдел снабжения\n048 799 66 21\n094 949 96 21\n\nОтдел реализации\n048 783 72 50 \n073 480 83 08');

/*!40000 ALTER TABLE `contact_content` ENABLE KEYS */;
UNLOCK TABLES;


# Дамп таблицы facility
# ------------------------------------------------------------

DROP TABLE IF EXISTS `facility`;

CREATE TABLE `facility` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `active` bit(1) NOT NULL,
  `ordering` int(4) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `facility` WRITE;
/*!40000 ALTER TABLE `facility` DISABLE KEYS */;

INSERT INTO `facility` (`id`, `active`, `ordering`)
VALUES
	(1,b'1',0);

/*!40000 ALTER TABLE `facility` ENABLE KEYS */;
UNLOCK TABLES;


# Дамп таблицы facility_content
# ------------------------------------------------------------

DROP TABLE IF EXISTS `facility_content`;

CREATE TABLE `facility_content` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `text_big` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT '',
  `text_small` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT '',
  `language_id` int(11) NOT NULL,
  `facility_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FKeaygtkctuj5acypioerk4p1ht` (`language_id`),
  KEY `FKoxgnn8y6vs8dut2hr0erhr1c3` (`facility_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `facility_content` WRITE;
/*!40000 ALTER TABLE `facility_content` DISABLE KEYS */;

INSERT INTO `facility_content` (`id`, `text_big`, `text_small`, `language_id`, `facility_id`)
VALUES
	(1,'600 КВ.М²','СВОЁ ПРОИЗВОДСТВО',1,1);

/*!40000 ALTER TABLE `facility_content` ENABLE KEYS */;
UNLOCK TABLES;


# Дамп таблицы faq
# ------------------------------------------------------------

DROP TABLE IF EXISTS `faq`;

CREATE TABLE `faq` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `active` bit(1) NOT NULL,
  `ordering` int(4) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

LOCK TABLES `faq` WRITE;
/*!40000 ALTER TABLE `faq` DISABLE KEYS */;

INSERT INTO `faq` (`id`, `active`, `ordering`)
VALUES
	(1,b'1',1),
	(2,b'1',2),
	(3,b'1',3),
	(4,b'1',4),
	(5,b'1',5),
	(6,b'1',6);

/*!40000 ALTER TABLE `faq` ENABLE KEYS */;
UNLOCK TABLES;


# Дамп таблицы faq_content
# ------------------------------------------------------------

DROP TABLE IF EXISTS `faq_content`;

CREATE TABLE `faq_content` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `answer` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `question` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `language_id` int(11) NOT NULL,
  `faq_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK1tl9bcjevhed6jisr5gheu1gx` (`language_id`),
  KEY `FKkl7nogorg441foxv6plc77di6` (`faq_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `faq_content` WRITE;
/*!40000 ALTER TABLE `faq_content` DISABLE KEYS */;

INSERT INTO `faq_content` (`id`, `answer`, `question`, `language_id`, `faq_id`)
VALUES
	(1,'Качество строения и последующие эксплуатационные расходы в большой степени зависят от качества используемых материалов. Поэтому в производственном и строительном процессе мы используем только качественные, экологически чистые и безопасные материалы.','ЭНЕРГОЭФФЕКТИВНОСТЬ И ЗВУКОИЗОЛЯЦИЯ',1,1),
	(2,'Качество строения и последующие эксплуатационные расходы в большой степени зависят от качества используемых материалов. Поэтому в производственном и строительном процессе мы используем только качественные, экологически чистые и безопасные материалы.','ЭНЕРГОЭФФЕКТИВНОСТЬ И ЗВУКОИЗОЛЯЦИЯ',1,2),
	(3,'Качество строения и последующие эксплуатационные расходы в большой степени зависят от качества используемых материалов. Поэтому в производственном и строительном процессе мы используем только качественные, экологически чистые и безопасные материалы.','ПРОЧНОСТЬ И ОГНЕСТОЙКОСТЬ',1,3),
	(4,'Качество строения и последующие эксплуатационные расходы в большой степени зависят от качества используемых материалов. Поэтому в производственном и строительном процессе мы используем только качественные, экологически чистые и безопасные материалы.','ЭНЕРГОЭФФЕКТИВНОСТЬ И ЗВУКОИЗОЛЯЦИЯ',1,4),
	(5,'Качество строения и последующие эксплуатационные расходы в большой степени зависят от качества используемых материалов. Поэтому в производственном и строительном процессе мы используем только качественные, экологически чистые и безопасные материалы.','КОНКУРЕНТНОЕ СООТНОШЕНИЕ ЦЕНЫ И КАЧЕСТВА',1,5),
	(6,'Качество строения и последующие эксплуатационные расходы в большой степени зависят от качества используемых материалов. Поэтому в производственном и строительном процессе мы используем только качественные, экологически чистые и безопасные материалы.','КОНКУРЕНТНОЕ СООТНОШЕНИЕ ЦЕНЫ И КАЧЕСТВА',1,6);

/*!40000 ALTER TABLE `faq_content` ENABLE KEYS */;
UNLOCK TABLES;


# Дамп таблицы for_investors
# ------------------------------------------------------------

DROP TABLE IF EXISTS `for_investors`;

CREATE TABLE `for_investors` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `active` bit(1) NOT NULL,
  `ordering` int(4) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

LOCK TABLES `for_investors` WRITE;
/*!40000 ALTER TABLE `for_investors` DISABLE KEYS */;

INSERT INTO `for_investors` (`id`, `active`, `ordering`)
VALUES
	(1,b'1',0);

/*!40000 ALTER TABLE `for_investors` ENABLE KEYS */;
UNLOCK TABLES;


# Дамп таблицы for_investors_content
# ------------------------------------------------------------

DROP TABLE IF EXISTS `for_investors_content`;

CREATE TABLE `for_investors_content` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `text` text,
  `language_id` int(11) NOT NULL,
  `for_investors_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FKef3nspp49ynt6k8j6gwd6u7v5` (`language_id`),
  KEY `FKpquofktmaggvb4vsu9cb1hi9x` (`for_investors_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

LOCK TABLES `for_investors_content` WRITE;
/*!40000 ALTER TABLE `for_investors_content` DISABLE KEYS */;

INSERT INTO `for_investors_content` (`id`, `text`, `language_id`, `for_investors_id`)
VALUES
	(1,'<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla quam velit, vulputate eu pharetra nec, mattis ac neque. Duis vulputate commodo lectus, ac blandit elit tincidunt id. Sed rhoncus, tortor sed eleifend tristique, tortor mauris molestie elit, et lacinia ipsum quam nec dui. Quisque nec mauris sit amet elit iaculis pretium sit amet quis magna. Aenean velit odio, elementum in tempus ut, vehicula eu diam. Pellentesque rhoncus aliquam mattis. Ut vulputate eros sed felis sodales nec vulputate justo hendrerit. Vivamus varius pretium ligula, a aliquam odio euismod sit amet. Quisque laoreet sem sit amet orci ullamcorper at ultricies metus viverra. Pellentesque arcu mauris, malesuada quis ornare accumsan, blandit sed diam.</p>\n\n<h4>Lorem ipsum dolor</h4>\n<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla quam velit, vulputate eu pharetra nec, mattis ac neque. Duis vulputate commodo lectus, ac blandit elit tincidunt id. Sed rhoncus, tortor sed eleifend tristique, tortor mauris molestie elit, et lacinia ipsum quam nec dui. Quisque nec mauris sit amet elit iaculis pretium sit amet quis magna. Aenean velit odio, elementum in tempus ut, vehicula eu diam. Pellentesque rhoncus aliquam mattis. Ut vulputate eros sed felis sodales nec vulputate justo hendrerit. Vivamus varius pretium ligula, a aliquam odio euismod sit amet. Quisque laoreet sem sit amet orci ullamcorper at ultricies metus viverra. Pellentesque arcu mauris, malesuada quis ornare accumsan, blandit sed diam.</p>',1,1);

/*!40000 ALTER TABLE `for_investors_content` ENABLE KEYS */;
UNLOCK TABLES;


# Дамп таблицы for_teamtext
# ------------------------------------------------------------

DROP TABLE IF EXISTS `for_teamtext`;

CREATE TABLE `for_teamtext` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `active` bit(1) NOT NULL,
  `ordering` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

LOCK TABLES `for_teamtext` WRITE;
/*!40000 ALTER TABLE `for_teamtext` DISABLE KEYS */;

INSERT INTO `for_teamtext` (`id`, `active`, `ordering`)
VALUES
	(1,b'1',1);

/*!40000 ALTER TABLE `for_teamtext` ENABLE KEYS */;
UNLOCK TABLES;


# Дамп таблицы for_teamtext_content
# ------------------------------------------------------------

DROP TABLE IF EXISTS `for_teamtext_content`;

CREATE TABLE `for_teamtext_content` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `text` text,
  `language_id` int(11) NOT NULL,
  `for_teamtext_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FKrsfh5jcou12ljmntpgugbr202` (`language_id`),
  KEY `FKsrm3042kc40qvi57mdcyohknm` (`for_teamtext_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

LOCK TABLES `for_teamtext_content` WRITE;
/*!40000 ALTER TABLE `for_teamtext_content` DISABLE KEYS */;

INSERT INTO `for_teamtext_content` (`id`, `text`, `language_id`, `for_teamtext_id`)
VALUES
	(1,'Применяемый метод строительства является эффективным и перспективным на рынке недвижимости, обеспечивает соблюдение строительных требований и стандартов. Особенностью проекта является то, что модули строятся из произведенных на заводе объемных элементов. Объемный элемент – это объемная часть строения, в которой пол, стены и потолок смонтированы между собой уже на заводе.',1,1);

/*!40000 ALTER TABLE `for_teamtext_content` ENABLE KEYS */;
UNLOCK TABLES;


# Дамп таблицы formrequest
# ------------------------------------------------------------

DROP TABLE IF EXISTS `formrequest`;

CREATE TABLE `formrequest` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `phone` varchar(255) DEFAULT NULL,
  `request_date` date DEFAULT NULL,
  `text` varchar(255) DEFAULT NULL,
  `form` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

LOCK TABLES `formrequest` WRITE;
/*!40000 ALTER TABLE `formrequest` DISABLE KEYS */;

INSERT INTO `formrequest` (`id`, `email`, `name`, `phone`, `request_date`, `text`, `form`)
VALUES
	(1,NULL,'123','qwe','2017-12-05',NULL,NULL),
	(2,NULL,'345','cvb','2017-12-05',NULL,NULL),
	(3,NULL,'567','tyu','2017-12-05',NULL,NULL),
	(4,'ghjghj',NULL,NULL,'2017-12-05','ghjghjghjghj',NULL),
	(5,NULL,'','','2017-12-05',NULL,NULL),
	(6,NULL,'dfg','dfg','2017-12-05',NULL,NULL),
	(7,NULL,'asd','123','2017-12-05',NULL,'form-header'),
	(8,NULL,'qwe','567','2017-12-05',NULL,'form-build'),
	(9,'tyu',NULL,NULL,'2017-12-05','rtyrty','form-footer'),
	(10,NULL,'цук','цук','2017-12-13',NULL,'form-header'),
	(11,NULL,'','','2017-12-14',NULL,'form-object'),
	(12,NULL,'','','2017-12-14',NULL,'form-object'),
	(13,'','ываыва','234234234','2017-12-25','ываыва 234234234','получить расчет- к. Базовая'),
	(14,'','уцкцукц','324234','2017-12-25','уцкцукц 324234','скачать все прайсы'),
	(15,'','ert','345','2017-12-27','ert 345','скачать все прайсы');

/*!40000 ALTER TABLE `formrequest` ENABLE KEYS */;
UNLOCK TABLES;


# Дамп таблицы language
# ------------------------------------------------------------

DROP TABLE IF EXISTS `language`;

CREATE TABLE `language` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `interpritation` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `active` bit(1) NOT NULL,
  `ordering` int(4) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `language` WRITE;
/*!40000 ALTER TABLE `language` DISABLE KEYS */;

INSERT INTO `language` (`id`, `interpritation`, `name`, `active`, `ordering`)
VALUES
	(1,'по-русски','ru',b'1',1),
	(2,'українською','ua',b'1',2),
	(3,'in English','en',b'1',3);

/*!40000 ALTER TABLE `language` ENABLE KEYS */;
UNLOCK TABLES;


# Дамп таблицы object_type
# ------------------------------------------------------------

DROP TABLE IF EXISTS `object_type`;

CREATE TABLE `object_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `active` bit(1) NOT NULL,
  `uri` varchar(255) NOT NULL,
  `ordering` int(4) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

LOCK TABLES `object_type` WRITE;
/*!40000 ALTER TABLE `object_type` DISABLE KEYS */;

INSERT INTO `object_type` (`id`, `active`, `uri`, `ordering`)
VALUES
	(1,b'1','inns',1),
	(2,b'1','houses',2),
	(3,b'1','complexes',3),
	(4,b'1','recreations',4),
	(5,b'1','hotels',5),
	(6,b'1','buildings',6);

/*!40000 ALTER TABLE `object_type` ENABLE KEYS */;
UNLOCK TABLES;


# Дамп таблицы object_type_content
# ------------------------------------------------------------

DROP TABLE IF EXISTS `object_type_content`;

CREATE TABLE `object_type_content` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `language_id` int(11) NOT NULL,
  `object_type_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK4hhl2j71iiio3pe0cpqmrjvim` (`language_id`),
  KEY `FKnmghis2jdwsheaca0k696qo45` (`object_type_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

LOCK TABLES `object_type_content` WRITE;
/*!40000 ALTER TABLE `object_type_content` DISABLE KEYS */;

INSERT INTO `object_type_content` (`id`, `name`, `language_id`, `object_type_id`)
VALUES
	(1,'Гостиницы',1,1),
	(2,'Малоквартирные дома',1,2),
	(3,'Комплексы',1,3),
	(4,'Базы отдыха',1,4),
	(5,'Отели',1,5),
	(6,'Малоэтажные дома',1,6);

/*!40000 ALTER TABLE `object_type_content` ENABLE KEYS */;
UNLOCK TABLES;


# Дамп таблицы page
# ------------------------------------------------------------

DROP TABLE IF EXISTS `page`;

CREATE TABLE `page` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `active` bit(1) NOT NULL,
  `uri` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `ordering` int(4) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `page` WRITE;
/*!40000 ALTER TABLE `page` DISABLE KEYS */;

INSERT INTO `page` (`id`, `active`, `uri`, `ordering`)
VALUES
	(1,b'1','projects',2),
	(2,b'1','prices',3),
	(3,b'1','team',4),
	(4,b'1','objects',5),
	(5,b'1','realtors',6),
	(6,b'1','forinvestors',7),
	(7,b'1','partners',8),
	(8,b'1','contacts',9),
	(9,b'0','index',1);

/*!40000 ALTER TABLE `page` ENABLE KEYS */;
UNLOCK TABLES;


# Дамп таблицы page_content
# ------------------------------------------------------------

DROP TABLE IF EXISTS `page_content`;

CREATE TABLE `page_content` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `content` text COLLATE utf8_unicode_ci,
  `description` text COLLATE utf8_unicode_ci,
  `h1` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `keywords` text COLLATE utf8_unicode_ci,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `language_id` int(11) NOT NULL,
  `page_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FKpl1s48myb3baorwetviq41qog` (`language_id`),
  KEY `FKq5j6xuuhmye050gv8rxi3kys3` (`page_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `page_content` WRITE;
/*!40000 ALTER TABLE `page_content` DISABLE KEYS */;

INSERT INTO `page_content` (`id`, `content`, `description`, `h1`, `keywords`, `name`, `title`, `language_id`, `page_id`)
VALUES
	(1,'    <section class=\"projects\">\n        <div class=\"container\">\n            <div class=\"row\">\n                <form class=\"proj-filter\">\n                    <div class=\"col-sm-4\">\n                        <p>Метраж, м2</p>\n                        <div class=\"form-group\">\n                            <input type=\"text\" id=\"range-slider\" value=\"\" />\n                        </div>\n                    </div>\n                    <div class=\"col-sm-4\">\n                        <p>Этажность</p>\n                        <div class=\"form-group\">\n                            <select>\n                                <option selected disabled>Кол-во этажей</option>\n                                <option>1</option>\n                                <option>2</option>\n                                <option>3</option>\n                            </select>\n                            <i></i>\n                        </div>\n                    </div>\n                    <div class=\"col-sm-4\">\n                        <p class=\"hidden-xs\">&nbsp;</p>\n                        <div class=\"form-group\">\n                            <button type=\"button\" class=\"btn btn-filled-o btn-sm-h btn-full\">показать</button>\n                        </div>\n                    </div>\n                </form>\n            </div>\n            <div class=\"row text-center\">\n                <div class=\"col-sm-4\">\n                    <a href=\"#\" class=\"obj-item\">\n                        <img src=\"images/obj-item-1.jpg\" alt=\"house\">\n                        <h4>Дом Модульный</h4>\n                        <p>Внешние размеры – 4 х 5</p>\n                        <p>Жилая площадь – 220 м2</p>\n                        <p class=\"btn btn-sm-h\">ПОДРОБНЕЕ</p>\n                    </a>\n                </div>\n                <div class=\"col-sm-4\">\n                    <a href=\"#\" class=\"obj-item\">\n                        <img src=\"images/obj-item-1.jpg\" alt=\"house\">\n                        <h4>Дом Модульный</h4>\n                        <p>Внешние размеры – 4 х 5</p>\n                        <p>Жилая площадь – 220 м2</p>\n                        <p class=\"btn btn-sm-h\">ПОДРОБНЕЕ</p>\n                    </a>\n                </div>\n                <div class=\"col-sm-4\">\n                    <a href=\"#\" class=\"obj-item\">\n                        <img src=\"images/obj-item-1.jpg\" alt=\"house\">\n                        <h4>Дом Модульный</h4>\n                        <p>Внешние размеры – 4 х 5</p>\n                        <p>Жилая площадь – 220 м2</p>\n                        <p class=\"btn btn-sm-h\">ПОДРОБНЕЕ</p>\n                    </a>\n                </div>\n            </div>\n            <div class=\"row text-center\">\n                <div class=\"col-sm-4\">\n                    <a href=\"#\" class=\"obj-item\">\n                        <img src=\"images/obj-item-1.jpg\" alt=\"house\">\n                        <h4>Дом Модульный</h4>\n                        <p>Внешние размеры – 4 х 5</p>\n                        <p>Жилая площадь – 220 м2</p>\n                        <p class=\"btn btn-sm-h\">ПОДРОБНЕЕ</p>\n                    </a>\n                </div>\n                <div class=\"col-sm-4\">\n                    <a href=\"#\" class=\"obj-item\">\n                        <img src=\"images/obj-item-1.jpg\" alt=\"house\">\n                        <h4>Дом Модульный</h4>\n                        <p>Внешние размеры – 4 х 5</p>\n                        <p>Жилая площадь – 220 м2</p>\n                        <p class=\"btn btn-sm-h\">ПОДРОБНЕЕ</p>\n                    </a>\n                </div>\n                <div class=\"col-sm-4\">\n                    <a href=\"#\" class=\"obj-item\">\n                        <img src=\"images/obj-item-1.jpg\" alt=\"house\">\n                        <h4>Дом Модульный</h4>\n                        <p>Внешние размеры – 4 х 5</p>\n                        <p>Жилая площадь – 220 м2</p>\n                        <p class=\"btn btn-sm-h\">ПОДРОБНЕЕ</p>\n                    </a>\n                </div>\n            </div>\n            <div class=\"row text-center\">\n                <div class=\"col-sm-4\">\n                    <a href=\"#\" class=\"obj-item\">\n                        <img src=\"images/obj-item-1.jpg\" alt=\"house\">\n                        <h4>Дом Модульный</h4>\n                        <p>Внешние размеры – 4 х 5</p>\n                        <p>Жилая площадь – 220 м2</p>\n                        <p class=\"btn btn-sm-h\">ПОДРОБНЕЕ</p>\n                    </a>\n                </div>\n                <div class=\"col-sm-4\">\n                    <a href=\"#\" class=\"obj-item\">\n                        <img src=\"images/obj-item-1.jpg\" alt=\"house\">\n                        <h4>Дом Модульный</h4>\n                        <p>Внешние размеры – 4 х 5</p>\n                        <p>Жилая площадь – 220 м2</p>\n                        <p class=\"btn btn-sm-h\">ПОДРОБНЕЕ</p>\n                    </a>\n                </div>\n                <div class=\"col-sm-4\">\n                    <a href=\"#\" class=\"obj-item\">\n                        <img src=\"images/obj-item-1.jpg\" alt=\"house\">\n                        <h4>Дом Модульный</h4>\n                        <p>Внешние размеры – 4 х 5</p>\n                        <p>Жилая площадь – 220 м2</p>\n                        <p class=\"btn btn-sm-h\">ПОДРОБНЕЕ</p>\n                    </a>\n                </div>\n            </div>\n            <div class=\"text-center\">\n                <button type=\"button\" class=\"btn btn-bordered-o btn-lg\">показать еще</button>\n            </div>\n        </div>\n    </section>','Типовые проекты','Типовые проекты','Типовые проекты','Проекты','Типовые проекты',1,1),
	(2,'    <section class=\"price\">\n        <div class=\"container\">\n            <div class=\"row\">\n                <div class=\"col-md-3\">\n                    <ul class=\"nav nav-tabs\">\n                        <li class=\"active\"><a href=\"#tab-1\" data-toggle=\"tab\">Многоквартирные жилые дома<span></span></a></li>\n                        <li><a href=\"#tab-2\" data-toggle=\"tab\">Гостиницы и апартаменты<span></span></a></li>\n                        <li><a href=\"#tab-3\" data-toggle=\"tab\">Индивидуальные жилые дома<span></span></a></li>\n                    </ul>\n                    <button type=\"button\" class=\"btn btn-full btn-sm btn-filled-g\" data-toggle=\"modal\" data-target=\"#modal-download\">скачать все прайсы</button>\n                </div>\n                <div class=\"col-md-9\">\n                    <div class=\"tab-content\">\n                        <div class=\"tab-pane fade in active\" id=\"tab-1\">\n                            <h4>Варианты комплектаций многоквартирных жилых домов (3-4 этажа)</h4>\n                            <div class=\"table-overflow\">\n                                <table class=\"table\">\n                                    <thead>\n                                        <tr>\n                                            <td>Стадии работ</td>\n                                            <td>Опции</td>\n                                            <td><p>Комплектация «Базовая»</p></td>\n                                            <td><p>Комплектация «Комфорт»</p></td>\n                                            <td><p>Комплектация «Люкс»</p></td>\n                                        </tr>\n                                    </thead>\n                                    <tbody>\n                                        <tr>\n                                            <td colspan=\"5\"></td>\n                                        </tr>\n                                        <tr>\n                                            <td><p>Подготовительные работы</p></td>\n                                            <td colspan=\"4\">\n                                                <table>\n                                                    <tbody>\n                                                    <tr>\n                                                        <td>Проект</td>\n                                                        <td><span class=\"plus\"></span></td>\n                                                        <td><span class=\"plus\"></span></td>\n                                                        <td><span class=\"plus\"></span></td>\n                                                    </tr>\n                                                    <tr>\n                                                        <td>Ленточный фундамент с армированием</td>\n                                                        <td><span class=\"plus\"></span></td>\n                                                        <td><span class=\"plus\"></span></td>\n                                                        <td><span class=\"plus\"></span></td>\n                                                    </tr>\n                                                    <tr>\n                                                        <td>Отвод за пределы фундамента канализационных выходов, труб водообеспечения и кабеля электроснабжения.</td>\n                                                        <td><span class=\"plus\"></span></td>\n                                                        <td><span class=\"plus\"></span></td>\n                                                        <td><span class=\"plus\"></span></td>\n                                                    </tr>\n                                                    </tbody>\n                                                </table>\n                                            </td>\n                                        </tr>\n                                        <tr>\n                                            <td colspan=\"5\">\n                                                <div></div>\n                                            </td>\n                                        </tr>\n                                        <tr>\n                                            <td><p>Наружная отделка</p></td>\n                                            <td colspan=\"4\">\n                                                <table>\n                                                    <tbody>\n                                                    <tr>\n                                                        <td>Декоративная штукатурка</td>\n                                                        <td><span class=\"plus\"></span></td>\n                                                        <td><span class=\"plus\"></span></td>\n                                                        <td><span class=\"plus\"></span></td>\n                                                    </tr>\n                                                    <tr>\n                                                        <td>Керамогранитные фасады</td>\n                                                        <td><span class=\"minus\"></span></td>\n                                                        <td><span class=\"minus\"></span></td>\n                                                        <td><span class=\"plus\"></span></td>\n                                                    </tr>\n                                                    <tr>\n                                                        <td>Навесные вентилируемые фасады</td>\n                                                        <td><span class=\"minus\"></span></td>\n                                                        <td><span class=\"minus\"></span></td>\n                                                        <td><span class=\"plus\"></span></td>\n                                                    </tr>\n                                                    <tr>\n                                                        <td>Кирпичные фасады</td>\n                                                        <td><span class=\"plus\"></span></td>\n                                                        <td><span class=\"plus\"></span></td>\n                                                        <td><span class=\"plus\"></span></td>\n                                                    </tr>\n                                                    <tr>\n                                                        <td>Деревянные фасады</td>\n                                                        <td><span class=\"plus\"></span></td>\n                                                        <td><span class=\"plus\"></span></td>\n                                                        <td><span class=\"plus\"></span></td>\n                                                    </tr>\n                                                    </tbody>\n                                                </table>\n                                            </td>\n                                        </tr>\n                                    </tbody>\n                                    <tfoot>\n                                        <tr>\n                                            <td></td>\n                                            <td>Стоимость у.е. за 1 кв.м.</td>\n                                            <td><p>от 330</p></td>\n                                            <td><p>от 420</p></td>\n                                            <td><p>от 470</p></td>\n                                        </tr>\n                                        <tr>\n                                            <td></td>\n                                            <td></td>\n                                            <td><button type=\"button\" class=\"btn btn-filled-o\" data-toggle=\"modal\" data-target=\"#modal-price\">подать заявку</button></td>\n                                            <td><button type=\"button\" class=\"btn btn-filled-o\"  data-toggle=\"modal\" data-target=\"#modal-price\">подать заявку</button></td>\n                                            <td><button type=\"button\" class=\"btn btn-filled-o\"  data-toggle=\"modal\" data-target=\"#modal-price\">подать заявку</button></td>\n                                        </tr>\n                                    </tfoot>\n                                </table>\n                            </div>\n                        </div>\n                        <div class=\"tab-pane fade\" id=\"tab-2\">..</div>\n                        <div class=\"tab-pane fade\" id=\"tab-3\">...</div>\n                    </div>\n                </div>\n            </div>\n        </div>\n    </section>','Прайсы','Прайсы','Прайсы','Прайсы','Прайсы',1,2),
	(3,'    <section class=\"team\">\n        <div class=\"build-wrapper\">\n            <div class=\"team-leader\">\n                <div>\n                    <div>\n                        <span>\n                            <img src=\"images/team-1.jpg\" alt=\"Иванов Федор Иванович\">\n                        </span>\n                    </div>\n                </div>\n                <div>\n                    <h4>Иванов Федор Иванович</h4>\n                    <h5>Директор ECMOS</h5>\n                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla quam velit, vulputate eu pharetra nec, mattis ac neque. Duis vulputate commodo lectus, ac blandit elit tincidunt id. Sed rhoncus, tortor sed eleifend tristique, tortor mauris molestie elit, et lacinia ipsum quam nec dui. Quisque nec mauris sit amet elit iaculis pretium sit</p>\n                </div>\n            </div>\n        </div>\n        <div class=\"build-wrapper\">\n            <ul class=\"build-list\">\n\n\n\n                <!--ряд 1-->\n                <li class=\"b-l-hide-mob\">\n                    <p class=\"f-o\"></p>\n                </li>\n                <li>\n                    <p>\n                        <span>\n                            <img src=\"images/team-2.jpg\" alt=\"Иванов С.А.\">\n                        </span>\n                    </p>\n                    <strong><span>Иванов С.А.</span></strong>\n                </li>\n                <li>\n                    <p>\n                        <span>\n                            <img src=\"images/team-3.jpg\" alt=\"горина О.В.\">\n                        </span>\n                    </p>\n                    <strong><span>горина О.В.</span></strong>\n                </li>\n                <li class=\"b-l-hide\"></li>\n                <li class=\"b-l-hide\">\n                    <p class=\"b-o\"></p>\n                </li>\n                <li>\n                    <p>\n                        <span>\n                            <img src=\"images/team-4.jpg\" alt=\"Стокова Л.Д.\">\n                        </span>\n                    </p>\n                    <strong><span>Стокова Л.Д.</span></strong>\n                </li>\n                <li class=\"b-l-hide-mob\">\n                    <p class=\"f-o\"></p>\n                </li>\n                <!--ряд 1-->\n\n\n\n\n\n                <!--ряд 2-->\n                <li class=\"b-l-hide-mob\"></li>\n                <li>\n                    <p>\n                        <span>\n                            <img src=\"images/team-2.jpg\" alt=\"Иванов С.А.\">\n                        </span>\n                    </p>\n                    <strong><span>Иванов С.А.</span></strong>\n                </li>\n                <li>\n                    <p>\n                        <span>\n                            <img src=\"images/team-3.jpg\" alt=\"горина О.В.\">\n                        </span>\n                    </p>\n                    <strong><span>горина О.В.</span></strong>\n                </li>\n                <li class=\"b-l-hide-mob\">\n                    <p class=\"f-g\"></p>\n                </li>\n                <!--ряд 2-->\n\n\n\n\n\n                <!--ряд 3-->\n                <!--li class=\"b-l-hide-mob\">\n                    <p class=\"f-g b-l-hide\"></p>\n                </li>\n                <li class=\"b-l-hide-inverse\">\n                    <p class=\"b-o\"></p>\n                </li>\n                <li>\n                    <p>\n                        <span>\n                            <img src=\"images/build-4.jpg\" alt=\"house\">\n                        </span>\n                    </p>\n                    <strong><span>Малоэтажные дома</span></strong>\n                </li>\n                <li class=\"b-l-hide-mob\">\n                    <p class=\"f-o\"></p>\n                </li>\n                <li class=\"b-l-hide-inverse\"></li-->\n                <!--ряд 3-->\n\n\n\n\n                <!--ряд 4(дополнительный)-->\n                <!--li class=\"b-l-hide-mob\"></li>\n                <li>\n                    <p>\n                        <span>\n                            <img src=\"images/build-4.jpg\" alt=\"house\">\n                        </span>\n                    </p>\n                    <strong><span>Малоэтажные дома</span></strong>\n                </li>\n                <li>\n                    <p>\n                        <span>\n                            <img src=\"images/build-4.jpg\" alt=\"house\">\n                        </span>\n                    </p>\n                    <strong><span>Малоэтажные дома</span></strong>\n                </li-->\n                <!--ряд 4-->\n\n\n            </ul>\n        </div>\n    </section>\n\n    <section class=\"why\">\n        <div class=\"container\">\n            <h3 class=\"wow fadeInLeft\">Почему мы<span><i></i></span></h3>\n            <ul>\n                <li class=\"wow fadeInUp\">\n                    <img src=\"images/why-1.png\" alt=\"temperature\">\n                    <p>строим в любое время года</p>\n                </li>\n                <li class=\"wow fadeInUp\">\n                    <img src=\"images/why-2.png\" alt=\"temperature\">\n                    <p>уникальная технология</p>\n                </li>\n                <li class=\"wow fadeInUp\">\n                    <img src=\"images/why-3.png\" alt=\"temperature\">\n                    <p>монтаж готового объекта</p>\n                </li>\n                <li class=\"wow fadeInUp\">\n                    <img src=\"images/why-4.png\" alt=\"temperature\">\n                    <p>Ответственность за сохранность материалов</p>\n                </li>\n                <li class=\"wow fadeInUp\">\n                    <img src=\"images/why-5.png\" alt=\"temperature\">\n                    <p>Фиксированная цена по договору</p>\n                </li>\n                <li class=\"wow fadeInUp\">\n                    <img src=\"images/why-6.png\" alt=\"temperature\">\n                    <p>Свое производство 1 500 м2</p>\n                </li>\n                <li class=\"wow fadeInUp\">\n                    <img src=\"images/why-7.png\" alt=\"temperature\">\n                    <p>Экологичность</p>\n                </li>\n                <li class=\"wow fadeInUp\">\n                    <img src=\"images/why-8.png\" alt=\"temperature\">\n                    <p>Долговечность</p>\n                </li>\n                <li class=\"wow fadeInUp\">\n                    <img src=\"images/why-9.png\" alt=\"temperature\">\n                    <p>Экономия</p>\n                </li>\n                <li class=\"wow fadeInUp\">\n                    <img src=\"images/why-10.png\" alt=\"temperature\">\n                    <p>высокое энергосбережение</p>\n                </li>\n                <li class=\"wow fadeInUp\">\n                    <img src=\"images/why-11.png\" alt=\"temperature\">\n                    <p>Огнестойкие материалы</p>\n                </li>\n                <li class=\"wow fadeInUp\">\n                    <img src=\"images/why-12.png\" alt=\"temperature\">\n                    <p>Материалы не едят грызуны</p>\n                </li>\n            </ul>\n        </div>\n    </section>\n\n    <section class=\"own\" style=\"background: #322f19 url(images/own-bg.jpg) no-repeat center;\">\n        <div class=\"container\">\n            <h2>СВОё производство<strong>600 кв.м²<span><i></i></span></strong></h2>\n        </div>\n    </section>\n\n    <section class=\"sertificates\">\n        <div class=\"container\">\n            <h3 class=\"wow fadeInLeft\">сертификаты<span><i></i></span></h3>\n            <div class=\"sert\">\n                <div>\n                    <img src=\"images/sert-1.jpg\" alt=\"sertificate\">\n                </div>\n                <div>\n                    <img src=\"images/sert-2.jpg\" alt=\"sertificate\">\n                </div>\n                <div>\n                    <img src=\"images/sert-3.jpg\" alt=\"sertificate\">\n                </div>\n                <div>\n                    <img src=\"images/sert-2.jpg\" alt=\"sertificate\">\n                </div>\n            </div>\n        </div>\n    </section>','Наша команда','Наша команда','Наша команда','О нас','Наша команда',1,3),
	(4,'    <section class=\"objects-short\">\n        <div class=\"container\">\n            <ul class=\"obj-list\">\n                <li><a href=\"#\">Все объекты</a></li>\n                <li><a href=\"#\">Гостиницы</a></li>\n                <li class=\"active\"><a href=\"#\">Малоквартирные дома</a></li>\n                <li><a href=\"#\">комплексы</a></li>\n                <li><a href=\"#\">Базы отдыха</a></li>\n                <li><a href=\"#\">отели</a></li>\n                <li><a href=\"#\">Малоэтажные дома</a></li>\n            </ul>\n            <div class=\"form-group\">\n                <select onchange=\"window.location.href=this.options[this.selectedIndex].value\">\n                    <option VALUE=\"link here\">Все объекты</option>\n                    <option VALUE=\"link here\">Гостиницы</option>\n                    <option VALUE=\"link here\">Малоквартирные дома</option>\n                    <option VALUE=\"link here\">комплексы</option>\n                    <option VALUE=\"link here\">Базы отдыха</option>\n                    <option VALUE=\"link here\">отели</option>\n                    <option VALUE=\"link here\">Малоэтажные дома</option>\n                </select>\n                <i></i>\n            </div>\n            <div class=\"row obj-short-item\">\n                <div class=\"col-sm-6\">\n                    <img src=\"images/object-short-1.jpg\" alt=\"house\">\n                </div>\n                <div class=\"col-sm-6\">\n                    <h4>Дом Модульный<span><i></i></span></h4>\n                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla quam velit, vulputate eu pharetra nec, mattis ac neque. Duis vulputate commodo lectus, ac blandit elit tincidunt id. Sed rhoncus, tortor sed eleifend tristique, tortor mauris molestie elit, et lacinia ipsum quam nec dui. Quisque nec mauris sit amet elit iaculis pretium sit amet quis magna. Aenean velit odio, elementum in tempus ut, vehicula eu diam. Pellentesque rhoncus aliquam mattis. Ut vulputate eros sed felis sodales nec vulputate justo hendrerit. Vivamus varius pretium ligula, a aliquam odio euismod sit amet. Quisque laoreet sem sit amet orci ullamcorper at ultricies metus viverra. Pellentesque arcu mauris, malesuada quis ornare accumsan, blandit sed diam.</p>\n                    <a href=\"#\" class=\"btn btn-filled-o btn-sm-h\">ПОДРОБНЕЕ</a>\n                </div>\n            </div>\n            <div class=\"row obj-short-item\">\n                <div class=\"col-sm-6\">\n                    <img src=\"images/object-short-1.jpg\" alt=\"house\">\n                </div>\n                <div class=\"col-sm-6\">\n                    <h4>Дом Модульный<span><i></i></span></h4>\n                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla quam velit, vulputate eu pharetra nec, mattis ac neque. Duis vulputate commodo lectus, ac blandit elit tincidunt id. Sed rhoncus, tortor sed eleifend tristique, tortor mauris molestie elit, et lacinia ipsum quam nec dui. Quisque nec mauris sit amet elit iaculis pretium sit amet quis magna. Aenean velit odio, elementum in tempus ut, vehicula eu diam. Pellentesque rhoncus aliquam mattis. Ut vulputate eros sed felis sodales nec vulputate justo hendrerit. Vivamus varius pretium ligula, a aliquam odio euismod sit amet. Quisque laoreet sem sit amet orci ullamcorper at ultricies metus viverra. Pellentesque arcu mauris, malesuada quis ornare accumsan, blandit sed diam.</p>\n                    <a href=\"#\" class=\"btn btn-filled-o btn-sm-h\">ПОДРОБНЕЕ</a>\n                </div>\n            </div>\n            <div class=\"row obj-short-item\">\n                <div class=\"col-sm-6\">\n                    <img src=\"images/object-short-1.jpg\" alt=\"house\">\n                </div>\n                <div class=\"col-sm-6\">\n                    <h4>Дом Модульный<span><i></i></span></h4>\n                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla quam velit, vulputate eu pharetra nec, mattis ac neque. Duis vulputate commodo lectus, ac blandit elit tincidunt id. Sed rhoncus, tortor sed eleifend tristique, tortor mauris molestie elit, et lacinia ipsum quam nec dui. Quisque nec mauris sit amet elit iaculis pretium sit amet quis magna. Aenean velit odio, elementum in tempus ut, vehicula eu diam. Pellentesque rhoncus aliquam mattis. Ut vulputate eros sed felis sodales nec vulputate justo hendrerit. Vivamus varius pretium ligula, a aliquam odio euismod sit amet. Quisque laoreet sem sit amet orci ullamcorper at ultricies metus viverra. Pellentesque arcu mauris, malesuada quis ornare accumsan, blandit sed diam.</p>\n                    <a href=\"#\" class=\"btn btn-filled-o btn-sm-h\">ПОДРОБНЕЕ</a>\n                </div>\n            </div>\n            <div class=\"row obj-short-item\">\n                <div class=\"col-sm-6\">\n                    <img src=\"images/object-short-1.jpg\" alt=\"house\">\n                </div>\n                <div class=\"col-sm-6\">\n                    <h4>Дом Модульный<span><i></i></span></h4>\n                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla quam velit, vulputate eu pharetra nec, mattis ac neque. Duis vulputate commodo lectus, ac blandit elit tincidunt id. Sed rhoncus, tortor sed eleifend tristique, tortor mauris molestie elit, et lacinia ipsum quam nec dui. Quisque nec mauris sit amet elit iaculis pretium sit amet quis magna. Aenean velit odio, elementum in tempus ut, vehicula eu diam. Pellentesque rhoncus aliquam mattis. Ut vulputate eros sed felis sodales nec vulputate justo hendrerit. Vivamus varius pretium ligula, a aliquam odio euismod sit amet. Quisque laoreet sem sit amet orci ullamcorper at ultricies metus viverra. Pellentesque arcu mauris, malesuada quis ornare accumsan, blandit sed diam.</p>\n                    <a href=\"#\" class=\"btn btn-filled-o btn-sm-h\">ПОДРОБНЕЕ</a>\n                </div>\n            </div>\n            <div class=\"text-center\">\n                <button type=\"button\"class=\"btn btn-bordered-o btn-lg\">показать еще</button>\n            </div>\n        </div>\n    </section>','Построенные объекты','Построенные объекты','Построенные объекты','Построенные объекты','Построенные объекты',1,4),
	(5,'    <section class=\"objects-short rieltors\">\n        <div class=\"container\">\n            <div class=\"row obj-short-item\">\n                <div class=\"col-sm-6\">\n                    <img src=\"images/object-short-1.jpg\" alt=\"house\">\n                </div>\n                <div class=\"col-sm-6\">\n                    <h4>Дом Модульный<span><i></i></span></h4>\n                    <ul class=\"rielt-info\">\n                        <li>\n                            <div>Адрес:</div>\n                            <div>\n                                <h6>г. Одесса, ул. Канатная. 88</h6>\n                            </div>\n                        </li>\n                        <li>\n                            <div>Стоимость:</div>\n                            <div>\n                                <strong>541 т.грн</strong>\n                            </div>\n                        </li>\n                        <li>\n                            <div>Акционная стоимость:</div>\n                            <div>\n                                <b>351 т.грн</b>\n                            </div>\n                        </li>\n                    </ul>\n                    <p>Акция действует до 09.09.2017</p>\n                    <a href=\"#\" class=\"btn btn-filled-o btn-sm-h\">ПОДРОБНЕЕ</a>\n                </div>\n            </div>\n            <div class=\"row obj-short-item\">\n                <div class=\"col-sm-6\">\n                    <img src=\"images/object-short-1.jpg\" alt=\"house\">\n                </div>\n                <div class=\"col-sm-6\">\n                    <h4>Дом Модульный<span><i></i></span></h4>\n                    <ul class=\"rielt-info\">\n                        <li>\n                            <div>Адрес:</div>\n                            <div>\n                                <h6>г. Одесса, ул. Канатная. 88</h6>\n                            </div>\n                        </li>\n                        <li>\n                            <div>Стоимость:</div>\n                            <div>\n                                <strong>541 т.грн</strong>\n                            </div>\n                        </li>\n                        <li>\n                            <div>Акционная стоимость:</div>\n                            <div>\n                                <b>351 т.грн</b>\n                            </div>\n                        </li>\n                    </ul>\n                    <p>Акция действует до 09.09.2017</p>\n                    <a href=\"#\" class=\"btn btn-filled-o btn-sm-h\">ПОДРОБНЕЕ</a>\n                </div>\n            </div>\n            <div class=\"row obj-short-item\">\n                <div class=\"col-sm-6\">\n                    <img src=\"images/object-short-1.jpg\" alt=\"house\">\n                </div>\n                <div class=\"col-sm-6\">\n                    <h4>Дом Модульный<span><i></i></span></h4>\n                    <ul class=\"rielt-info\">\n                        <li>\n                            <div>Адрес:</div>\n                            <div>\n                                <h6>г. Одесса, ул. Канатная. 88</h6>\n                            </div>\n                        </li>\n                        <li>\n                            <div>Стоимость:</div>\n                            <div>\n                                <strong>541 т.грн</strong>\n                            </div>\n                        </li>\n                        <li>\n                            <div>Акционная стоимость:</div>\n                            <div>\n                                <b>351 т.грн</b>\n                            </div>\n                        </li>\n                    </ul>\n                    <p>Акция действует до 09.09.2017</p>\n                    <a href=\"#\" class=\"btn btn-filled-o btn-sm-h\">ПОДРОБНЕЕ</a>\n                </div>\n            </div>\n            <div class=\"row obj-short-item\">\n                <div class=\"col-sm-6\">\n                    <img src=\"images/object-short-1.jpg\" alt=\"house\">\n                </div>\n                <div class=\"col-sm-6\">\n                    <h4>Дом Модульный<span><i></i></span></h4>\n                    <ul class=\"rielt-info\">\n                        <li>\n                            <div>Адрес:</div>\n                            <div>\n                                <h6>г. Одесса, ул. Канатная. 88</h6>\n                            </div>\n                        </li>\n                        <li>\n                            <div>Стоимость:</div>\n                            <div>\n                                <strong>541 т.грн</strong>\n                            </div>\n                        </li>\n                        <li>\n                            <div>Акционная стоимость:</div>\n                            <div>\n                                <b>351 т.грн</b>\n                            </div>\n                        </li>\n                    </ul>\n                    <p>Акция действует до 09.09.2017</p>\n                    <a href=\"#\" class=\"btn btn-filled-o btn-sm-h\">ПОДРОБНЕЕ</a>\n                </div>\n            </div>\n            <div class=\"text-center\">\n                <button type=\"button\"class=\"btn btn-bordered-o btn-lg\">показать еще</button>\n            </div>\n        </div>\n    </section>','Наши риэлторы','Наши риэлторы','Наши риэлторы','Наши риэлторы','Наши риэлторы',1,5),
	(6,'    <section class=\"text\">\n        <div class=\"container\">\n            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla quam velit, vulputate eu pharetra nec, mattis ac neque. Duis vulputate commodo lectus, ac blandit elit tincidunt id. Sed rhoncus, tortor sed eleifend tristique, tortor mauris molestie elit, et lacinia ipsum quam nec dui. Quisque nec mauris sit amet elit iaculis pretium sit amet quis magna. Aenean velit odio, elementum in tempus ut, vehicula eu diam. Pellentesque rhoncus aliquam mattis. Ut vulputate eros sed felis sodales nec vulputate justo hendrerit. Vivamus varius pretium ligula, a aliquam odio euismod sit amet. Quisque laoreet sem sit amet orci ullamcorper at ultricies metus viverra. Pellentesque arcu mauris, malesuada quis ornare accumsan, blandit sed diam.</p>\n            <h4>Lorem ipsum dolor</h4>\n            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla quam velit, vulputate eu pharetra nec, mattis ac neque. Duis vulputate commodo lectus, ac blandit elit tincidunt id. Sed rhoncus, tortor sed eleifend tristique, tortor mauris molestie elit, et lacinia ipsum quam nec dui. Quisque nec mauris sit amet elit iaculis pretium sit amet quis magna. Aenean velit odio, elementum in tempus ut, vehicula eu diam. Pellentesque rhoncus aliquam mattis. Ut vulputate eros sed felis sodales nec vulputate justo hendrerit. Vivamus varius pretium ligula, a aliquam odio euismod sit amet. Quisque laoreet sem sit amet orci ullamcorper at ultricies metus viverra. Pellentesque arcu mauris, malesuada quis ornare accumsan, blandit sed diam.</p>\n            <div class=\"text-center\">\n                <button type=\"button\" class=\"btn btn-filled-g btn-sm\" data-toggle=\"modal\" data-target=\"#modal-invest\">отправить запрос на инвестирование</button>\n            </div>\n        </div>\n    </section>\n\n    <section class=\"why\">\n        <div class=\"container\">\n            <h3 class=\"wow fadeInLeft\">Почему мы<span><i></i></span></h3>\n            <ul>\n                <li class=\"wow fadeInUp\">\n                    <img src=\"images/why-1.png\" alt=\"temperature\">\n                    <p>строим в любое время года</p>\n                </li>\n                <li class=\"wow fadeInUp\">\n                    <img src=\"images/why-2.png\" alt=\"temperature\">\n                    <p>уникальная технология</p>\n                </li>\n                <li class=\"wow fadeInUp\">\n                    <img src=\"images/why-3.png\" alt=\"temperature\">\n                    <p>монтаж готового объекта</p>\n                </li>\n                <li class=\"wow fadeInUp\">\n                    <img src=\"images/why-4.png\" alt=\"temperature\">\n                    <p>Ответственность за сохранность материалов</p>\n                </li>\n                <li class=\"wow fadeInUp\">\n                    <img src=\"images/why-5.png\" alt=\"temperature\">\n                    <p>Фиксированная цена по договору</p>\n                </li>\n                <li class=\"wow fadeInUp\">\n                    <img src=\"images/why-6.png\" alt=\"temperature\">\n                    <p>Свое производство 1 500 м2</p>\n                </li>\n                <li class=\"wow fadeInUp\">\n                    <img src=\"images/why-7.png\" alt=\"temperature\">\n                    <p>Экологичность</p>\n                </li>\n                <li class=\"wow fadeInUp\">\n                    <img src=\"images/why-8.png\" alt=\"temperature\">\n                    <p>Долговечность</p>\n                </li>\n                <li class=\"wow fadeInUp\">\n                    <img src=\"images/why-9.png\" alt=\"temperature\">\n                    <p>Экономия</p>\n                </li>\n                <li class=\"wow fadeInUp\">\n                    <img src=\"images/why-10.png\" alt=\"temperature\">\n                    <p>высокое энергосбережение</p>\n                </li>\n                <li class=\"wow fadeInUp\">\n                    <img src=\"images/why-11.png\" alt=\"temperature\">\n                    <p>Огнестойкие материалы</p>\n                </li>\n                <li class=\"wow fadeInUp\">\n                    <img src=\"images/why-12.png\" alt=\"temperature\">\n                    <p>Материалы не едят грызуны</p>\n                </li>\n            </ul>\n        </div>\n    </section>\n\n    <section class=\"work-world\" style=\"background: #354145 url(images/work-world-bg.jpg) no-repeat center;\">\n        <div class=\"container\">\n            <div class=\"map-pin\">\n                <div class=\"figure wow flipInX\"></div>\n                <div class=\"center-pin wow flipInX\" data-wow-delay=\"0.2s\"></div>\n                <div class=\"pin-1 wow flipInX\" data-wow-delay=\"1s\"></div>\n                <div class=\"pin-2 wow flipInX\" data-wow-delay=\"0.4s\"></div>\n                <div class=\"pin-3 wow flipInX\" data-wow-delay=\"0.8s\"></div>\n                <div class=\"pin-4 wow flipInX\" data-wow-delay=\"0.6s\"></div>\n                <div class=\"pin-5 wow flipInX\" data-wow-delay=\"1.2s\"></div>\n            </div>\n            <h2>работаем по всему миру</h2>\n        </div>\n    </section>','Инвесторам','Инвесторам','Инвесторам','Инвесторам','Инвесторам',1,6),
	(7,'    <section class=\"partners\">\n        <div class=\"build-wrapper\">\n            <ul class=\"build-list\">\n\n\n\n                <!--ряд 1-->\n                <li class=\"b-l-hide-mob\">\n                    <p class=\"b-o\"></p>\n                </li>\n                <li>\n                    <p>\n                        <span>\n                            <img src=\"images/partner-1.jpg\" alt=\"house\">\n                        </span>\n                    </p>\n                </li>\n                <li>\n                    <p>\n                        <span>\n                            <img src=\"images/partner-2.jpg\" alt=\"house\">\n                        </span>\n                    </p>\n                </li>\n                <li class=\"b-l-hide\"></li>\n                <li class=\"b-l-hide\">\n                    <p class=\"b-o\"></p>\n                </li>\n                <li>\n                    <p>\n                        <span>\n                            <img src=\"images/partner-3.jpg\" alt=\"house\">\n                        </span>\n                    </p>\n                </li>\n                <li class=\"b-l-hide-mob\">\n                    <p class=\"f-o\"></p>\n                </li>\n                <!--ряд 1-->\n\n\n\n\n\n                <!--ряд 2-->\n                <li class=\"b-l-hide-mob\">\n                    <p class=\"f-o\"></p>\n                </li>\n                <li>\n                    <p>\n                        <span>\n                            <img src=\"images/partner-4.jpg\" alt=\"house\">\n                        </span>\n                    </p>\n                </li>\n                <li>\n                    <p>\n                        <span>\n                            <img src=\"images/partner-5.jpg\" alt=\"house\">\n                        </span>\n                    </p>\n                </li>\n                <li class=\"b-l-hide-mob\">\n                    <p class=\"b-o\"></p>\n                </li>\n                <!--ряд 2-->\n\n\n\n\n\n                <!--ряд 3-->\n                <!--li class=\"b-l-hide-mob\">\n                    <p class=\"f-g b-l-hide\"></p>\n                </li>\n                <li class=\"b-l-hide-inverse\">\n                    <p class=\"b-o\"></p>\n                </li>\n                <li>\n                    <p>\n                        <span>\n                            <img src=\"images/build-4.jpg\" alt=\"house\">\n                        </span>\n                    </p>\n                </li>\n                <li class=\"b-l-hide-mob\">\n                    <p class=\"f-o\"></p>\n                </li>\n                <li class=\"b-l-hide-inverse\"></li-->\n                <!--ряд 3-->\n\n\n\n\n                <!--ряд 4(дополнительный)-->\n                <!--li class=\"b-l-hide-mob\"></li>\n                <li>\n                    <p>\n                        <span>\n                            <img src=\"images/build-4.jpg\" alt=\"house\">\n                        </span>\n                    </p>\n                </li>\n                <li>\n                    <p>\n                        <span>\n                            <img src=\"images/build-4.jpg\" alt=\"house\">\n                        </span>\n                    </p>\n                </li-->\n                <!--ряд 4-->\n\n\n            </ul>\n        </div>\n    </section>','Наши партнеры','Наши партнеры','Наши партнеры','Наши партнеры','Наши партнеры',1,7),
	(8,'    <section class=\"contacts\">\n        <div class=\"container\">\n            <h2>Экмос Групп</h2>\n            <p>Украина, Одесская область<br>с. Еремеевка, ул. Молодежная</p>\n            <a href=\"tel:+380960530306\">+38 096 053 03 06</a>\n            <br>\n            <a href=\"mailto:info@ecmos.com.ua\">info@ecmos.com.ua</a>\n            <p><strong>Время работы:</strong>Пн - Пт 10.00 - 18.00</p>\n        </div>\n    </section>\n\n    <div id=\"map\"></div>','Контакты','Контакты','Контакты','Контакты','Контакты',1,8),
	(9,'    <section class=\"projects\">English projects</section>','	 Standard projects','	 Standard projects','	 Standard projects','Projects','Standard projects',3,1),
	(10,'    <section class=\"price\">English price</section>','Prices','Prices','Prices','Prices','Prices',3,2),
	(11,'    <section class=\"team\">English team</section>','Our team','Our team','Our team','About us','Our team',3,3),
	(12,'    <section class=\"objects-short\">English objects</section>','Constructed objects','Constructed objects','Constructed objects','Constructed objects','Constructed objects',3,4),
	(13,'    <section class=\"objects-short rieltors\">English Realtors</section>','	 Our realtors','	 Our realtors','	 Our realtors','Our realtors','Our realtors',3,5),
	(14,'    <section class=\"text\">English investors</section>','For investors','For investors','For investors','For investors','For investors',3,6),
	(15,'    <section class=\"partners\">English partners</section>','Our partners','Our partners','Our partners','Our partners','Our partners',3,7),
	(16,'    <section class=\"contacts\">English contacts</section>\n\n    <div id=\"map\"></div>','Contacts','Contacts','Contacts','Contacts','Contacts',3,8),
	(18,'    <section class=\"contacts\">English contacts</section>\r\n\r\n    <div id=\"map\"></div>','Home','Home','Home','Home','Home',3,9),
	(17,'    <section class=\"objects-short rieltors\">\r\n        <div class=\"container\">\r\n            <div class=\"row obj-short-item\">\r\n                <div class=\"col-sm-6\">\r\n                    <img src=\"images/object-short-1.jpg\" alt=\"house\">\r\n                </div>\r\n                <div class=\"col-sm-6\">\r\n                    <h4>Дом Модульный<span><i></i></span></h4>\r\n                    <ul class=\"rielt-info\">\r\n                        <li>\r\n                            <div>Адрес:</div>\r\n                            <div>\r\n                                <h6>г. Одесса, ул. Канатная. 88</h6>\r\n                            </div>\r\n                        </li>\r\n                        <li>\r\n                            <div>Стоимость:</div>\r\n                            <div>\r\n                                <strong>541 т.грн</strong>\r\n                            </div>\r\n                        </li>\r\n                        <li>\r\n                            <div>Акционная стоимость:</div>\r\n                            <div>\r\n                                <b>351 т.грн</b>\r\n                            </div>\r\n                        </li>\r\n                    </ul>\r\n                    <p>Акция действует до 09.09.2017</p>\r\n                    <a href=\"#\" class=\"btn btn-filled-o btn-sm-h\">ПОДРОБНЕЕ</a>\r\n                </div>\r\n            </div>\r\n            <div class=\"row obj-short-item\">\r\n                <div class=\"col-sm-6\">\r\n                    <img src=\"images/object-short-1.jpg\" alt=\"house\">\r\n                </div>\r\n                <div class=\"col-sm-6\">\r\n                    <h4>Дом Модульный<span><i></i></span></h4>\r\n                    <ul class=\"rielt-info\">\r\n                        <li>\r\n                            <div>Адрес:</div>\r\n                            <div>\r\n                                <h6>г. Одесса, ул. Канатная. 88</h6>\r\n                            </div>\r\n                        </li>\r\n                        <li>\r\n                            <div>Стоимость:</div>\r\n                            <div>\r\n                                <strong>541 т.грн</strong>\r\n                            </div>\r\n                        </li>\r\n                        <li>\r\n                            <div>Акционная стоимость:</div>\r\n                            <div>\r\n                                <b>351 т.грн</b>\r\n                            </div>\r\n                        </li>\r\n                    </ul>\r\n                    <p>Акция действует до 09.09.2017</p>\r\n                    <a href=\"#\" class=\"btn btn-filled-o btn-sm-h\">ПОДРОБНЕЕ</a>\r\n                </div>\r\n            </div>\r\n            <div class=\"row obj-short-item\">\r\n                <div class=\"col-sm-6\">\r\n                    <img src=\"images/object-short-1.jpg\" alt=\"house\">\r\n                </div>\r\n                <div class=\"col-sm-6\">\r\n                    <h4>Дом Модульный<span><i></i></span></h4>\r\n                    <ul class=\"rielt-info\">\r\n                        <li>\r\n                            <div>Адрес:</div>\r\n                            <div>\r\n                                <h6>г. Одесса, ул. Канатная. 88</h6>\r\n                            </div>\r\n                        </li>\r\n                        <li>\r\n                            <div>Стоимость:</div>\r\n                            <div>\r\n                                <strong>541 т.грн</strong>\r\n                            </div>\r\n                        </li>\r\n                        <li>\r\n                            <div>Акционная стоимость:</div>\r\n                            <div>\r\n                                <b>351 т.грн</b>\r\n                            </div>\r\n                        </li>\r\n                    </ul>\r\n                    <p>Акция действует до 09.09.2017</p>\r\n                    <a href=\"#\" class=\"btn btn-filled-o btn-sm-h\">ПОДРОБНЕЕ</a>\r\n                </div>\r\n            </div>\r\n            <div class=\"row obj-short-item\">\r\n                <div class=\"col-sm-6\">\r\n                    <img src=\"images/object-short-1.jpg\" alt=\"house\">\r\n                </div>\r\n                <div class=\"col-sm-6\">\r\n                    <h4>Дом Модульный<span><i></i></span></h4>\r\n                    <ul class=\"rielt-info\">\r\n                        <li>\r\n                            <div>Адрес:</div>\r\n                            <div>\r\n                                <h6>г. Одесса, ул. Канатная. 88</h6>\r\n                            </div>\r\n                        </li>\r\n                        <li>\r\n                            <div>Стоимость:</div>\r\n                            <div>\r\n                                <strong>541 т.грн</strong>\r\n                            </div>\r\n                        </li>\r\n                        <li>\r\n                            <div>Акционная стоимость:</div>\r\n                            <div>\r\n                                <b>351 т.грн</b>\r\n                            </div>\r\n                        </li>\r\n                    </ul>\r\n                    <p>Акция действует до 09.09.2017</p>\r\n                    <a href=\"#\" class=\"btn btn-filled-o btn-sm-h\">ПОДРОБНЕЕ</a>\r\n                </div>\r\n            </div>\r\n            <div class=\"text-center\">\r\n                <button type=\"button\"class=\"btn btn-bordered-o btn-lg\">показать еще</button>\r\n            </div>\r\n        </div>\r\n    </section>','Домашняя','Домашняя','Домашняя','Домашняя','Домашняя',1,9);

/*!40000 ALTER TABLE `page_content` ENABLE KEYS */;
UNLOCK TABLES;


# Дамп таблицы picture
# ------------------------------------------------------------

DROP TABLE IF EXISTS `picture`;

CREATE TABLE `picture` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `active` bit(1) NOT NULL,
  `ordering` int(4) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `picture` WRITE;
/*!40000 ALTER TABLE `picture` DISABLE KEYS */;

INSERT INTO `picture` (`id`, `type`, `active`, `ordering`)
VALUES
	(1,'whywe',b'1',2),
	(2,'whywe',b'1',1),
	(3,'whywe',b'1',3),
	(4,'whywe',b'1',4),
	(5,'whywe',b'1',5),
	(6,'whywe',b'1',6),
	(7,'whywe',b'1',7),
	(8,'whywe',b'1',8),
	(9,'whywe',b'1',9),
	(10,'whywe',b'1',10),
	(11,'whywe',b'1',11),
	(12,'whywe',b'1',12),
	(13,'certificate',b'1',1),
	(14,'certificate',b'1',2),
	(15,'certificate',b'1',3),
	(16,'certificate',b'1',4),
	(17,'certificate',b'1',5),
	(18,'employee',b'1',1),
	(19,'employee',b'1',2),
	(20,'employee',b'1',3),
	(21,'employee',b'1',4),
	(22,'employee',b'1',5),
	(23,'employee',b'1',6),
	(24,'partner',b'1',1),
	(25,'partner',b'1',2),
	(26,'partner',b'1',3),
	(27,'partner',b'1',4),
	(28,'partner',b'1',5),
	(29,'whatwebuild',b'1',1),
	(30,'whatwebuild',b'1',2),
	(31,'whatwebuild',b'1',3),
	(32,'whatwebuild',b'1',4),
	(33,'whatwebuild',b'1',5),
	(34,'whatwebuild',b'1',6);

/*!40000 ALTER TABLE `picture` ENABLE KEYS */;
UNLOCK TABLES;


# Дамп таблицы picture_content
# ------------------------------------------------------------

DROP TABLE IF EXISTS `picture_content`;

CREATE TABLE `picture_content` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `picture_title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT '',
  `language_id` int(11) NOT NULL,
  `picture_id` int(11) NOT NULL,
  `picture_link` text COLLATE utf8mb4_unicode_ci,
  PRIMARY KEY (`id`),
  KEY `FKgtjcsmnmnrn2fo2qchf7xxhpd` (`language_id`),
  KEY `FKmu1ccewr71f1gk4wnr20a0nrt` (`picture_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `picture_content` WRITE;
/*!40000 ALTER TABLE `picture_content` DISABLE KEYS */;

INSERT INTO `picture_content` (`id`, `picture_title`, `language_id`, `picture_id`, `picture_link`)
VALUES
	(1,'СТРОИМ В ЛЮБОЕ ВРЕМЯ ГОДА',1,1,NULL),
	(2,'УНИКАЛЬНАЯ ТЕХНОЛОГИЯ',1,2,NULL),
	(3,'МОНТАЖ ГОТОВОГО ОБЪЕКТА',1,3,NULL),
	(4,'ОТВЕТСТВЕННОСТЬ ЗА СОХРАННОСТЬ МАТЕРИАЛОВ',1,4,NULL),
	(5,'ФИКСИРОВАННАЯ ЦЕНА ПО ДОГОВОРУ',1,5,NULL),
	(6,'СВОЕ ПРОИЗВОДСТВО 1 500 М2',1,6,NULL),
	(7,'ЭКОЛОГИЧНОСТЬ',1,7,NULL),
	(8,'ДОЛГОВЕЧНОСТЬ',1,8,NULL),
	(9,'ЭКОНОМИЯ',1,9,NULL),
	(10,'ВЫСОКОЕ ЭНЕРГОСБЕРЕЖЕНИЕ',1,10,NULL),
	(11,'ОГНЕСТОЙКИЕ МАТЕРИАЛЫ',1,11,NULL),
	(12,'МАТЕРИАЛЫ НЕ ЕДЯТ ГРЫЗУНЫ',1,12,NULL),
	(13,'Иванов Федор Иванович',1,18,NULL),
	(14,'Иванов С. А.',1,19,NULL),
	(15,'Горина О. В.',1,20,NULL),
	(16,'Стокова Л. Д.',1,21,NULL),
	(17,'Иванов С. А.',1,22,NULL),
	(18,'Горина О. В.',1,23,NULL),
	(19,'Utah Jazz',1,24,NULL),
	(20,'Intel',1,25,'sdfsdfsdfsdf'),
	(21,'Nexus',1,26,'sdfsdfsdfsdf'),
	(22,'Consult D',1,27,'sdfsdfsdf'),
	(23,'Business Finance Group',1,28,NULL),
	(24,'Гостиницы',1,29,NULL),
	(25,'Малоквартирные дома',1,30,NULL),
	(26,'Комплексы',1,31,NULL),
	(27,'Базы отдыха',1,32,NULL),
	(28,'Отели',1,33,NULL),
	(29,'Малоэтажные дома',1,34,NULL);

/*!40000 ALTER TABLE `picture_content` ENABLE KEYS */;
UNLOCK TABLES;


# Дамп таблицы price
# ------------------------------------------------------------

DROP TABLE IF EXISTS `price`;

CREATE TABLE `price` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `active` bit(1) NOT NULL,
  `ordering` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

LOCK TABLES `price` WRITE;
/*!40000 ALTER TABLE `price` DISABLE KEYS */;

INSERT INTO `price` (`id`, `active`, `ordering`)
VALUES
	(1,b'1',1),
	(2,b'1',2),
	(3,b'1',3);

/*!40000 ALTER TABLE `price` ENABLE KEYS */;
UNLOCK TABLES;


# Дамп таблицы price_content
# ------------------------------------------------------------

DROP TABLE IF EXISTS `price_content`;

CREATE TABLE `price_content` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `language_id` int(11) NOT NULL,
  `price_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK22n8hqtxgfccydfxxu7i0bnx4` (`language_id`),
  KEY `FKegm72s6abl3oocncc7ud7svk7` (`price_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

LOCK TABLES `price_content` WRITE;
/*!40000 ALTER TABLE `price_content` DISABLE KEYS */;

INSERT INTO `price_content` (`id`, `name`, `language_id`, `price_id`)
VALUES
	(1,'Многоквартирные жилые дома',1,1),
	(2,'Гостиницы и апартаменты',1,2),
	(3,'Индивидуальные жилые дома',1,3);

/*!40000 ALTER TABLE `price_content` ENABLE KEYS */;
UNLOCK TABLES;


# Дамп таблицы project
# ------------------------------------------------------------

DROP TABLE IF EXISTS `project`;

CREATE TABLE `project` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `action_begin` date DEFAULT NULL,
  `action_end` date DEFAULT NULL,
  `active` bit(1) NOT NULL,
  `coast` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `coast_action` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `dimensions` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `square` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `video` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `type_object` bit(1) NOT NULL,
  `type_project` bit(1) NOT NULL,
  `type_realtor` bit(1) NOT NULL,
  `object_type_id` int(11) DEFAULT NULL,
  `index_output` bit(1) NOT NULL,
  `ordering` int(4) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FKpqh54nta1lradvvi66djrnw7q` (`object_type_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `project` WRITE;
/*!40000 ALTER TABLE `project` DISABLE KEYS */;

INSERT INTO `project` (`id`, `action_begin`, `action_end`, `active`, `coast`, `coast_action`, `dimensions`, `square`, `video`, `type_object`, `type_project`, `type_realtor`, `object_type_id`, `index_output`, `ordering`)
VALUES
	(1,'2017-10-20','2017-12-22',b'1','225','145','4 х 5','120','xcvxcvsdfgsfsxcvxcv',b'1',b'1',b'1',1,b'1',5),
	(2,'2017-10-20','2019-12-22',b'1','255','125','4 х 5','121','12345',b'1',b'1',b'1',2,b'1',3),
	(3,NULL,NULL,b'1','226','125','4 х 5','122',NULL,b'1',b'0',b'0',3,b'0',2),
	(4,NULL,NULL,b'1','227','123','4 х 5','123',NULL,b'0',b'1',b'0',4,b'0',1),
	(5,NULL,NULL,b'1','228','123','4 х 5','24',NULL,b'0',b'1',b'1',5,b'1',4),
	(6,NULL,NULL,b'1','229','124','4 х 5','25',NULL,b'0',b'0',b'1',6,b'0',6);

/*!40000 ALTER TABLE `project` ENABLE KEYS */;
UNLOCK TABLES;


# Дамп таблицы project_content
# ------------------------------------------------------------

DROP TABLE IF EXISTS `project_content`;

CREATE TABLE `project_content` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `description` mediumtext COLLATE utf8_unicode_ci,
  `flor1_description` mediumtext COLLATE utf8_unicode_ci,
  `flor2_description` mediumtext COLLATE utf8_unicode_ci,
  `flor3_description` mediumtext COLLATE utf8_unicode_ci,
  `keywords` mediumtext COLLATE utf8_unicode_ci,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `language_id` int(11) NOT NULL,
  `project_id` int(11) NOT NULL,
  `h1` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `address` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `general_description` text COLLATE utf8_unicode_ci,
  `flor4_description` text COLLATE utf8_unicode_ci,
  PRIMARY KEY (`id`),
  KEY `FKdu33dsboifyvy7xa9jus4a8oj` (`language_id`),
  KEY `FKnyda7ha0bskepmowtcl2alix0` (`project_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `project_content` WRITE;
/*!40000 ALTER TABLE `project_content` DISABLE KEYS */;

INSERT INTO `project_content` (`id`, `description`, `flor1_description`, `flor2_description`, `flor3_description`, `keywords`, `name`, `language_id`, `project_id`, `h1`, `title`, `address`, `general_description`, `flor4_description`)
VALUES
	(1,NULL,'Внутри три комнаты, большая из которых занимает 18 квадратных метров и может быть разделена еще на две. Используйте сруб как летнюю кухню, баню или дачный дом.',NULL,NULL,NULL,'Дом модульный 1',1,1,NULL,NULL,'Одесса, Греческая, 19','Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla quam velit, vulputate eu pharetra nec, mattis ac neque. Duis vulputate commodo lectus, ac blandit elit tincidunt id. Sed rhoncus, tortor sed eleifend tristique, tortor mauris molestie elit, et lacinia ipsum quam nec dui. Quisque nec mauris sit amet elit iaculis pretium sit amet quis magna. Aenean velit odio, elementum in tempus ut, vehicula eu diam. Pellentesque rhoncus aliquam mattis. Ut vulputate eros sed felis sodales nec vulputate justo hendrerit. Vivamus varius pretium ligula, a aliquam odio euismod sit amet. Quisque laoreet sem sit amet orci ullamcorper at ultricies metus viverra. Pellentesque arcu mauris, malesuada quis ornare accumsan, blandit sed diam.',NULL),
	(2,'Test','Test','Test','Test',NULL,'Дом модульный 2',1,2,'h1 проекта 2','title проекта 2','Одесса, Пушкинская, 18',NULL,NULL),
	(3,'Test','Test','Test','Test',NULL,'Дом модульный 3',1,3,'h1 проекта 3','title проекта 3','Одесса, Екатерининская, 11','Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla quam velit, vulputate eu pharetra nec, mattis ac neque. Duis vulputate commodo lectus, ac blandit elit tincidunt id. Sed rhoncus, tortor sed eleifend tristique, tortor mauris molestie elit, et lacinia ipsum quam nec dui. Quisque nec mauris sit amet elit iaculis pretium sit amet quis magna. Aenean velit odio, elementum in tempus ut, vehicula eu diam. Pellentesque rhoncus aliquam mattis. Ut vulputate eros sed felis sodales nec vulputate justo hendrerit. Vivamus varius pretium ligula, a aliquam odio euismod sit amet. Quisque laoreet sem sit amet orci ullamcorper at ultricies metus viverra. Pellentesque arcu mauris, malesuada quis ornare accumsan, blandit sed diam.',NULL),
	(4,'Test','Test','Test','Test',NULL,'Дом модульный 4',1,4,'h1 проекта 4','title проекта 4','Одесса, Ришельевская, 10','Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla quam velit, vulputate eu pharetra nec, mattis ac neque. Duis vulputate commodo lectus, ac blandit elit tincidunt id. Sed rhoncus, tortor sed eleifend tristique, tortor mauris molestie elit, et lacinia ipsum quam nec dui. Quisque nec mauris sit amet elit iaculis pretium sit amet quis magna. Aenean velit odio, elementum in tempus ut, vehicula eu diam. Pellentesque rhoncus aliquam mattis. Ut vulputate eros sed felis sodales nec vulputate justo hendrerit. Vivamus varius pretium ligula, a aliquam odio euismod sit amet. Quisque laoreet sem sit amet orci ullamcorper at ultricies metus viverra. Pellentesque arcu mauris, malesuada quis ornare accumsan, blandit sed diam.',NULL),
	(5,'Test','Test','Test','Test',NULL,'Дом модульный 5',1,5,'h1 проекта 5','title проекта 5','','Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla quam velit, vulputate eu pharetra nec, mattis ac neque. Duis vulputate commodo lectus, ac blandit elit tincidunt id. Sed rhoncus, tortor sed eleifend tristique, tortor mauris molestie elit, et lacinia ipsum quam nec dui. Quisque nec mauris sit amet elit iaculis pretium sit amet quis magna. Aenean velit odio, elementum in tempus ut, vehicula eu diam. Pellentesque rhoncus aliquam mattis. Ut vulputate eros sed felis sodales nec vulputate justo hendrerit. Vivamus varius pretium ligula, a aliquam odio euismod sit amet. Quisque laoreet sem sit amet orci ullamcorper at ultricies metus viverra. Pellentesque arcu mauris, malesuada quis ornare accumsan, blandit sed diam.',NULL),
	(6,'Test','Test','Test','Test',NULL,'Дом модульный 6',1,6,'h1 проекта 6','title проекта 6','Одесса, Канатная, 5','Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla quam velit, vulputate eu pharetra nec, mattis ac neque. Duis vulputate commodo lectus, ac blandit elit tincidunt id. Sed rhoncus, tortor sed eleifend tristique, tortor mauris molestie elit, et lacinia ipsum quam nec dui. Quisque nec mauris sit amet elit iaculis pretium sit amet quis magna. Aenean velit odio, elementum in tempus ut, vehicula eu diam. Pellentesque rhoncus aliquam mattis. Ut vulputate eros sed felis sodales nec vulputate justo hendrerit. Vivamus varius pretium ligula, a aliquam odio euismod sit amet. Quisque laoreet sem sit amet orci ullamcorper at ultricies metus viverra. Pellentesque arcu mauris, malesuada quis ornare accumsan, blandit sed diam.',NULL);

/*!40000 ALTER TABLE `project_content` ENABLE KEYS */;
UNLOCK TABLES;


# Дамп таблицы slide
# ------------------------------------------------------------

DROP TABLE IF EXISTS `slide`;

CREATE TABLE `slide` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `active` bit(1) NOT NULL,
  `ordering` int(4) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `slide` WRITE;
/*!40000 ALTER TABLE `slide` DISABLE KEYS */;

INSERT INTO `slide` (`id`, `active`, `ordering`)
VALUES
	(1,b'1',1),
	(2,b'1',2),
	(3,b'1',3);

/*!40000 ALTER TABLE `slide` ENABLE KEYS */;
UNLOCK TABLES;


# Дамп таблицы slide_content
# ------------------------------------------------------------

DROP TABLE IF EXISTS `slide_content`;

CREATE TABLE `slide_content` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `link` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `text_big` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `text_small` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `language_id` int(11) NOT NULL,
  `slide_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FKrabhm33vt39ntk4faan6igpxu` (`language_id`),
  KEY `FKl1lsfxev8k52hghvxxrdcpp9l` (`slide_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `slide_content` WRITE;
/*!40000 ALTER TABLE `slide_content` DISABLE KEYS */;

INSERT INTO `slide_content` (`id`, `link`, `text_big`, `text_small`, `language_id`, `slide_id`)
VALUES
	(1,'ecmos.com.ua','индивидуальные и типовые проекты','Сделайте шаг к своей мечте!',1,1),
	(2,'http://google.com','индивидуальные и типовые проекты1','Сделайте шаг к своей мечте1!',1,2),
	(3,'http://ua.fm','qwe','asd',1,3);

/*!40000 ALTER TABLE `slide_content` ENABLE KEYS */;
UNLOCK TABLES;



/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
