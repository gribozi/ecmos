function addSlideObject() {
	$("#slide-service-add").click(function(e) {
		e.preventDefault();
		var createUri = $(this).attr('name');
		var jsonSlide = {
			active: $("#slide-new-active").prop('checked'),
			slideContents: [
				{
					textBig: $("#slide-new-textbig").val(),
					textSmall: $("#slide-new-textsmall").val(),
					link: $("#slide-new-link").val()
				}
			]
		}
		var dataForSend = new FormData();
		dataForSend.append("jsonSlide", JSON.stringify(jsonSlide));
		dataForSend.append("slidePic", $("#slide-new-pic")[0].files[0]);
		$.ajax({
			url: createUri,
			type: "POST",
			enctype: 'multipart/form-data',
			processData: false,
			contentType: false,
			data: dataForSend,
			success: function(result) {
				swal({
					position: 'top-right',
					type: 'success',
					title: 'Изменения сохранены',
					showConfirmButton: false,
					timer: 1500
				});
				console.log("ok");
				window.location.reload();
			},
			error: function(xhr, resp, text) {
				swal({
					position: 'top-right',
					type: 'error',
					title: 'Изменения не сохранены',
					showConfirmButton: false,
					timer: 1500
				})
			}
		})
	});
}

function updateSlideText() {
	$(".slide-service-text").on('blur', function(e) {
		e.preventDefault();
		var parametrName=$(this).attr('name');
		var updateUri = parametrName.substring(0, parametrName.indexOf("/end"));
		var currentId = parametrName.substring(parametrName.indexOf("/") + 1, parametrName.indexOf("/end"));
		var jsonSlide = {
			id: parseInt(currentId),
			active: $("#slide-active-" + currentId).prop('checked'),
			ordering: parseInt($("#slide-ordering-" + currentId).html()),
			slideContents: [
				{
					textBig: $("#slide-textbig-" + currentId)[0].innerHTML,
					textSmall: $("#slide-textsmall-" + currentId)[0].innerHTML,
					link: $("#slide-link-" + currentId)[0].innerHTML
				}
			]
		}
		var dataForSend = new FormData();
		dataForSend.append("jsonSlide", JSON.stringify(jsonSlide));
		$.ajax({
			url: updateUri,
			type: "PUT",
			enctype: 'multipart/form-data',
			processData: false,
			contentType: false,
			data: dataForSend,
			success: function(result) {
				console.log("ok");
				window.location.reload();
			},
			error: function(xhr, resp, text) {
				swal({
					position: 'top-right',
					type: 'error',
					title: 'Изменения не сохранены',
					showConfirmButton: false,
					timer: 1500
				})
			}
		})
	});
}

function updateSlidePicAndActive() {
	$(".slide-service-pic-and-active").change(function(e) {
		e.preventDefault();
		var parametrName=$(this).attr('name');
		var updateUri = parametrName.substring(0, parametrName.indexOf("/end"));
		var currentId = parametrName.substring(parametrName.indexOf("/") + 1, parametrName.indexOf("/end"));
		var jsonSlide = {
			id: parseInt(currentId),
			active: $("#slide-active-" + currentId).prop('checked'),
			ordering: parseInt($("#slide-ordering-" + currentId).html()),
			slideContents: [
				{
					textBig: $("#slide-textbig-" + currentId)[0].innerHTML,
					textSmall: $("#slide-textsmall-" + currentId)[0].innerHTML,
					link: $("#slide-link-" + currentId)[0].innerHTML
				}
			]
		}
		var dataForSend = new FormData();
		dataForSend.append("jsonSlide", JSON.stringify(jsonSlide));
		if ($("#slide-pic-" + currentId)[0].files[0] != undefined) {
			dataForSend.append("slidePic", $("#slide-pic-" + currentId)[0].files[0]);
		}
		$.ajax({
			url: updateUri,
			type: "PUT",
			enctype: 'multipart/form-data',
			processData: false,
			contentType: false,
			data: dataForSend,
			success: function(result) {
				console.log("ok");
				window.location.reload();
			},
			error: function(xhr, resp, text) {
				swal({
					position: 'top-right',
					type: 'error',
					title: 'Изменения не сохранены',
					showConfirmButton: false,
					timer: 1500
				})
			}
		})
	});
};

function removeSlideObject() {
	$(".slide-service-del").click(function() {
		var parametrName=$(this).attr('name');
		var removeUri = parametrName.substring(0, parametrName.indexOf("/end"));
		$.ajax({
			url: removeUri,
			type: "DELETE",
			success: function(result) {
				swal({
					position: 'top-right',
					type: 'success',
					title: 'Изменения сохранены',
					showConfirmButton: false,
					timer: 1500
				});
				window.location.reload();
			},
			error: function(xhr, resp, text) {
				swal({
					position: 'top-right',
					type: 'error',
					title: 'Изменения не сохранены',
					showConfirmButton: false,
					timer: 1500
				})
			}
		})
	});
}

function updateUploadButtonBackground() {
	$("#slide-new-pic").on('change', function(){
		var file = this.files[0];
		var reader = new FileReader();
		reader.onloadend = function () {
				$("#small-pic-upload-button").css('background-image', 'url("' + reader.result + '")');
			};
		if (file) {
			reader.readAsDataURL(file);
		}
	});
}

$(document).ready(function() {
	var table = $('#slides').DataTable({
		rowReorder: true,
		columns: [
			{className: "dt-center"},
			{className: "dt-center"},
			{className: "dt-center"},
			{className: "dt-center"},
			{className: "dt-center"},
			{className: "dt-center"},
			{className: "dt-center"},
			{className: "dt-center"}
		],
		language: {
			search: "Поиск",
			lengthMenu:    "Показать _MENU_ ",
			info:           "Показано с _START_ по _END_ из _TOTAL_",
			infoEmpty:      "Показано 0 из 0",
			infoFiltered:   "Показано 0 из 0",
			infoPostFix:    "",
			loadingRecords: "Загрузка",
			zeroRecords:    "Показано 0 из 0",
			emptyTable:     "Показано 0 из 0",
			paginate: {
				first:      "Первая",
				previous:   "Пред.",
				next:       "След.",
				last:       "Последняя"
			}
		}
	});

	addSlideObject();
	updateSlideText();
	updateSlidePicAndActive();
	removeSlideObject();
	updateUploadButtonBackground();

	table.on('row-reorder', function (e, diff, edit) {
		var jsonForAjaxRequest = [];
		for (var i=0, ien=diff.length; i < ien; i++) {
			var rowData = table.row(diff[i].node).data();
			jsonForAjaxRequest.push({id:rowData[1], ordering:diff[i].newData})
		}
		$.ajax({
			url: '/admin/slider/ordering',
			type: "PUT",
			contentType : "application/json",
			dataType: 'json',
			data: JSON.stringify(jsonForAjaxRequest),
			success: function(result) {
				swal({
					position: 'top-right',
					type: 'success',
					title: 'Изменения сохранены',
					showConfirmButton: false,
					timer: 1500
				})
			},
			error: function(xhr, resp, text) {
				swal({
					position: 'top-right',
					type: 'error',
					title: 'Изменения не сохранены',
					showConfirmButton: false,
					timer: 1500
				})
			}
		})
	});

	$("#slides_paginate").on('click',function () {
		updateSlideText();
		updateSlidePicAndActive();
		removeSlideObject();
	});

});
