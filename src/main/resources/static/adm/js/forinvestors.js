
/**
 * Send ajax form data to save method in controller
*/

$(function () {
	$("#form-text-page").on('submit', function (e) {
		e.preventDefault()
		var jsonForAjaxRequest = {
			forInvestorsContents: [
				{
					text: $("#form-text").val()
				}
			]
		}
		$.ajax({
			url: '/admin/forinvestors',
			type: "PUT",
			contentType: "application/json",
			dataType: 'json',
			data: JSON.stringify(jsonForAjaxRequest),
			success: function(result) {
				swal({
					position: 'top-right',
					type: 'success',
					title: 'Изменения сохранены',
					showConfirmButton: false,
					timer: 1500
				});
				$("#form-text").val(result.forInvestorsContents[0].text);
			},
			error: function(xhr, resp, text) {
				swal({
					position: 'top-right',
					type: 'error',
					title: 'Изменения не сохранены',
					showConfirmButton: false,
					timer: 1500
				})
			}
		})
	})
})
