function addPictureObject() {
	$("#picture-service-add").click(function(e) {
		e.preventDefault();
		var createUri = $(this).attr('name');
		var pictureLink;
		if ($("#picture-new-link").length < 1) {
				pictureLink = "";
		} else {
			pictureLink = $("#picture-new-link").val();
		}
		var jsonPicture = {
			active: $("#picture-new-active").prop('checked'),
			pictureContents: [
				{
					pictureTitle: $("#picture-new-text").val(),
					pictureLink: pictureLink
				}
			]
		}
		var dataForSend = new FormData();
		dataForSend.append("jsonPicture", JSON.stringify(jsonPicture));
		dataForSend.append("picturePic", $("#picture-new-pic")[0].files[0]);
		$.ajax({
			url: createUri,
			type: "POST",
			enctype: 'multipart/form-data',
			processData: false,
			contentType: false,
			data: dataForSend,
			success: function(result) {
				swal({
					position: 'top-right',
					type: 'success',
					title: 'Изменения сохранены',
					showConfirmButton: false,
					timer: 1500
				});
				console.log("ok");
				window.location.reload();
			},
			error: function(xhr, resp, text) {
				swal({
					position: 'top-right',
					type: 'error',
					title: 'Изменения не сохранены',
					showConfirmButton: false,
					timer: 1500
				})
			}
		})
	});
}

function updatePictureText() {
	$(".picture-service-text").on('blur', function(e) {
		e.preventDefault();
		var parametrName = $(this).attr('name');
		var updateUri = parametrName.substring(0, parametrName.indexOf("/end"));
		var currentId = parametrName.substring(parametrName.indexOf("/") + 1, parametrName.indexOf("/end"));
		var pictureLink;
		if ($("#picture-new-link").length < 1) {
				pictureLink = "";
		} else {
			pictureLink = $("#picture-link-" + currentId)[0].innerHTML;
		}
		var jsonPicture = {
			id: parseInt(currentId),
			active: $("#picture-active-" + currentId).prop('checked'),
			ordering: parseInt($("#picture-ordering-" + currentId).html()),
			pictureContents: [
				{
					pictureTitle: $("#picture-text-" + currentId)[0].innerHTML,
					pictureLink: pictureLink
				}
			]
		}
		var dataForSend = new FormData();
		dataForSend.append("jsonPicture", JSON.stringify(jsonPicture));
		$.ajax({
			url: updateUri,
			type: "PUT",
			enctype: 'multipart/form-data',
			processData: false,
			contentType: false,
			data: dataForSend,
			success: function(result) {
				console.log("ok");
				window.location.reload();
			},
			error: function(xhr, resp, text) {
				swal({
					position: 'top-right',
					type: 'error',
					title: 'Изменения не сохранены',
					showConfirmButton: false,
					timer: 1500
				})
			}
		})
	});
}

function updatePicturePicAndActive() {
	$(".picture-service-pic-and-active").change(function(e) {
		e.preventDefault();
		var parametrName=$(this).attr('name');
		var updateUri = parametrName.substring(0, parametrName.indexOf("/end"));
		var currentId = parametrName.substring(parametrName.indexOf("/")+1, parametrName.indexOf("/end"));
		var pictureLink;
		if ($("#picture-new-link").length < 1) {
				pictureLink = "";
		} else {
			pictureLink = $("#picture-link-" + currentId)[0].innerHTML;
		}
		var jsonPicture = {
			id: parseInt(currentId),
			active: $("#picture-active-" + currentId).prop('checked'),
			ordering: parseInt($("#picture-ordering-" + currentId).html()),
			pictureContents: [
				{
					pictureTitle: $("#picture-text-" + currentId)[0].innerHTML,
					pictureLink: pictureLink
				}
			]
		}
		var dataForSend = new FormData();
		dataForSend.append("jsonPicture", JSON.stringify(jsonPicture));
		if ($("#picture-pic-" + currentId)[0].files[0] != undefined) {
			dataForSend.append("picturePic", $("#picture-pic-" + currentId)[0].files[0]);
		}
		$.ajax({
			url: updateUri,
			type: "PUT",
			enctype: 'multipart/form-data',
			processData: false,
			contentType: false,
			data: dataForSend,
			success: function(result) {
				console.log("ok");
				window.location.reload();
			},
			error: function(xhr, resp, text) {
				swal({
					position: 'top-right',
					type: 'error',
					title: 'Изменения не сохранены',
					showConfirmButton: false,
					timer: 1500
				})
			}
		})
	});
}

function removePicObject() {
	$(".picture-service-del").click(function() {
		var parametrName=$(this).attr('name');
		var removeUri = parametrName.substring(0, parametrName.indexOf("/end"));
		$.ajax({
			url: removeUri,
			type: "DELETE",
			success: function(result) {
				swal({
					position: 'top-right',
					type: 'success',
					title: 'Изменения сохранены',
					showConfirmButton: false,
					timer: 1500
				});
				window.location.reload();
			},
			error: function(xhr, resp, text) {
				swal({
					position: 'top-right',
					type: 'error',
					title: 'Изменения не сохранены',
					showConfirmButton: false,
					timer: 1500
				})
			}
		})
	});
};

function updateUploadButtonBackground() {
	$("#picture-new-pic").on('change', function(){
		var file = this.files[0];
		var reader = new FileReader();
		reader.onloadend = function() {
			$("#small-pic-upload-button").css('background-image', 'url("' + reader.result + '")');
		};
		if (file) {
			reader.readAsDataURL(file);
		}
	});
}

function updateTeamText() {
	$("#form-text-page").on('submit', function (e) {
		e.preventDefault()
		var jsonForAjaxRequest = {
			forTeamTextContents: [
				{
					text: $("#form-text").val()
				}
			]
		}
		$.ajax({
			url: '/admin/pictures/teamtext',
			type: "PUT",
			contentType: "application/json",
			dataType: 'json',
			data: JSON.stringify(jsonForAjaxRequest),
			success: function(result) {
				swal({
					position: 'top-right',
					type: 'success',
					title: 'Изменения сохранены',
					showConfirmButton: false,
					timer: 1500
				});
				window.location.reload();
			},
			error: function(xhr, resp, text) {
				swal({
					position: 'top-right',
					type: 'error',
					title: 'Изменения не сохранены',
					showConfirmButton: false,
					timer: 1500
				})
			}
		})
	})
}

$(document).ready(function() {
	var tableColumes = [
							{className: "dt-center"},
							{className: "dt-center"},
							{className: "dt-center"},
							{className: "dt-center"},
							{className: "dt-center"},
							{className: "dt-center"},
							{className: "dt-center"}
						];
	if ($("#picture-new-link").length < 1) {
		tableColumes.splice(1, 1);
	}
	var table = $('#pictures').DataTable({
		rowReorder: true,
		columns: tableColumes,
		language: {
			search: "Поиск",
			lengthMenu:    "Показать _MENU_ ",
			info:           "Показано с _START_ по _END_ из _TOTAL_",
			infoEmpty:      "Показано 0 из 0",
			infoFiltered:   "Показано 0 из 0",
			infoPostFix:    "",
			loadingRecords: "Загрузка",
			zeroRecords:    "Показано 0 из 0",
			emptyTable:     "Показано 0 из 0",
			paginate: {
				first:      "Первая",
				previous:   "Пред.",
				next:       "След.",
				last:       "Последняя"
			}
		}
	});

	addPictureObject();
	updatePictureText();
	updatePicturePicAndActive();
	removePicObject();
	updateUploadButtonBackground();
	updateTeamText();

	table.on('row-reorder', function (e, diff, edit) {
		var jsonForAjaxRequest = [];
		for (var i = 0, ien = diff.length; i < ien; i++) {
			var rowData = table.row(diff[i].node).data();
			jsonForAjaxRequest.push({id:rowData[1], ordering:diff[i].newData})
		}
		$.ajax({
			url: '/admin/pictures/ordering',
			type: "PUT",
			contentType : "application/json",
			dataType: 'json',
			data: JSON.stringify(jsonForAjaxRequest),
			success: function(result) {
				swal({
					position: 'top-right',
					type: 'success',
					title: 'Изменения сохранены',
					showConfirmButton: false,
					timer: 1500
				})
			},
			error: function(xhr, resp, text) {
				swal({
					position: 'top-right',
					type: 'error',
					title: 'Изменения не сохранены',
					showConfirmButton: false,
					timer: 1500
				})
			}
		})
	});

	$("#pictures_paginate").on('click',function () {
		updatePictureText();
		updatePicturePicAndActive();
		removePicObject();
	});

});
