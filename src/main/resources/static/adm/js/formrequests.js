function removeFormRequestObject() {
	$(".formrequest-service-del").click(function() {
		var parametrName=$(this).attr('name');
		var deleteUri = parametrName.substring(0, parametrName.indexOf("/end"));
		$.ajax({
			url: deleteUri,
			type: "DELETE",
			success: function(result) {
				swal({
					position: 'top-right',
					type: 'success',
					title: 'Изменения сохранены',
					showConfirmButton: false,
					timer: 1500
				});
				window.location.reload();
			},
			error: function(xhr, resp, text) {
				swal({
					position: 'top-right',
					type: 'error',
					title: 'Изменения не сохранены',
					showConfirmButton: false,
					timer: 1500
				})
			}
		})
	});
}

$(document).ready(function() {
	var table = $('#formrequests').DataTable({
		rowReorder: true,
		columns: [
			{className: "dt-center"},
			{className: "dt-center"},
			{className: "dt-center"},
			{className: "dt-center"},
			{className: "dt-center"},
			{className: "dt-center"},
			{className: "dt-center"},
			{className: "dt-center"}
		],
		language: {
			search: "Поиск",
			lengthMenu:    "Показать _MENU_ ",
			info:           "Показано с _START_ по _END_ из _TOTAL_",
			infoEmpty:      "Показано 0 из 0",
			infoFiltered:   "Показано 0 из 0",
			infoPostFix:    "",
			loadingRecords: "Загрузка",
			zeroRecords:    "Показано 0 из 0",
			emptyTable:     "Показано 0 из 0",
			paginate: {
				first:      "Первая",
				previous:   "Пред.",
				next:       "След.",
				last:       "Последняя"
			}
		}
	});

	removeFormRequestObject();

	$("#formrequests_paginate").on('click',function () {
		removeFormRequestObject();
	});

});
