/**
 * Created by No Fear on 09.10.2017.
 */

$(document).ready(function () {

    $('.menu-btn').click(function () {
        $('.header nav').fadeIn(300);
    });

    $('.menu-close').click(function () {
        $('.header nav').fadeOut(300);
    });

    new WOW({
        offset: 120
    }).init();

});

$(window).resize(function () {
    $('.header nav').fadeOut(300);
});

var lastScrollTop = 0;

$(window).scroll(function(){

    var st = $(this).scrollTop();
    if (st > lastScrollTop) {
        $('.header').removeClass('active');
    }
    else {
        $('.header').addClass('active');
    }
    if (st>50){
        $('.header').addClass('notop');
    }
    else {
        $('.header').removeClass('notop');
    }
    lastScrollTop = st;

});