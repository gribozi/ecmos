
/**
 * Send ajax form data to update method in controller
*/

$(function () {
	$("#form-store").on('submit', function (e) {
		e.preventDefault()
		var jsonFacility = {
			facilityContents: [
				{
					textSmall: $("#form-textSmall").val(),
					textBig: $("#form-textBig").val()
				}
			]
		}
		var dataForSend = new FormData();
		dataForSend.append("jsonFacility", JSON.stringify(jsonFacility));
		if ($("#form-pic")[0].files[0] != undefined) {
			dataForSend.append("facilityPic", $("#form-pic")[0].files[0]);
		}
		$.ajax({
			url: '/admin/facility',
			type: "PUT",
			enctype: 'multipart/form-data',
			processData: false,
			contentType: false,
			data: dataForSend,
			success: function(result) {
				swal({
					position: 'top-right',
					type: 'success',
					title: 'Изменения сохранены',
					showConfirmButton: false,
					timer: 1500
				});
				$("#form-textSmall").val(result.facilityContents[0].textSmall);
				$("#form-textBig").val(result.facilityContents[0].textBig);
				$("#form-label-pic").css("background-image","url(/images/facility/own-bg.jpg?random=" + new Date().getTime() + ')');
			},
			error: function(xhr, resp, text) {
				swal({
					position: 'top-right',
					type: 'error',
					title: 'Изменения не сохранены',
					showConfirmButton: false,
					timer: 1500
				})
			}
		})
	})
})
