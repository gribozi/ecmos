
/**
 * Send ajax form data to update method in controller
*/

function updateProjectSetting() {
	$("#project-settings").on('submit', function (e) {
		e.preventDefault();
		var jsonProject = {
			id: parseInt($("#project-id").attr("name")),
			typeProject: $("#form-typeProject").prop('checked'),
			typeObject: $("#form-typeObject").prop('checked'),
			typeRealtor: $("#form-typeRealtor").prop('checked'),
			indexOutput: $("#form-indexOutput").prop('checked'),
			objectType: parseInt($("#form-objectType").val()),
			active: $("#form-active").prop('checked'),
			dimensions: $("#form-dimensions").val(),
			square: $("#form-square").val(),
			coast: $("#form-coast").val(),
			coastAction: $("#form-coastAction").val(),
			actionBegin: $("#form-actionBegin").val(),
			actionEnd: $("#form-actionEnd").val(),
			video: $("#form-video").val(),
			projectContents: [
				{
					name: $("#form-name").val(),
					address: $("#form-address").val(),
					generalDescription: $("#form-generalDescription").val(),
					floor1description: $("#form-floor1description").val(),
					floor2description: $("#form-floor2description").val(),
					floor3description: $("#form-floor3description").val(),
					floor4description: $("#form-floor4description").val()
				}
			]
		};

		var dataForSend = new FormData();
		dataForSend.append("jsonProject", JSON.stringify(jsonProject));
		dataForSend.append("floor1Pic", $("#form-floor1")[0].files[0]);
		dataForSend.append("floor2Pic", $("#form-floor2")[0].files[0]);
		dataForSend.append("floor3Pic", $("#form-floor3")[0].files[0]);
		dataForSend.append("floor4Pic", $("#form-floor4")[0].files[0]);
		dataForSend.append("realtorPlanPic", $("#form-plan")[0].files[0]);
		dataForSend.append("projectPics", $("#form-projectPics")[0].files[0]);

		var httpMethod = "POST";
		var id = "";
		if ($('#project-id').length) {
			httpMethod = "PUT";
			id = jsonProject.id;
		}
		$.ajax({
			url: '/admin/project/' + id,
			type: httpMethod,
			enctype: 'multipart/form-data',
			processData: false,
			contentType: false,
			data: dataForSend,
			success: function(result) {
				swal({
					position: 'top-right',
					type: 'success',
					title: 'Изменения сохранены',
					showConfirmButton: false,
					timer: 1500
				});
				window.location="/admin/project/" + result.id;
			},
			error: function(xhr, resp, text) {
				swal({
					position: 'top-right',
					type: 'error',
					title: 'Изменения не сохранены',
					showConfirmButton: false,
					timer: 1500
				})
			}
		})
	})
};

function removeProjectPlans() {
	$(".project-plans").on('click', function(){
		var deleteUri = $(this).attr('name');
		$.ajax({
			url: deleteUri,
			type: "DELETE",
			success: function(result) {
				swal({
					position: 'top-right',
					type: 'success',
					title: 'Изменения сохранены',
					showConfirmButton: false,
					timer: 1500
				});
				window.location.reload();
			},
			error: function(xhr, resp, text) {
				swal({
					position: 'top-right',
					type: 'error',
					title: 'Изменения не сохранены',
					showConfirmButton: false,
					timer: 1500
				})
			}
		})
	})
}

function removeProjectPics() {
	$(".project-pics").on('click', function(){
		var deleteUri = $(this).attr('id');
		var actualId = deleteUri.substring(deleteUri.indexOf("/pic/") + 5);
		$.ajax({
			url: deleteUri,
			type: "DELETE",
			success: function(result) {
				$("#pic-row-" + actualId).remove();
				swal({
					position: 'top-right',
					type: 'success',
					title: 'Изменения сохранены',
					showConfirmButton: false,
					timer: 1500
				});
			},
			error: function(xhr, resp, text) {
				swal({
					position: 'top-right',
					type: 'error',
					title: 'Изменения не сохранены',
					showConfirmButton: false,
					timer: 1500
				})
			}
		})
	});
};

function updateUploadButtonBackground() {
	$(".image-inputfile").on('change', function(){
		var inputId = $(this).attr("id");
		var file = this.files[0];
		var reader = new FileReader();
		var labelId = "#label-"+inputId.substring(inputId.indexOf('-')+1);
		reader.onloadend = function () {
				$(labelId).css('background-image', 'url("' + reader.result + '")');
			};
		if (file) {
			reader.readAsDataURL(file);
		}
	});
};

$(document).ready(function() {

	updateProjectSetting();
	removeProjectPlans();
	removeProjectPics();
	updateUploadButtonBackground();

	var table = $('#project-pics').DataTable({
			rowReorder: true,
			columns: [
				{className: "dt-center"},
				{className: "dt-center"},
				{className: "dt-center"}
			],
			language: {
				search: "Поиск",
				lengthMenu:    "Показать _MENU_ ",
				info:           "Показано с _START_ по _END_ из _TOTAL_",
				infoEmpty:      "Показано 0 из 0",
				infoFiltered:   "Показано 0 из 0",
				infoPostFix:    "",
				loadingRecords: "Загрузка",
				zeroRecords:    "Показано 0 из 0",
				emptyTable:     "Показано 0 из 0",
				paginate: {
					first:      "Первая",
					previous:   "Пред.",
					next:       "След.",
					last:       "Последняя"
				}
			}
		});

	table.on('row-reorder', function (e, diff, edit) {
		var jsonForAjaxRequest = [];
		var projectId;
		for (var i = 0, ien = diff.length; i < ien; i++) {
			var rowData = table.row(diff[i].node).data();
			jsonForAjaxRequest.push({id:rowData[0], ordering:diff[i].newData})
		}
		if ($('#project-id').length) {
			projectId = parseInt($("#project-id").attr("name"))
			$.ajax({
				url: projectId + '/pic/ordering',
				type: "PUT",
				contentType : "application/json",
				dataType: 'json',
				data: JSON.stringify(jsonForAjaxRequest),
				success: function(result) {
					swal({
						position: 'top-right',
						type: 'success',
						title: 'Изменения сохранены',
						showConfirmButton: false,
						timer: 1500
					});
					window.location.reload();
				},
				error: function(xhr, resp, text) {
					swal({
						position: 'top-right',
						type: 'error',
						title: 'Изменения не сохранены',
						showConfirmButton: false,
						timer: 1500
					})
				}
			})
		}
	});

	$("#project-pics_paginate").on('click', function() {
		removeProjectPics();
	});

});
