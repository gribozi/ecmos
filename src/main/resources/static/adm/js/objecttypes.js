$(document).ready(function() {
	var table = $('#objecttypes').DataTable({
		rowReorder: true,
		columns: [
			{className: "dt-center"},
			{className: "dt-center"},
			{className: "dt-center"},
			{className: "dt-center"},
			{className: "dt-center"},
			{className: "dt-center"},
			{className: "dt-center"},
			{className: "dt-center"}
		],
		language: {
			search: "Поиск",
			lengthMenu:    "Показать _MENU_ ",
			info:           "Показано с _START_ по _END_ из _TOTAL_",
			infoEmpty:      "Показано 0 из 0",
			infoFiltered:   "Показано 0 из 0",
			infoPostFix:    "",
			loadingRecords: "Загрузка",
			zeroRecords:    "Показано 0 из 0",
			emptyTable:     "Показано 0 из 0",
			paginate: {
				first:      "Первая",
				previous:   "Пред.",
				next:       "След.",
				last:       "Последняя"
			}
		}
	});
	table.on('row-reorder', function (e, diff, edit) {
		var jsonForAjaxRequest = [];
		for (var i=0, ien=diff.length; i < ien; i++) {
			var rowData = table.row(diff[i].node).data();
			jsonForAjaxRequest.push({id:rowData[1], ordering:diff[i].newData})
		}
		$.ajax({
			url: '/admin/objecttypes/ordering',
			type: "PUT",
			contentType : "application/json",
			dataType: 'json',
			data: JSON.stringify(jsonForAjaxRequest),
			success: function(result) {
				swal({
					position: 'top-right',
					type: 'success',
					title: 'Изменения сохранены',
					showConfirmButton: false,
					timer: 1500
				})
			},
			error: function(xhr, resp, text) {
				swal({
					position: 'top-right',
					type: 'error',
					title: 'Изменения не сохранены',
					showConfirmButton: false,
					timer: 1500
				})
			}
		})
	});
});
