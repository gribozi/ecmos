function addFaqObject() {
	$("#faq-service-add").click(function(e) {
		e.preventDefault();
		var createUri = $(this).attr('name');
		var jsonForAjaxRequest = {
			active: $("#faq-new-active").prop('checked'),
			faqContents: [
				{
					question: $("#faq-new-question").val(),
					answer: $("#faq-new-answer").val()
				}
			]
		}
		$.ajax({
			url: createUri,
			type: "POST",
			contentType : "application/json",
			dataType: 'json',
			data: JSON.stringify(jsonForAjaxRequest),
			success: function(result) {
				swal({
					position: 'top-right',
					type: 'success',
					title: 'Изменения сохранены',
					showConfirmButton: false,
					timer: 1500
				});
				console.log("ok");
				window.location.reload();
			},
			error: function(xhr, resp, text) {
				swal({
					position: 'top-right',
					type: 'error',
					title: 'Изменения не сохранены',
					showConfirmButton: false,
					timer: 1500
				})
			}
		})
	});
}

function updateFaqText() {
	$(".faq-service-text").on('blur', function(e) {
		e.preventDefault();
		var parametrName=$(this).attr('name');
		var updateUri = parametrName.substring(0, parametrName.indexOf("/end"));
		var currentId = parametrName.substring(parametrName.indexOf("/") + 1, parametrName.indexOf("/end"));
		var jsonForAjaxRequest = {
			id: parseInt(currentId),
			active: $("#faq-active-" + currentId).prop('checked'),
			ordering: parseInt($("#faq-ordering-" + currentId).html()),
			faqContents: [
				{
					question: $("#faq-question-" + currentId)[0].innerHTML,
					answer: $("#faq-answer-" + currentId)[0].innerHTML
				}
			]
		}
		$.ajax({
			url: updateUri,
			type: "PUT",
			contentType : "application/json",
			dataType: 'json',
			data: JSON.stringify(jsonForAjaxRequest),
			success: function(result) {
				console.log("ok");
				window.location.reload();
			},
			error: function(xhr, resp, text) {
				swal({
					position: 'top-right',
					type: 'error',
					title: 'Изменения не сохранены',
					showConfirmButton: false,
					timer: 1500
				})
			}
		})
	});
}

function updateFaqPicAndActive() {
	$(".faq-service-active").change(function(e) {
		e.preventDefault();
		var parametrName=$(this).attr('name');
		var updateUri = parametrName.substring(0, parametrName.indexOf("/end"));
		var currentId = parametrName.substring(parametrName.indexOf("/") + 1, parametrName.indexOf("/end"));
		var jsonForAjaxRequest = {
			id: parseInt(currentId),
			active: $("#faq-active-" + currentId).prop('checked'),
			ordering: parseInt($("#faq-ordering-" + currentId).html()),
			faqContents: [
				{
					question: $("#faq-question-" + currentId)[0].innerHTML,
					answer: $("#faq-answer-" + currentId)[0].innerHTML
				}
			]
		}
		$.ajax({
			url: updateUri,
			type: "PUT",
			contentType : "application/json",
			dataType: 'json',
			data: JSON.stringify(jsonForAjaxRequest),
			success: function(result) {
				console.log("ok");
				window.location.reload();
			},
			error: function(xhr, resp, text) {
				swal({
					position: 'top-right',
					type: 'error',
					title: 'Изменения не сохранены',
					showConfirmButton: false,
					timer: 1500
				})
			}
		})
	});
}

function removeFaqObject() {
	$(".faq-service-del").click(function() {
		var parametrName=$(this).attr('name');
		var removeUri = parametrName.substring(0, parametrName.indexOf("/end"));
		$.ajax({
			url: removeUri,
			type: "DELETE",
			success: function(result) {
				swal({
					position: 'top-right',
					type: 'success',
					title: 'Изменения сохранены',
					showConfirmButton: false,
					timer: 1500
				});
				window.location.reload();
			},
			error: function(xhr, resp, text) {
				swal({
					position: 'top-right',
					type: 'error',
					title: 'Изменения не сохранены',
					showConfirmButton: false,
					timer: 1500
				})
			}
		})
	});
}

$(document).ready(function() {
	var table = $('#faqs').DataTable({
		rowReorder: true,
		columns: [
			{className: "dt-center"},
			{className: "dt-center"},
			{className: "dt-center"},
			{className: "dt-center"},
			{className: "dt-center"},
			{className: "dt-center"}
		],
		language: {
			search: "Поиск",
			lengthMenu:    "Показать _MENU_ ",
			info:           "Показано с _START_ по _END_ из _TOTAL_",
			infoEmpty:      "Показано 0 из 0",
			infoFiltered:   "Показано 0 из 0",
			infoPostFix:    "",
			loadingRecords: "Загрузка",
			zeroRecords:    "Показано 0 из 0",
			emptyTable:     "Показано 0 из 0",
			paginate: {
				first:      "Первая",
				previous:   "Пред.",
				next:       "След.",
				last:       "Последняя"
			}
		}
	});

	addFaqObject();
	updateFaqText();
	updateFaqPicAndActive();
	removeFaqObject();

	table.on('row-reorder', function (e, diff, edit) {
		var jsonForAjaxRequest = [];
		for (var i=0, ien=diff.length; i < ien; i++) {
			var rowData = table.row(diff[i].node).data();
			jsonForAjaxRequest.push({id:rowData[1], ordering:diff[i].newData})
		}
		$.ajax({
			url: '/admin/faqs/ordering',
			type: "PUT",
			contentType : "application/json",
			dataType: 'json',
			data: JSON.stringify(jsonForAjaxRequest),
			success: function(result) {
				swal({
					position: 'top-right',
					type: 'success',
					title: 'Изменения сохранены',
					showConfirmButton: false,
					timer: 1500
				})
			},
			error: function(xhr, resp, text) {
				swal({
					position: 'top-right',
					type: 'error',
					title: 'Изменения не сохранены',
					showConfirmButton: false,
					timer: 1500
				})
			}
		})
	});

	$("#faqs_paginate").on('click',function () {
		updateFaqText();
		updateFaqPicAndActive();
		removeFaqObject();
	});

});
