function addPriceObject() {
	$("#price-service-add").click(function(e) {
		e.preventDefault();
		var createUri = $(this).attr('name');
		var jsonPrice = {
			active: $("#price-new-active").prop('checked'),
			priceContents: [
				{
					name: $("#price-new-name").val()
				}
			]
		}
		var dataForSend = new FormData();
		dataForSend.append("jsonPrice", JSON.stringify(jsonPrice));
		dataForSend.append("priceFile", $("#price-new-file")[0].files[0]);
		$.ajax({
			url: createUri,
			type: "POST",
			enctype: 'multipart/form-data',
			processData: false,
			contentType: false,
			data: dataForSend,
			success: function(result) {
				swal({
					position: 'top-right',
					type: 'success',
					title: 'Изменения сохранены',
					showConfirmButton: false,
					timer: 1500
				});
				console.log("ok");
				window.location.reload();
			},
			error: function(xhr, resp, text) {
				swal({
					position: 'top-right',
					type: 'error',
					title: 'Изменения не сохранены',
					showConfirmButton: false,
					timer: 1500
				})
			}
		})
	});
}

function updatePriceText() {
	$(".price-service-text").on('blur', function(e) {
		e.preventDefault();
		var parametrName=$(this).attr('name');
		var updateUri = parametrName.substring(0, parametrName.indexOf("/end"));
		var currentId = parametrName.substring(parametrName.indexOf("/") + 1, parametrName.indexOf("/end"));
		var jsonPrice = {
			id: parseInt(currentId),
			active: $("#price-active-" + currentId).prop('checked'),
			ordering: parseInt($("#price-ordering-" + currentId).html()),
			priceContents: [
				{
					name: $("#price-name-" + currentId)[0].innerHTML
				}
			]
		}
		var dataForSend = new FormData();
		dataForSend.append("jsonPrice", JSON.stringify(jsonPrice));
		$.ajax({
			url: updateUri,
			type: "PUT",
			enctype: 'multipart/form-data',
			processData: false,
			contentType: false,
			data: dataForSend,
			success: function(result) {
				console.log("ok");
				window.location.reload();
			},
			error: function(xhr, resp, text) {
				swal({
					position: 'top-right',
					type: 'error',
					title: 'Изменения не сохранены',
					showConfirmButton: false,
					timer: 1500
				})
			}
		})
	});
}

function updatePriceActive() {
	$(".price-service-active").change(function(e) {
		e.preventDefault();
		var parametrName=$(this).attr('name');
		var updateUri = parametrName.substring(0, parametrName.indexOf("/end"));
		var currentId = parametrName.substring(parametrName.indexOf("/") + 1, parametrName.indexOf("/end"));
		var jsonPrice = {
			id: parseInt(currentId),
			active: $("#price-active-" + currentId).prop('checked'),
			ordering: parseInt($("#price-ordering-" + currentId).html()),
			priceContents: [
				{
					name: $("#price-name-" + currentId)[0].innerHTML
				}
			]
		}
		var dataForSend = new FormData();
		dataForSend.append("jsonPrice", JSON.stringify(jsonPrice));
		$.ajax({
			url: updateUri,
			type: "PUT",
			enctype: 'multipart/form-data',
			processData: false,
			contentType: false,
			data: dataForSend,
			success: function(result) {
				console.log("ok");
				window.location.reload();
			},
			error: function(xhr, resp, text) {
				swal({
					position: 'top-right',
					type: 'error',
					title: 'Изменения не сохранены',
					showConfirmButton: false,
					timer: 1500
				})
			}
		})
	});
};

function removePriceObject() {
	$(".price-service-del").click(function() {
		var parametrName=$(this).attr('name');
		var removeUri = parametrName.substring(0, parametrName.indexOf("/end"));
		$.ajax({
			url: removeUri,
			type: "DELETE",
			success: function(result) {
				swal({
					position: 'top-right',
					type: 'success',
					title: 'Изменения сохранены',
					showConfirmButton: false,
					timer: 1500
				});
				window.location.reload();
			},
			error: function(xhr, resp, text) {
				swal({
					position: 'top-right',
					type: 'error',
					title: 'Изменения не сохранены',
					showConfirmButton: false,
					timer: 1500
				})
			}
		})
	});
}

$(document).ready(function() {
	var table = $('#prices').DataTable({
		rowReorder: true,
		columns: [
			{className: "dt-center"},
			{className: "dt-center"},
			{className: "dt-center"},
			{className: "dt-center"},
			{className: "dt-center"}
		],
		language: {
			search: "Поиск",
			lengthMenu:    "Показать _MENU_ ",
			info:           "Показано с _START_ по _END_ из _TOTAL_",
			infoEmpty:      "Показано 0 из 0",
			infoFiltered:   "Показано 0 из 0",
			infoPostFix:    "",
			loadingRecords: "Загрузка",
			zeroRecords:    "Показано 0 из 0",
			emptyTable:     "Показано 0 из 0",
			paginate: {
				first:      "Первая",
				previous:   "Пред.",
				next:       "След.",
				last:       "Последняя"
			}
		}
	});

	addPriceObject();
	updatePriceText();
	updatePriceActive();
	removePriceObject();

	table.on('row-reorder', function (e, diff, edit) {
		var jsonForAjaxRequest = [];
		for (var i=0, ien=diff.length; i < ien; i++) {
			var rowData = table.row(diff[i].node).data();
			jsonForAjaxRequest.push({id:rowData[1], ordering:diff[i].newData})
		}
		$.ajax({
			url: '/admin/prices/ordering',
			type: "PUT",
			contentType : "application/json",
			dataType: 'json',
			data: JSON.stringify(jsonForAjaxRequest),
			success: function(result) {
				swal({
					position: 'top-right',
					type: 'success',
					title: 'Изменения сохранены',
					showConfirmButton: false,
					timer: 1500
				})
			},
			error: function(xhr, resp, text) {
				swal({
					position: 'top-right',
					type: 'error',
					title: 'Изменения не сохранены',
					showConfirmButton: false,
					timer: 1500
				})
			}
		})
	});

	$("#prices_paginate").on('click',function () {
		updatePriceText();
		updatePriceActive();
		removePriceObject();
	});

});
