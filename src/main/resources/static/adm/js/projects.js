function removeProjectsObject() {
	$(".projects-rows").on('click', function(){
		var removeUri = $(this).attr('id');
		var actualId = removeUri.substring(removeUri.indexOf("ject/") + 5);
		$.ajax({
			url: removeUri,
			type: "DELETE",
			success: function(result) {
				swal({
					position: 'top-right',
					type: 'success',
					title: 'Изменения сохранены',
					showConfirmButton: false,
					timer: 1500
				});
				window.location.reload();
				//$("#proj-row-" + actualId).remove();
				//$('#projects').ajax.reload();
			},
			error: function(xhr, resp, text) {
				swal({
					position: 'top-right',
					type: 'error',
					title: 'Изменения не сохранены',
					showConfirmButton: false,
					timer: 1500
				})
			}
		})
	});
};

$(document).ready(function() {
	var table = $('#projects').DataTable({
		rowReorder: true,
		columns: [
			{className: "dt-center"},
			{className: "dt-center"},
			{className: "dt-center"},
			{className: "dt-center"},
			{className: "dt-center"},
			{className: "dt-center"},
			{className: "dt-center"}
		],
		language: {
			search: "Поиск",
			lengthMenu:    "Показать _MENU_ ",
			info:           "Показано с _START_ по _END_ из _TOTAL_",
			infoEmpty:      "Показано 0 из 0",
			infoFiltered:   "Показано 0 из 0",
			infoPostFix:    "",
			loadingRecords: "Загрузка",
			zeroRecords:    "Показано 0 из 0",
			emptyTable:     "Показано 0 из 0",
			paginate: {
				first:      "Первая",
				previous:   "Пред.",
				next:       "След.",
				last:       "Последняя"
			}
		}
	});

	removeProjectsObject();

	table.on('row-reorder', function (e, diff, edit) {
		var jsonForAjaxRequest = [];
		for (var i=0, ien=diff.length; i < ien; i++) {
			var rowData = table.row(diff[i].node).data();
			jsonForAjaxRequest.push({id:rowData[2], ordering:diff[i].newData})
		}
		$.ajax({
			url: '/admin/projects/ordering',
			type: "PUT",
			contentType : "application/json",
			dataType: 'json',
			data: JSON.stringify(jsonForAjaxRequest),
			success: function(result) {
				swal({
					position: 'top-right',
					type: 'success',
					title: 'Изменения сохранены',
					showConfirmButton: false,
					timer: 1500
				})
			},
			error: function(xhr, resp, text) {
				swal({
					position: 'top-right',
					type: 'error',
					title: 'Изменения не сохранены',
					showConfirmButton: false,
					timer: 1500
				})
			}
		})
	});

	$("#projects_paginate").on('click',function () {
		removeProjectsObject();
	});

});

