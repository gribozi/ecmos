
/**
 * Send ajax form data to save method in controller
*/

$(function () {
	$("#form-store").on('submit', function (e) {
		e.preventDefault()
		var jsonForAjaxRequest = {
			contactContents: [
				{
					infoTitle: $("#form-infoTitle").val(),
					infoAddress: $("#form-infoAddress").val(),
					infoWorking: $("#form-infoWorking").val(),
					infoPhones: $("#form-infoPhones").val()
				}
			],
			phone: $("#form-phone").val(),
			emailTo: $("#form-emailTo").val(),
			smFb: $("#form-smFb").val(),
			smVk: $("#form-smVk").val(),
			smYt: $("#form-smYt").val(),
			smIn: $("#form-smIn").val(),
			adressLng: longitude,
			adressLtg: latitude
		}
		$.ajax({
			url: '/admin/contacts',
			type: "PUT",
			contentType: "application/json",
			dataType: 'json',
			data: JSON.stringify(jsonForAjaxRequest),
			success: function(result) {
				swal({
					position: 'top-right',
					type: 'success',
					title: 'Изменения сохранены',
					showConfirmButton: false,
					timer: 1500
				});
				$("#form-phone").val(result.phone),
				$("#form-emailTo").val(result.emailTo),
				$("#form-smFb").val(result.smFb),
				$("#form-smVk").val(result.smVk),
				$("#form-smYt").val(result.smYt),
				$("#form-smIn").val(result.smIn);
				$("#form-infoTitle").val(result.contactContents[0].infoTitle);
				$("#form-infoAddress").val(result.contactContents[0].infoAddress);
				$("#form-infoWorking").val(result.contactContents[0].infoWorking);
				$("#form-infoPhones").val(result.contactContents[0].infoPhones);
			},
			error: function(xhr, resp, text) {
				swal({
					position: 'top-right',
					type: 'error',
					title: 'Изменения не сохранены',
					showConfirmButton: false,
					timer: 1500
				})
			}
		})
	})
})
