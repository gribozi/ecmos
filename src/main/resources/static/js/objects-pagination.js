function showPaginationResult(urlPath, lastPaginationPosition) {
	$.ajax({
		url: urlPath,
		type: "POST",
		data: {
			lastPaginationPosition:lastPaginationPosition
		},
		success: function(result) {
			$('#objects-paginate').remove();
			$('#objects-list').append(result);
			$("#objects-paginate").on('click', function(event){
				event.preventDefault();
				var urlPath = $("#objects-list > ul > li.active > a").attr("href");
				var lastPaginationPosition = parseInt($(this).attr("name"));
				showPaginationResult(urlPath, lastPaginationPosition);
			});
		},
		error: function(xhr, resp, text) {
			console.log("err")
		}
	})
}

$(function() {
	$("#objects-paginate").on('click', function(event) {
		event.preventDefault();
		var urlPath = $("#objects-list > ul > li.active > a").attr("href");
		var lastPaginationPosition = parseInt($(this).attr("name"));
		showPaginationResult(urlPath, lastPaginationPosition);
	});
})
