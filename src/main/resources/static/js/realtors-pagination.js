function showPaginationResult(lastPaginationPosition) {
	$.ajax({
		url: '/realtors',
		type: "POST",
		data: {
			lastPaginationPosition:lastPaginationPosition
		},
		success: function(result) {
			$('#realtors-paginate').remove();
			$('#realtors-list').append(result);
			$("#realtors-paginate").on('click', function(event){
				event.preventDefault();
				var lastPaginationPosition = parseInt($(this).attr("name"));
				showPaginationResult(lastPaginationPosition);
			});
		},
		error: function(xhr, resp, text) {
			console.log("err")
		}
	})
}

$(function() {
	$("#realtors-paginate").on('click', function(event) {
		event.preventDefault();
		var lastPaginationPosition = parseInt($(this).attr("name"));
		showPaginationResult(lastPaginationPosition);
	});
})
