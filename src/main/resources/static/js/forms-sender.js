/**
 * Send ajax form data to controller
*/

function sendForm() {
	$(".modal-form").on('submit', function (e) {
		e.preventDefault();
		var divId = "#" + $(this).attr('id');
		if (formValidate(divId)) {
			
			if ($(divId + ' .btn').attr("data-form-text").length) {
				formForAjaxRequest = $(divId + ' .btn').attr("data-form-text");
			} else {
				formForAjaxRequest = "";
			}
			if ($(divId + ' .form-name').length) {
				nameForAjaxRequest = $(divId + ' .form-name').val();
			} else {
				nameForAjaxRequest = "";
			}
			if ($(divId + ' .form-phone').length) {
				phoneForAjaxRequest = $(divId + ' .form-phone').val();
			} else {
				phoneForAjaxRequest = "";
			}
			if ($(divId + ' .form-email').length) {
				emailForAjaxRequest = $(divId + ' .form-email').val();
			} else {
				emailForAjaxRequest = "";
			}
			if ($(divId + ' .form-text').length) {
				textForAjaxRequest = $(divId + ' .form-text').val();
			} else {
				textForAjaxRequest = nameForAjaxRequest + " " + phoneForAjaxRequest;
			}

			$.ajax({
				url: '/form',
				type: "POST",
				data: {
					form: formForAjaxRequest,
					name: nameForAjaxRequest,
					phone: phoneForAjaxRequest,
					email: emailForAjaxRequest,
					text: textForAjaxRequest
				},
				success: function(result) {
					$('.modal').modal('hide');
					$(divId + ' .form-name').val('');
					$(divId + ' .form-phone').val('');
					$(divId + ' .form-email').val('');
					$(divId + ' .form-text').val('');
					if (divId == "#modal-price") {
						var file_path = 'prices/price.pdf';
						var a = document.createElement('A');
						a.href = file_path;
						a.download = file_path.substr(file_path.lastIndexOf('/') + 1);
						document.body.appendChild(a);
						a.click();
						document.body.removeChild(a);
					}
					$('#modal-thankyou').modal('show');
				},
				error: function(xhr, resp, text) {
					$('.modal').modal('hide');
					console.log("err")
				}
			})
		}
	})
}

$(document).ready(function() {
	sendForm();
})

