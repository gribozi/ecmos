function showPaginationResult(lastPaginationPosition) {
	var floorDrop;
	var minSquareI;
	var maxSquareI;
	if (($("#floor-select").val() == null) || ($("#floor-select").val() == "Кол-во этажей") || ($("#floor-select").val() == "Показать все") ) {
		floorDrop = 0;
	} else {
		floorDrop = parseInt($("#floor-select").val());
	}
	if ($("#floor-select").val() == "Показать все") {
		minSquareI = 0;
		maxSquareI = 99999;
	} else {
		minSquareI = parseInt($('#range-slider').val().substring(0, $('#range-slider').val().indexOf(";")));
		maxSquareI = parseInt($('#range-slider').val().substring($('#range-slider').val().indexOf(";") + 1, $('#range-slider').val().length));
	}

	$.ajax({
		url: '/projects',
		type: "POST",
		data: {
			floor: floorDrop,
			minSquare: minSquareI,
			maxSquare: maxSquareI,
			lastPaginationPosition:lastPaginationPosition
		},
		success: function(result) {
			$('#projects-paginate-button-block').remove();
			$('#projects-list').append(result);
			$('#projects-paginate-button-block').detach().insertAfter("#projects-list");
			$("#projects-paginate").on('click', function(event){
				event.preventDefault();
				var lastPaginationPosition = parseInt($(this).attr("name"));
				showPaginationResult(lastPaginationPosition);
			});
		},
		error: function(xhr, resp, text) {
			console.log("err")
		}
	})
}

$(function() {
	$('#projects-paginate-button-block').detach().insertAfter("#projects-list");
	$("#projects-paginate").on('click', function(event) {
		event.preventDefault();
		var lastPaginationPosition = parseInt($(this).attr("name"));
		showPaginationResult(lastPaginationPosition);
	});
	$("#projects-filter").on('submit', function (e) {
		$('#projects-list').empty();
		event.preventDefault();
		var lastPaginationPosition = 0;
		showPaginationResult(lastPaginationPosition);
	});
})
