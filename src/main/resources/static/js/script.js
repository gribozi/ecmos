/**
 * Created by No Fear on 09.10.2017.
 */

$(document).ready(function () {

    $('.menu-btn').click(function () {
        $('.header nav').fadeIn(300);
    });

    $('.menu-close').click(function () {
        $('.header nav').fadeOut(300);
    });

    new WOW({
        offset: 120
    }).init();

    $('.partners .build-list, .team .build-list').each(function () {
        var i = 0;
        $(this).children('li').each(function () {
            i += 1
        });
        if(i==0){
            $('.build-list').addClass('hidden');
        }
        else if(i==1){
            $('.build-list li:nth-of-type(1)').before('<li class="b-l-hide-mob"><p class="b-o"></p></li>').after('<li class="b-l-hide-mob"><p class="b-g"></p></li><li class="b-l-hide"></li><li class="b-l-hide"><p class="b-o"></p></li><li class="b-l-hide-mob"><p class="b-g"></p></li><li class="b-l-hide-mob"><p class="f-o"></p></li>');
        }
        else if(i==2){
            $('.build-list li:nth-of-type(1)').before('<li class="b-l-hide-mob"><p class="b-o"></p></li>');
            $('.build-list li:nth-of-type(3)').after('<li class="b-l-hide"></li><li class="b-l-hide"><p class="b-o"></p></li><li class="b-l-hide-mob"><p class="b-g"></p></li><li class="b-l-hide-mob"><p class="f-o"></p></li>');
        }
        else if(i==3){
            $('.build-list li:nth-of-type(1)').before('<li class="b-l-hide-mob"><p class="b-o"></p></li>');
            $('.build-list li:nth-of-type(3)').after('<li class="b-l-hide"></li><li class="b-l-hide"><p class="b-o"></p></li>');
            $('.build-list li:nth-of-type(6)').after('<li class="b-l-hide-mob"><p class="f-o"></p></li>');
        }
        else if(i==4){
            $('.build-list li:nth-of-type(1)').before('<li class="b-l-hide-mob"><p class="b-o"></p></li>');
            $('.build-list li:nth-of-type(3)').after('<li class="b-l-hide"></li><li class="b-l-hide"><p class="b-o"></p></li>');
            $('.build-list li:nth-of-type(6)').after('<li class="b-l-hide-mob"><p class="f-o"></p></li><li class="b-l-hide-mob"><p class="f-o"></p></li>');
            $('.build-list li:nth-of-type(9)').after('<li class="b-l-hide-mob"><p class="b-g"></p></li><li class="b-l-hide-mob"><p class="b-o"></p></li>');
        }
        else if(i==5){
            $('.build-list li:nth-of-type(1)').before('<li class="b-l-hide-mob"><p class="b-o"></p></li>');
            $('.build-list li:nth-of-type(3)').after('<li class="b-l-hide"></li><li class="b-l-hide"><p class="b-o"></p></li>');
            $('.build-list li:nth-of-type(6)').after('<li class="b-l-hide-mob"><p class="f-o"></p></li><li class="b-l-hide-mob"><p class="f-o"></p></li>');
            $('.build-list li:nth-of-type(10)').after('<li class="b-l-hide-mob"><p class="b-o"></p></li>');
        }
        else if(i==6){
            $('.build-list li:nth-of-type(1)').before('<li class="b-l-hide-mob"><p class="b-o"></p></li>');
            $('.build-list li:nth-of-type(3)').after('<li class="b-l-hide"></li><li class="b-l-hide"><p class="b-o"></p></li>');
            $('.build-list li:nth-of-type(6)').after('<li class="b-l-hide-mob"><p class="f-o"></p></li><li class="b-l-hide-mob"><p class="f-o"></p></li>');
            $('.build-list li:nth-of-type(10)').after('<li class="b-l-hide-mob"><p class="b-o"></p></li><li class="b-l-hide-mob"><p class="f-g b-l-hide"></p></li><li class="b-l-hide-inverse"><p class="b-o"></p></li>');
            $('.build-list li:nth-of-type(14)').after('<li class="b-l-hide-mob"><p class="f-o"></p></li><li class="b-l-hide-inverse"></li>');
        }
        else if(i==7){
            $('.build-list li:nth-of-type(1)').before('<li class="b-l-hide-mob"><p class="b-o"></p></li>');
            $('.build-list li:nth-of-type(3)').after('<li class="b-l-hide"></li><li class="b-l-hide"><p class="b-o"></p></li>');
            $('.build-list li:nth-of-type(6)').after('<li class="b-l-hide-mob"><p class="f-o"></p></li><li class="b-l-hide-mob"><p class="f-o"></p></li>');
            $('.build-list li:nth-of-type(10)').after('<li class="b-l-hide-mob"><p class="b-o"></p></li><li class="b-l-hide-mob"><p class="f-g b-l-hide"></p></li><li class="b-l-hide-inverse"><p class="b-o"></p></li>');
            $('.build-list li:nth-of-type(14)').after('<li class="b-l-hide-mob"><p class="f-o"></p></li><li class="b-l-hide-inverse"></li><li class="b-l-hide-mob"></li>');
            $('.build-list li:nth-of-type(18)').after('<li class="b-l-hide-mob"><p class="b-g"></p></li>');
        }
        else if(i==8){
            $('.build-list li:nth-of-type(1)').before('<li class="b-l-hide-mob"><p class="b-o"></p></li>');
            $('.build-list li:nth-of-type(3)').after('<li class="b-l-hide"></li><li class="b-l-hide"><p class="b-o"></p></li>');
            $('.build-list li:nth-of-type(6)').after('<li class="b-l-hide-mob"><p class="f-o"></p></li><li class="b-l-hide-mob"><p class="f-o"></p></li>');
            $('.build-list li:nth-of-type(10)').after('<li class="b-l-hide-mob"><p class="b-o"></p></li><li class="b-l-hide-mob"><p class="f-g b-l-hide"></p></li><li class="b-l-hide-inverse"><p class="b-o"></p></li>');
            $('.build-list li:nth-of-type(14)').after('<li class="b-l-hide-mob"><p class="f-o"></p></li><li class="b-l-hide-inverse"></li><li class="b-l-hide-mob"></li>');
        }
    });

    $('.project-info img, #realtors-list img, .sertificates img').click(function(){
        var img = $(this).clone();
        $(img).on('load',function(){
            var block = document.createElement('div');
            block.className = "img-fullsize";
            $('body').append(block);
            $(block).append(img).fadeIn(300).click(function(){
                $(this).fadeOut(300);
                setTimeout(function(){
                    $(block).detach();
                },500);
            });
        });
    });

});

$(window).resize(function () {
    $('.header nav').fadeOut(300);
});

var lastScrollTop = 0;

$(window).scroll(function(){

    var st = $(this).scrollTop();
    if (st > lastScrollTop) {
        $('.header').removeClass('active');
    }
    else {
        $('.header').addClass('active');
    }
    if (st>50){
        $('.header').addClass('notop');
    }
    else {
        $('.header').removeClass('notop');
    }
    lastScrollTop = st;

});