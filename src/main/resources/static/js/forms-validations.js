/**
 * Validation of form data from user
*/

function formValidate (divId) {

	var inputs = $(divId + ' input');

	for (var i = 0; i < inputs.length; i++) {

		var input = inputs[i];

		if (input.classList.contains("form-name")) {
			if ((input.validity.valueMissing) & !$("#name-not-exist").length) {
				$(divId + " .form-name").after('<p id="name-not-exist" style="color:red;">Это поле обязательное</p>');
			}
			if (!input.validity.valueMissing) {
				$("#name-not-exist").remove();
			}
		}

		if (input.classList.contains("form-phone")) {
			if ((input.validity.valueMissing) & !$("#phone-not-exist").length) {
				$(divId + " .form-phone").after('<p id="phone-not-exist" style="color:red;">Это поле обязательное</p>');
			}
			if (!input.validity.valueMissing) {
				$("#phone-not-exist").remove();
			}
			if ((input.validity.patternMismatch) & !$("#phone-wrong-pattern").length) {
				$(divId + " .form-phone").after('<p id="phone-wrong-pattern" style="color:red;">Допустимы только цифры</p>');
			}
			if (!input.validity.patternMismatch) {
				$("#phone-wrong-pattern").remove();
			}
		}

		if (input.classList.contains("form-email")) {
			if ((input.validity.valueMissing) & !$("#email-not-exist").length) {
				$(divId + " .form-email").after('<p id="email-not-exist" style="color:red;">Это поле обязательное</p>');
			}
			if (!input.validity.valueMissing) {
				$("#email-not-exist").remove();
			}
			if ((input.validity.typeMismatch) & !$("#email-wrong-type").length) {
				$(divId + " .form-email").after('<p id="email-wrong-type" style="color:red;">Не корректный email</p>');
			}
			if (!input.validity.typeMismatch) {
				$("#email-wrong-type").remove();
			}
		}

		if (input.classList.contains("form-text")) {
			if ((input.validity.valueMissing) & !$("#text-not-exist").length) {
				$(divId + " .form-text").after('<p id="text-not-exist" style="color:red;">Это поле обязательное</p>');
			}
			if (!input.validity.valueMissing) {
				$("#text-not-exist").remove();
			}
		}
		
	}

	var textareas = $(divId + ' textarea');
	for (var i = 0; i < textareas.length; i++) {

		var textarea = textareas[i];

		if (textarea.classList.contains("form-text")) {
			if ((textarea.validity.valueMissing) & !$("#text-not-exist").length) {
				$(divId + " .form-text").after('<p id="text-not-exist" style="color:red;">Это поле обязательное</p>');
			}
			if (!textarea.validity.valueMissing) {
				$("#text-not-exist").remove();
			}
		}
	}

	if (
			!$("#name-not-exist").length & 
			!$("#phone-not-exist").length & 
			!$("#phone-wrong-pattern").length &
			!$("#email-not-exist").length &
			!$("#email-wrong-type").length &
			!$("#text-not-exist").length
		) {
		return true;
	} else {
		return false;
	}
}

