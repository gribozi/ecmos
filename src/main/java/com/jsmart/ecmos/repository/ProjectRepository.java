package com.jsmart.ecmos.repository;

import java.util.List;

import com.jsmart.ecmos.entity.ObjectType;
import com.jsmart.ecmos.entity.Project;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * @author Nikolay Koretskyy
 *
 */
@Repository
public interface ProjectRepository extends JpaRepository<Project, Integer> {
	List<Project> findByTypeProject(boolean typeProject);
	List<Project> findByTypeObject(boolean typeObject);
	List<Project> findByTypeRealtor(boolean typeRealtor);
	List<Project> findByTypeProjectAndActiveOrderByOrderingAsc(boolean typeProject, boolean active);
	List<Project> findByTypeObjectAndActiveOrderByOrderingAsc(boolean typeObject, boolean active);
	List<Project> findByTypeRealtorAndActiveOrderByOrderingAsc(boolean typeRealtor, boolean active);
	List<Project> findByTypeObjectAndActiveAndObjectTypeOrderByOrderingAsc(boolean typeObject, boolean active, ObjectType objectType);
	List<Project> findByTypeProjectAndActiveAndIndexOutputOrderByOrderingAsc(boolean typeObject, boolean active, boolean indexOutput);
}
