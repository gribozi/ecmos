package com.jsmart.ecmos.repository;

import com.jsmart.ecmos.entity.ForInvestors;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * @author Nikolay Koretskyy
 *
 */
@Repository
public interface ForInvestorsRepository extends JpaRepository<ForInvestors, Integer> {
}
