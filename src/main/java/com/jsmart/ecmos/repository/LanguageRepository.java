package com.jsmart.ecmos.repository;

import com.jsmart.ecmos.entity.Language;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * @author Nikolay Koretskyy
 *
 */
@Repository
public interface LanguageRepository extends JpaRepository<Language, Integer> {
	Language findByName(String name);
}
