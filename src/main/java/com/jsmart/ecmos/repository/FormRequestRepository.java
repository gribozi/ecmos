package com.jsmart.ecmos.repository;

import com.jsmart.ecmos.entity.FormRequest;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * @author Sergey Khomich
 *
 */
@Repository
public interface FormRequestRepository extends JpaRepository<FormRequest, Integer> {
}
