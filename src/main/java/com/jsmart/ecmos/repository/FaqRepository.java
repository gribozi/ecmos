package com.jsmart.ecmos.repository;

import java.util.List;

import com.jsmart.ecmos.entity.Faq;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * @author Sergey Khomich
 *
 */
@Repository
public interface FaqRepository extends JpaRepository<Faq, Integer> {
	List<Faq> findByOrderByOrderingAsc();
	List<Faq> findByActiveOrderByOrderingAsc(boolean active);
}
