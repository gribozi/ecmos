package com.jsmart.ecmos.repository;

import java.util.List;

import com.jsmart.ecmos.entity.ObjectType;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * @author Nikolay Koretskyy
 *
 */
@Repository
public interface ObjectTypeRepository extends JpaRepository<ObjectType, Integer> {
	ObjectType findByUri(String uri);
	List<ObjectType> findByOrderByOrderingAsc();
	List<ObjectType> findByActiveOrderByOrderingAsc(boolean active);
}
