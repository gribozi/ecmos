package com.jsmart.ecmos.repository;

import com.jsmart.ecmos.entity.Facility;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * @author Sergey Khomich
 *
 */
@Repository
public interface FacilityRepository extends JpaRepository<Facility, Integer> {
}
