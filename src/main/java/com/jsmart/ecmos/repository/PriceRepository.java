package com.jsmart.ecmos.repository;

import java.util.List;

import com.jsmart.ecmos.entity.Price;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * @author Nikolay Koretskyy
 *
 */
@Repository
public interface PriceRepository extends JpaRepository<Price, Integer> {
	List<Price> findByOrderByOrderingAsc();
	List<Price> findByActiveOrderByOrderingAsc(boolean active);
}
