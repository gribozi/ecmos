package com.jsmart.ecmos.repository;

import com.jsmart.ecmos.entity.ForTeamText;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * @author Sergey Khomich
 *
 */
@Repository
public interface ForTeamTextRepository extends JpaRepository<ForTeamText, Integer> {
}
