package com.jsmart.ecmos.repository;

import java.util.List;

import com.jsmart.ecmos.entity.Slide;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * @author Sergey Khomich
 *
 */
@Repository
public interface SlideRepository extends JpaRepository<Slide, Integer> {
	List<Slide> findByOrderByOrderingAsc();
	List<Slide> findByActiveOrderByOrderingAsc(boolean active);
}
