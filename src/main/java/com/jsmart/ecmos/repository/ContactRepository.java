package com.jsmart.ecmos.repository;

import com.jsmart.ecmos.entity.Contact;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * @author Sergey Khomich
 *
 */
@Repository
public interface ContactRepository extends JpaRepository<Contact, Integer> {
}
