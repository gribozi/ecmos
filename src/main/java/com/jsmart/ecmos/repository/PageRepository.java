package com.jsmart.ecmos.repository;

import java.util.List;

import com.jsmart.ecmos.entity.Page;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * @author Nikolay Koretskyy
 *
 */
@Repository
public interface PageRepository extends JpaRepository<Page, Integer> {
	List<Page> findByActiveOrderByOrderingAsc(boolean active);
	Page findByUri(String uri);
}
