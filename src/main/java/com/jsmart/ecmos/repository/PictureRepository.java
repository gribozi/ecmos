package com.jsmart.ecmos.repository;

import java.util.List;

import com.jsmart.ecmos.entity.Picture;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * @author Nikolay Koretskyy
 *
 */
@Repository
public interface PictureRepository extends JpaRepository<Picture, Integer> {
	List<Picture> findByTypeOrderByOrderingAsc(String type);
	List<Picture> findByTypeAndActiveOrderByOrderingAsc(String type, boolean active);
}
