package com.jsmart.ecmos.entity;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @author Nikolay Koretskyy
 *
 */
@Entity
@Data
@EqualsAndHashCode(callSuper=false)
@Table(name = "FOR_INVESTORS")
public class ForInvestors extends BaseEntity {

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "forInvestors", cascade = CascadeType.ALL)
	private List<ForInvestorsContent> forInvestorsContents;

}
