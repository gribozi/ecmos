package com.jsmart.ecmos.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @author Sergey Khomich
 *
 */
@Entity
@Data
@EqualsAndHashCode(callSuper=false)
@Table(name = "FOR_TEAMTEXT_CONTENT")
public class ForTeamTextContent extends BaseContent {

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "FOR_TEAMTEXT_ID", nullable = false)
	private ForTeamText forTeamText;

	@Column(name = "TEXT", columnDefinition = "TEXT")
	private String text;

}
