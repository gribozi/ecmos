package com.jsmart.ecmos.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @author Nikolay Koretskyy
 *
 */
@Entity
@Data
@EqualsAndHashCode(callSuper=false)
@Table(name = "LANGUAGE")
public class Language extends BaseEntity {

	@Column(name = "NAME", nullable = false)
	private String name;

	@Column(name = "INTERPRITATION", nullable = false)
	private String interpretation;

}
