package com.jsmart.ecmos.entity;

import java.util.List;

import lombok.Data;

/**
 * @author Sergey Khomich
 *
 */
@Data
public class PriceOption {
	private String optionTitle;
	private List<PriceStage> priceStages;
}
