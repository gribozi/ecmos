package com.jsmart.ecmos.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @author Sergey Khomich
 *
 */
@Entity
@Data
@EqualsAndHashCode(callSuper=false)
@Table(name = "SLIDE_CONTENT")
public class SlideContent extends BaseContent {

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "SLIDE_ID", nullable = false)
	private Slide slide;

	@Column(name = "TEXT_BIG", nullable = false)
	private String textBig;

	@Column(name = "TEXT_SMALL", nullable = false)
	private String textSmall;

	@Column(name = "LINK")
	private String link;

}
