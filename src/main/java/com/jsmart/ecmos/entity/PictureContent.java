package com.jsmart.ecmos.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @author Nikolay Koretskyy
 *
 */
@Entity
@Data
@EqualsAndHashCode(callSuper=false)
@Table(name = "PICTURE_CONTENT")
public class PictureContent extends BaseContent {

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "PICTURE_ID", nullable = false)
	private Picture picture;

	@Column(name = "PICTURE_TITLE")
	private String pictureTitle;

	@Column(name = "PICTURE_LINK", columnDefinition = "TEXT")
	private String pictureLink;

}
