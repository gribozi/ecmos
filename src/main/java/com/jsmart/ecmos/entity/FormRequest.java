package com.jsmart.ecmos.entity;

import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

/**
 * @author Sergey Khomich
 *
 */
@Entity
@Data
@Table(name = "FORMREQUEST")
public class FormRequest {

	@Id
	@GeneratedValue(strategy= GenerationType.IDENTITY)
	@Column(name = "ID")
	private int id;

	@Column(name = "REQUEST_DATE")
	private LocalDate requestDate;

	@Column(name = "FORM")
	private String form;

	@Column(name = "NAME")
	private String name;

	@Column(name = "PHONE")
	private String phone;

	@Column(name = "EMAIL")
	private String email;

	@Column(name = "TEXT")
	private String text;

	public FormRequest() {}

	public FormRequest(LocalDate requestDate, String form, String name, String phone, String email, String text) {
		this.requestDate = requestDate;
		this.form = form;
		this.name = name;
		this.phone = phone;
		this.email = email;
		this.text = text;
	}

}
