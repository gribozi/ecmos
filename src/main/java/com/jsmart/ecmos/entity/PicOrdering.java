package com.jsmart.ecmos.entity;

import lombok.Data;

/**
 * @author Sergey Khomich
 *
 */
@Data
public class PicOrdering {
	private int id;
	private int ordering;
}
