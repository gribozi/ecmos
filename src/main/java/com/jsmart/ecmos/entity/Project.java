package com.jsmart.ecmos.entity;

import java.time.LocalDate;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @author Nikolay Koretskyy
 *
 */
@Entity
@Data
@EqualsAndHashCode(callSuper=false)
@Table(name = "PROJECT")
public class Project extends BaseEntity {

	@Column(name = "TYPE_PROJECT", nullable = false)
	private boolean typeProject;

	@Column(name = "TYPE_OBJECT", nullable = false)
	private boolean typeObject;

	@Column(name = "TYPE_REALTOR", nullable = false)
	private boolean typeRealtor;

	@Column(name = "INDEX_OUTPUT", nullable = false)
	private boolean indexOutput;

	@JsonDeserialize(using = ObjectTypeDeserializer.class)
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "OBJECT_TYPE_ID")
	private ObjectType objectType;

	@Column(name = "DIMENSIONS")
	private String dimensions;

	@Column(name = "SQUARE")
	private Integer square;

	@Column(name = "COAST")
	private String coast;

	@Column(name = "COAST_ACTION")
	private String coastAction;

	@Column(name = "ACTION_BEGIN")
	private LocalDate actionBegin;

	@Column(name = "ACTION_END")
	private LocalDate actionEnd;

	@Column(name = "VIDEO")
	private String video;

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "project", cascade = CascadeType.ALL)
	private List<ProjectContent> projectContents;

}
