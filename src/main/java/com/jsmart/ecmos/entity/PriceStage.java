package com.jsmart.ecmos.entity;

import lombok.Data;

/**
 * @author Sergey Khomich
 *
 */
@Data
public class PriceStage {
	private String stageTitle;
	private boolean inBase;
	private boolean inComfort;
	private boolean inLux;
}
