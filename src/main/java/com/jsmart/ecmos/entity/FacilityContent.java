package com.jsmart.ecmos.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @author Sergey Khomich
 *
 */
@Entity
@Data
@EqualsAndHashCode(callSuper=false)
@Table(name = "FACILITY_CONTENT")
public class FacilityContent extends BaseContent {

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "FACILITY_ID", nullable = false)
	private Facility facility;

	@Column(name = "TEXT_BIG")
	private String textBig;

	@Column(name = "TEXT_SMALL")
	private String textSmall;

}
