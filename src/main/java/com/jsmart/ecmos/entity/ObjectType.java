package com.jsmart.ecmos.entity;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @author Nikolay Koretskyy
 *
 */
@Entity
@Data
@EqualsAndHashCode(callSuper=false)
@Table(name = "OBJECT_TYPE")
@JsonIdentityInfo(
		generator = ObjectIdGenerators.PropertyGenerator.class,
		property = "id")
public class ObjectType extends BaseEntity {

	@Column(name = "URI", nullable = false)
	private String uri;

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "objectType", cascade = CascadeType.ALL)
	private List<ObjectTypeContent> objectTypeContents;

	@Override
	public String toString(){
		return "ObjectType " +uri;
	}
}
