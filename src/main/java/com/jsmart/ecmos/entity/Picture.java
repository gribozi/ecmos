package com.jsmart.ecmos.entity;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @author Nikolay Koretskyy
 *
 */
@Entity
@Data
@EqualsAndHashCode(callSuper=false)
@Table(name = "PICTURE")
public class Picture extends BaseEntity {

	@Column(name = "TYPE", nullable = false)
	private String type;

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "picture", cascade = CascadeType.ALL)
	private List<PictureContent> pictureContents;

}
