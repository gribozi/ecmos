package com.jsmart.ecmos.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @author Nikolay Koretskyy
 *
 */
@Entity
@Data
@EqualsAndHashCode(callSuper=false)
@Table(name = "PAGE_CONTENT")
public class PageContent extends BaseContent {

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "PAGE_ID", nullable = false)
	private Page page;

	@Column(name = "NAME")
	private String name;

	@Column(name = "CONTENT", columnDefinition = "TEXT")
	private String content;

	@Column(name = "H1")
	private String h1;

	@Column(name = "TITLE")
	private String title;

	@Column(name = "DESCRIPTION", columnDefinition = "TEXT")
	private String description;

	@Column(name = "KEYWORDS", columnDefinition = "TEXT")
	private String keywords;

}
