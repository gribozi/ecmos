package com.jsmart.ecmos.entity;

import java.io.IOException;

import com.jsmart.ecmos.repository.ObjectTypeRepository;

import org.springframework.beans.factory.annotation.Autowired;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonNode;

/**
 * @author Sergey Khomich
 *
 */
public class ObjectTypeDeserializer extends JsonDeserializer<ObjectType> {

	@Autowired
	ObjectTypeRepository objectTypeRepository;

	@Override
	public ObjectType deserialize(JsonParser jp, DeserializationContext ctxt) throws IOException, JsonProcessingException {
		JsonNode node = jp.getCodec().readTree(jp);
		return (objectTypeRepository.findById(node.intValue())).get();
	}
}
