package com.jsmart.ecmos.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @author Nikolay Koretskyy
 *
 */
@Entity
@Data
@EqualsAndHashCode(callSuper=false)
@Table(name = "PROJECT_CONTENT")
public class ProjectContent extends BaseContent {

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "PROJECT_ID", nullable = false)
	private Project project;

	@Column(name = "NAME")
	private String name;

	@Column(name = "GENERAL_DESCRIPTION", columnDefinition = "TEXT")
	private String generalDescription;

	@Column(name = "FLOR1_DESCRIPTION", columnDefinition = "TEXT")
	private String floor1description;

	@Column(name = "FLOR2_DESCRIPTION", columnDefinition = "TEXT")
	private String floor2description;

	@Column(name = "FLOR3_DESCRIPTION", columnDefinition = "TEXT")
	private String floor3description;

	@Column(name = "FLOR4_DESCRIPTION", columnDefinition = "TEXT")
	private String floor4description;

	@Column(name = "ADDRESS")
	private String address;

	@Column(name = "H1")
	private String h1;

	@Column(name = "TITLE")
	private String title;

	@Column(name = "DESCRIPTION", columnDefinition = "TEXT")
	private String description;

	@Column(name = "KEYWORDS", columnDefinition = "TEXT")
	private String keywords;

}
