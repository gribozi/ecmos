package com.jsmart.ecmos.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @author Sergey Khomich
 *
 */
@Entity
@Data
@EqualsAndHashCode(callSuper=false)
@Table(name = "CONTACT_CONTENT")
public class ContactContent extends BaseContent {

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "CONTACT_ID", nullable = false)
	private Contact contact;

	@Column(name = "INFO_TITLE")
	private String infoTitle;

	@Column(name = "INFO_ADDRESS")
	private String infoAddress;

	@Column(name = "INFO_WORKING")
	private String infoWorking;

	@Column(name = "INFO_PHONES", columnDefinition = "TEXT")
	private String infoPhones;

}
