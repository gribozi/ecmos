package com.jsmart.ecmos.entity;

import java.util.List;

import lombok.Data;

/**
 * @author Nikolay Koretskyy
 *
 */
@Data
public class PriceExcelContent {
	private String priceTile;
	private double basePrice;
	private double comfortPrice;
	private double luxPrice;
	private List<PriceOption> priceOptions;
}
