package com.jsmart.ecmos.entity;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @author Sergey Khomich
 *
 */
@Entity
@Data
@EqualsAndHashCode(callSuper=false)
@Table(name = "CONTACT")
public class Contact extends BaseEntity {

	@Column(name = "PHONE", nullable = false)
	private String phone;

	@Column(name = "EMAIL_TO", nullable = false)
	private String emailTo;

	@Column(name = "SMFB")
	private String smFb;

	@Column(name = "SMVK")
	private String smVk;

	@Column(name = "SMYT")
	private String smYt;

	@Column(name = "SMIN")
	private String smIn;

	@Column(name = "ADRESSLNG")
	private String adressLng;

	@Column(name = "ADRESSLTG")
	private String adressLtg;

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "contact", cascade = CascadeType.ALL)
	private List<ContactContent> contactContents;

}
