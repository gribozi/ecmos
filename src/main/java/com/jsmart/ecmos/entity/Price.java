package com.jsmart.ecmos.entity;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;
import javax.persistence.Transient;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @author Nikolay Koretskyy
 *
 */
@Entity
@Data
@EqualsAndHashCode(callSuper=false)
public class Price extends BaseEntity {

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "price", cascade = CascadeType.ALL)
	private List<PriceContent> priceContents;

	@Transient
	private PriceExcelContent priceExcelContent;

}
