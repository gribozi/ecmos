package com.jsmart.ecmos.entity;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @author Sergey Khomich
 *
 */
@Entity
@Data
@EqualsAndHashCode(callSuper=false)
@Table(name = "SLIDE")
public class Slide extends BaseEntity {

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "slide", cascade = CascadeType.ALL)
	private List<SlideContent> slideContents;

}
