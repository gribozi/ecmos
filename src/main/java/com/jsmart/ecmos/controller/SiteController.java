package com.jsmart.ecmos.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import com.jsmart.ecmos.entity.ObjectType;
import com.jsmart.ecmos.entity.Page;
import com.jsmart.ecmos.entity.PageContent;
import com.jsmart.ecmos.entity.Project;
import com.jsmart.ecmos.entity.ProjectContent;
import com.jsmart.ecmos.service.ContactService;
import com.jsmart.ecmos.service.FacilityService;
import com.jsmart.ecmos.service.FaqService;
import com.jsmart.ecmos.service.ForInvestorsService;
import com.jsmart.ecmos.service.ObjectTypeService;
import com.jsmart.ecmos.service.PageService;
import com.jsmart.ecmos.service.PictureService;
import com.jsmart.ecmos.service.PriceService;
import com.jsmart.ecmos.service.ProjectService;
import com.jsmart.ecmos.service.SlideService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.view.RedirectView;

/**
 * @author Nikolay Koretskyy
 *
 */
@Controller
public class SiteController {

	@Autowired
	private PageService pageService;

	@Autowired
	private ProjectService projectService;

	@Autowired
	private ObjectTypeService objectTypeService;

	@Autowired
	private PictureService pictureService;

	@Autowired
	private PriceService priceService;

	@Autowired
	private SlideService slideService;

	@Autowired
	private FacilityService facilityService;

	@Autowired
	private FaqService faqService;

	@Autowired
	private ForInvestorsService forInvestorsService;

	@Autowired
	private ContactService contactService;

	@Value("${site.language}")
	private String language;

	@Value("${site.projects.pagesize}")
	private int paginationPageSize;

	@RequestMapping(value = {"/", "/index"}, method = RequestMethod.GET)
	public String indexPage(Model model) {
		model.addAttribute("siteMenu", pageService.getSiteMenu(language));
		model.addAttribute("page", pageService.getOne("index", language));
		model.addAttribute("contentTpl", "index");
		model.addAttribute("slides", slideService.getAll(language));
		model.addAttribute("faqs", faqService.getAll(language));
		model.addAttribute("whyWes", pictureService.getByType("whywe", language));
		model.addAttribute("whatWeBuilds", pictureService.getByType("whatwebuild", language));
		model.addAttribute("facility", facilityService.getOne(language));
		model.addAttribute("projects", projectService.getProjectsForIndexPage(language));
		return "site/page";
	}

	@RequestMapping(value = "/projects", method = RequestMethod.GET)
	public String projectsPage(Model model) {
		model.addAttribute("siteMenu", pageService.getSiteMenu(language));
		model.addAttribute("page", pageService.getOne("projects", language));
		model.addAttribute("contentTpl", "projects");
		List<Project> projects = projectService.getByType("projects", language);
		if (projects.size() > paginationPageSize) {
			model.addAttribute("projects", projects.subList(0, paginationPageSize));
			model.addAttribute("isNeedPagination", true);
			model.addAttribute("lastPaginationPosition", paginationPageSize);
		} else {
			model.addAttribute("projects", projects);
			model.addAttribute("isNeedPagination", false);
		}
		return "site/page";
	}

	@RequestMapping (value = "/projects", method = RequestMethod.POST)
	public String filteredProjects(
									@RequestParam("floor") Integer floor,
									@RequestParam("minSquare") Integer minSquare,
									@RequestParam("maxSquare") Integer maxSquare,
									@RequestParam("lastPaginationPosition") Integer lastPaginationPosition,
									Model model) {
		List<Project> filteredProjects = projectService.getProjectsFiltered(floor, minSquare, maxSquare, language);
		if (filteredProjects.size() > lastPaginationPosition) {
			if (filteredProjects.size() > (paginationPageSize + lastPaginationPosition)) {
				model.addAttribute("projects", filteredProjects.subList(lastPaginationPosition, lastPaginationPosition + paginationPageSize));
				model.addAttribute("isNeedPagination", true);
				model.addAttribute("lastPaginationPosition", paginationPageSize + lastPaginationPosition);
			} else {
				model.addAttribute("projects", filteredProjects.subList(lastPaginationPosition, filteredProjects.size()));
				model.addAttribute("isNeedPagination", false);
			}
		} else {
			model.addAttribute("projects", filteredProjects);
			model.addAttribute("isNeedPagination",false);
		}
		return "site/content_templates/projects-paginated";
	}

	@RequestMapping(value = "/projects/{projectId}", method = RequestMethod.GET)
	public String projectPage(@PathVariable("projectId") int projectId, Model model) {

		Project project = projectService.getOne(projectId, language);
		ProjectContent projectContent = project.getProjectContents().get(0);

		Page page = pageService.getOne("projects", language);
		PageContent pageContent = page.getPageContents().get(0);

		String projectTitle = projectContent.getTitle();
		if ((Optional.ofNullable(projectTitle).isPresent()) && !projectTitle.isEmpty()) {
			pageContent.setTitle(projectTitle);
		}

		String projectH1 = projectContent.getH1();
		if ((Optional.ofNullable(projectH1).isPresent()) && !projectH1.isEmpty()) {
			pageContent.setH1(projectH1);
		}

		String projectKeywords = projectContent.getKeywords();
		if ((Optional.ofNullable(projectKeywords).isPresent()) && !projectKeywords.isEmpty()) {
			pageContent.setKeywords(projectKeywords);
		}

		String projectDescription = projectContent.getDescription();
		if ((Optional.ofNullable(projectDescription).isPresent()) && !projectDescription.isEmpty()) {
			pageContent.setDescription(projectDescription);
		}

		List<PageContent> pageContents = new ArrayList<PageContent>();
		pageContents.add(pageContent);
		page.setPageContents(pageContents);

		model.addAttribute("siteMenu", pageService.getSiteMenu(language));
		model.addAttribute("page", page);
		model.addAttribute("contentTpl", "project");
		model.addAttribute("project", project);
		model.addAttribute("isActionPeriod", projectService.isActionPeriod(project));
		model.addAttribute("picCount", projectService.getProjectPicturesCount(projectId));
		return "site/page";
	}

	@RequestMapping(value = "/prices", method = RequestMethod.GET)
	public String pricePage(Model model) {
		model.addAttribute("siteMenu", pageService.getSiteMenu(language));
		model.addAttribute("page", pageService.getOne("prices", language));
		model.addAttribute("contentTpl", "prices");
		model.addAttribute("prices", priceService.getAll(language));
		return "site/page";
	}

	@RequestMapping(value = "/team", method = RequestMethod.GET)
	public String teamPage(Model model) {
		model.addAttribute("siteMenu", pageService.getSiteMenu(language));
		model.addAttribute("page", pageService.getOne("team", language));
		model.addAttribute("contentTpl", "team");
		model.addAttribute("facility", facilityService.getOne(language));
		model.addAttribute("whyWes", pictureService.getByType("whywe", language));
		model.addAttribute("certificates", pictureService.getByType("certificate", language));
		model.addAttribute("employees", pictureService.getByType("employee", language));
		model.addAttribute("forTeamText", pictureService.getOneTeamText(language));
		return "site/page";
	}

	@RequestMapping(value = "/objects", method = RequestMethod.GET)
	public RedirectView redirectToObjectsPageTypedAll() {
		return new RedirectView("/objects/all");
	}

	@RequestMapping(value = "/objects/{objectTypeUri}", method = RequestMethod.GET)
	public String objectsPageTyped(
									@PathVariable("objectTypeUri") String objectTypeUri,
									Model model) {
		model.addAttribute("siteMenu", pageService.getSiteMenu(language));
		model.addAttribute("page", pageService.getOne("objects", language));
		model.addAttribute("contentTpl", "objects");
		List<Project> projects = null;
		if ("all".equals(objectTypeUri)) {
			projects = projectService.getByType("objects", language);
		} else {
			ObjectType objectType = objectTypeService.getOne(objectTypeUri, language);
			projects = projectService.getObjectsByObjectType(objectType, language);
		}
		if (projects.size() > paginationPageSize) {
			model.addAttribute("projects", projects.subList(0, paginationPageSize));
			model.addAttribute("isNeedPagination", true);
			model.addAttribute("lastPaginationPosition", paginationPageSize);
		} else {
			model.addAttribute("projects", projects);
			model.addAttribute("isNeedPagination", false);
		}
		model.addAttribute("objectsMenu", objectTypeService.getObjectsMenu(language));
		return "site/page";
	}

	@RequestMapping (value = "/objects/{objectTypeUri}", method = RequestMethod.POST)
	public String objectsPaginated(
									@PathVariable("objectTypeUri") String objectTypeUri,
									@RequestParam("lastPaginationPosition") Integer lastPaginationPosition,
									Model model) {
		List<Project> projects = null;
		if ("all".equals(objectTypeUri)) {
			projects = projectService.getByType("objects", language);
		} else {
			ObjectType objectType = objectTypeService.getOne(objectTypeUri, language);
			projects = projectService.getObjectsByObjectType(objectType, language);
		}
		if (projects.size() > lastPaginationPosition) {
			if (projects.size() > (paginationPageSize + lastPaginationPosition)) {
				model.addAttribute("projects", projects.subList(lastPaginationPosition, lastPaginationPosition + paginationPageSize));
				model.addAttribute("isNeedPagination", true);
				model.addAttribute("lastPaginationPosition", paginationPageSize + lastPaginationPosition);
			} else {
				model.addAttribute("projects", projects.subList(lastPaginationPosition, projects.size()));
				model.addAttribute("isNeedPagination", false);
			}
		} else {
			model.addAttribute("projects", projects);
			model.addAttribute("isNeedPagination",false);
		}
		return "site/content_templates/objects-paginated";
	}

	@RequestMapping(value = "/objects/{objectTypeUri}/{objectId}", method = RequestMethod.GET)
	public String objectPage(
			@PathVariable("objectTypeUri") String objectTypeUri,
			@PathVariable("objectId") int objectId,
			Model model) {

		Project project = projectService.getOne(objectId, language);
		ProjectContent projectContent = project.getProjectContents().get(0);

		Page page = pageService.getOne("objects", language);
		PageContent pageContent = page.getPageContents().get(0);

		String projectTitle = projectContent.getTitle();
		if ((Optional.ofNullable(projectTitle).isPresent()) && !projectTitle.isEmpty()) {
			pageContent.setTitle(projectTitle);
		}

		String projectH1 = projectContent.getH1();
		if ((Optional.ofNullable(projectH1).isPresent()) && !projectH1.isEmpty()) {
			pageContent.setH1(projectH1);
		}

		String projectKeywords = projectContent.getKeywords();
		if ((Optional.ofNullable(projectKeywords).isPresent()) && !projectKeywords.isEmpty()) {
			pageContent.setKeywords(projectKeywords);
		}

		String projectDescription = projectContent.getDescription();
		if ((Optional.ofNullable(projectDescription).isPresent()) && !projectDescription.isEmpty()) {
			pageContent.setDescription(projectDescription);
		}

		List<PageContent> pageContents = new ArrayList<PageContent>();
		pageContents.add(pageContent);
		page.setPageContents(pageContents);

		model.addAttribute("siteMenu", pageService.getSiteMenu(language));
		model.addAttribute("page", page);
		model.addAttribute("contentTpl", "object");
		model.addAttribute("project", project);
		if (!"all".equals(objectTypeUri)) {
			model.addAttribute("objectType", objectTypeService.getOne(objectTypeUri, language));
		}
		model.addAttribute("picCount", projectService.getProjectPicturesCount(objectId));
		return "site/page";
	}

	@RequestMapping(value = "/realtors", method = RequestMethod.GET)
	public String rieltorsPage(Model model) {
		model.addAttribute("siteMenu", pageService.getSiteMenu(language));
		model.addAttribute("page", pageService.getOne("realtors", language));
		model.addAttribute("contentTpl", "realtors");
		List<Project> projects = projectService.getByType("realtors", language);
		if (projects.size() > paginationPageSize) {
			model.addAttribute("projects", projects.subList(0, paginationPageSize));
			model.addAttribute("isNeedPagination", true);
			model.addAttribute("lastPaginationPosition", paginationPageSize);
			model.addAttribute("isActionPeriods", projectService.isActionPeriods(projects.subList(0, paginationPageSize)));
		} else {
			model.addAttribute("projects", projects);
			model.addAttribute("isNeedPagination", false);
			model.addAttribute("isActionPeriods", projectService.isActionPeriods(projects));
		}
		return "site/page";
	}

	@RequestMapping (value = "/realtors", method = RequestMethod.POST)
	public String realtorsPaginated(
									@RequestParam("lastPaginationPosition") Integer lastPaginationPosition,
									Model model) {
		List<Project> projects = projectService.getByType("realtors", language);
		if (projects.size() > lastPaginationPosition) {
			if (projects.size() > (paginationPageSize + lastPaginationPosition)) {
				model.addAttribute("projects", projects.subList(lastPaginationPosition, lastPaginationPosition + paginationPageSize));
				model.addAttribute("isNeedPagination", true);
				model.addAttribute("lastPaginationPosition", paginationPageSize + lastPaginationPosition);
				model.addAttribute("isActionPeriods", projectService.isActionPeriods(projects.subList(lastPaginationPosition, lastPaginationPosition + paginationPageSize)));
			} else {
				model.addAttribute("projects", projects.subList(lastPaginationPosition, projects.size()));
				model.addAttribute("isActionPeriods", projectService.isActionPeriods(projects.subList(lastPaginationPosition, projects.size())));
				model.addAttribute("isNeedPagination", false);
			}
		} else {
			model.addAttribute("projects", projects);
			model.addAttribute("isNeedPagination",false);
		}
		return "site/content_templates/realtors-paginated";
	}

	@RequestMapping(value = "/realtors/{realtorId}", method = RequestMethod.GET)
	public String realtorPage(@PathVariable("realtorId") int realtorId, Model model) {

		Project project = projectService.getOne(realtorId, language);
		ProjectContent projectContent = project.getProjectContents().get(0);

		Page page = pageService.getOne("realtors", language);
		PageContent pageContent = page.getPageContents().get(0);

		String projectTitle = projectContent.getTitle();
		if ((Optional.ofNullable(projectTitle).isPresent()) && !projectTitle.isEmpty()) {
			pageContent.setTitle(projectTitle);
		}

		String projectH1 = projectContent.getH1();
		if ((Optional.ofNullable(projectH1).isPresent()) && !projectH1.isEmpty()) {
			pageContent.setH1(projectH1);
		}

		String projectKeywords = projectContent.getKeywords();
		if ((Optional.ofNullable(projectKeywords).isPresent()) && !projectKeywords.isEmpty()) {
			pageContent.setKeywords(projectKeywords);
		}

		String projectDescription = projectContent.getDescription();
		if ((Optional.ofNullable(projectDescription).isPresent()) && !projectDescription.isEmpty()) {
			pageContent.setDescription(projectDescription);
		}

		List<PageContent> pageContents = new ArrayList<PageContent>();
		pageContents.add(pageContent);
		page.setPageContents(pageContents);

		model.addAttribute("siteMenu", pageService.getSiteMenu(language));
		model.addAttribute("page", page);
		model.addAttribute("contentTpl", "realtor");
		model.addAttribute("project", project);
		model.addAttribute("isActionPeriod", projectService.isActionPeriod(project));
		model.addAttribute("picCount", projectService.getProjectPicturesCount(realtorId));
		return "site/page";
	}

	@RequestMapping(value = "/forinvestors", method = RequestMethod.GET)
	public String investorsPage(Model model) {
		model.addAttribute("siteMenu", pageService.getSiteMenu(language));
		model.addAttribute("page", pageService.getOne("forinvestors", language));
		model.addAttribute("contentTpl", "forinvestors");
		model.addAttribute("forInvestors", forInvestorsService.getOne(language));
		model.addAttribute("whyWes", pictureService.getByType("whywe", language));
		return "site/page";
	}

	@RequestMapping(value = "/partners", method = RequestMethod.GET)
	public String partnersPage(Model model) {
		model.addAttribute("siteMenu", pageService.getSiteMenu(language));
		model.addAttribute("page", pageService.getOne("partners", language));
		model.addAttribute("contentTpl", "partners");
		model.addAttribute("partners", pictureService.getByType("partner", language));
		return "site/page";
	}

	@RequestMapping(value = "/contacts", method = RequestMethod.GET)
	public String contactsPage(Model model) {
		model.addAttribute("siteMenu", pageService.getSiteMenu(language));
		model.addAttribute("page", pageService.getOne("contacts", language));
		model.addAttribute("contentTpl", "contacts");
		model.addAttribute("contact", contactService.getOne(language));
		return "site/page";
	}

}
