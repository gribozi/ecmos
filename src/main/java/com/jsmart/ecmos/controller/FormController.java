package com.jsmart.ecmos.controller;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.jsmart.ecmos.service.technical.FormRequestService;

/**
 * @author Sergey Khomich
 *
 */
@Controller
public class FormController {

	@Autowired
	private FormRequestService formRequestService;

	@PostMapping(value = "/form")
	public ResponseEntity<String> processingMailRequest(
														@RequestParam("form") String form,
														@RequestParam("name") String name,
														@RequestParam("phone") String phone,
														@RequestParam("email") String email,
														@RequestParam("text") String text) {
		if (Optional.ofNullable(form).isPresent() & Optional.ofNullable(email).isPresent() & Optional.ofNullable(text).isPresent()) {
			formRequestService.processingRequest(form, name, phone, email, text);
		}
		return new ResponseEntity<String>(email, HttpStatus.OK);
	}

}
