package com.jsmart.ecmos.controller.admin;

import java.util.List;
import java.util.Optional;

import com.jsmart.ecmos.entity.ObjectType;
import com.jsmart.ecmos.service.ObjectTypeService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;

/**
 * @author Nikolay Koretskyy
 *
 */
@Controller
public class ObjectTypesAdminController {

	@Autowired
	private ObjectTypeService objectTypeService;

	@Value("${site.language}")
	private String language;

	@GetMapping("/admin/objecttypes")
	public String objectTypesList(Model model) {
		model.addAttribute("contentTpl", "objecttypes");
		model.addAttribute("objectTypes", objectTypeService.getForAdmin(language));
		return "admin/page";
	}

	@PutMapping(value = "/admin/objecttypes/ordering", consumes = MediaType.APPLICATION_JSON_UTF8_VALUE, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public ResponseEntity<List<ObjectType>> updateOrderings(@RequestBody List<ObjectType> objectTypes) {
		if (Optional.ofNullable(objectTypes).isPresent()) {
			List<ObjectType> updatedObjectTypesOrdering = objectTypeService.updateOrderings(objectTypes, language);
			if (Optional.ofNullable(objectTypes).isPresent()) {
				return new ResponseEntity<List<ObjectType>>(updatedObjectTypesOrdering, HttpStatus.OK);
			} else {
				return new ResponseEntity<List<ObjectType>>(HttpStatus.NOT_FOUND);
			}
		} else {
			return new ResponseEntity<List<ObjectType>>(HttpStatus.BAD_REQUEST);
		}
	}

}
