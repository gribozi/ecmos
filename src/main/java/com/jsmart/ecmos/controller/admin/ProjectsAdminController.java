package com.jsmart.ecmos.controller.admin;

import java.io.IOException;
import java.util.List;
import java.util.Optional;

import com.jsmart.ecmos.entity.PicOrdering;
import com.jsmart.ecmos.entity.Project;
import com.jsmart.ecmos.service.ProjectService;
import com.jsmart.ecmos.service.technical.FileProcessingService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.multipart.MultipartFile;

import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * @author Sergey Khomich
 *
 */
@Controller
public class ProjectsAdminController {

	@Autowired
	private ProjectService projectService;

	@Autowired
	private FileProcessingService fileProcessingService;

	@Autowired
	private ObjectMapper mapper;

	@Value("${site.language}")
	private String language;

	private Project savedProject;

	@GetMapping("/admin/projects/{objectTypeUri}")
	public String projects(Model model, @PathVariable("objectTypeUri") String objectTypeUri) {
		model.addAttribute("contentTpl", "projects");
		model.addAttribute("objectTypeUri", objectTypeUri);
		model.addAttribute("projects", projectService.getByTypeForAdmin(objectTypeUri, language));
		return "admin/page";
	}

	@PutMapping(value = "/admin/projects/ordering", consumes = MediaType.APPLICATION_JSON_UTF8_VALUE, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public ResponseEntity<List<Project>> updateOrderings(@RequestBody List<Project> projects) {
		if (Optional.ofNullable(projects).isPresent()) {
			List<Project> updatedProjectsOrdering = projectService.updateOrderings(projects, language);
			if (Optional.ofNullable(projects).isPresent()) {
				return new ResponseEntity<List<Project>>(updatedProjectsOrdering, HttpStatus.OK);
			} else {
				return new ResponseEntity<List<Project>>(HttpStatus.NOT_FOUND);
			}
		} else {
			return new ResponseEntity<List<Project>>(HttpStatus.BAD_REQUEST);
		}
	}

	@GetMapping("/admin/project")
	public String project(Model model, @RequestParam(value = "objectTypeUri") String objectTypeUri) {
		model.addAttribute("contentTpl", "project");
		model.addAttribute("objectTypeUri", objectTypeUri);
		model.addAttribute("objectTypes", projectService.getObjectsMenu(language));
		return "admin/page";
	}

	@GetMapping("/admin/project/{id}")
	public String projectPage(@PathVariable("id") int id, Model model) {
		model.addAttribute("contentTpl", "project");
		model.addAttribute("project", projectService.getOne(id, language));
		model.addAttribute("objectTypes", projectService.getObjectsMenu(language));
		model.addAttribute("picCount", projectService.getProjectPicturesCount(id));
		return "admin/page";
	}

	@PostMapping(value = "/admin/project/", consumes = "multipart/form-data", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public ResponseEntity<Project> create(	@RequestPart(value="jsonProject", required = true) String jsonProject,
											@RequestPart(value="floor1Pic", required = false) MultipartFile floor1Pic,
											@RequestPart(value="floor2Pic", required = false) MultipartFile floor2Pic,
											@RequestPart(value="floor3Pic", required = false) MultipartFile floor3Pic,
											@RequestPart(value="floor4Pic", required = false) MultipartFile floor4Pic,
											@RequestPart(value="realtorPlanPic", required = false) MultipartFile realtorPlanPic,
											@RequestPart(value="projectPics", required = false) MultipartFile projectPic) {
		try {
			Project project = mapper.readValue(jsonProject, Project.class);
			savedProject = projectService.create(project, language);
			if (Optional.ofNullable(floor1Pic).isPresent()) {
				fileProcessingService.saveProjectImages(savedProject.getId(), "floor1", floor1Pic);
			}
			if (Optional.ofNullable(floor2Pic).isPresent()) {
				fileProcessingService.saveProjectImages(savedProject.getId(), "floor2", floor2Pic);
			}
			if (Optional.ofNullable(floor3Pic).isPresent()) {
				fileProcessingService.saveProjectImages(savedProject.getId(), "floor3", floor3Pic);
			}
			if (Optional.ofNullable(floor4Pic).isPresent()) {
				fileProcessingService.saveProjectImages(savedProject.getId(), "floor4", floor4Pic);
			}
			if (Optional.ofNullable(realtorPlanPic).isPresent()) {
				fileProcessingService.saveProjectImages(savedProject.getId(), "plan", realtorPlanPic);
			}
			if (Optional.ofNullable(projectPic).isPresent()) {
				fileProcessingService.saveProjectImages(savedProject.getId(), "1", projectPic);
			}
		} catch (IOException e) {
			e.printStackTrace();
			return new ResponseEntity<Project>(HttpStatus.INTERNAL_SERVER_ERROR);
		}

		if (Optional.ofNullable(savedProject).isPresent())  {
			return new ResponseEntity<Project>(savedProject, HttpStatus.CREATED);
		} else {
			return new ResponseEntity<Project>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@PutMapping(value = "/admin/project/{id}", consumes = "multipart/form-data", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public ResponseEntity<Project> update(	@PathVariable("id") int id,
											@RequestPart(value="jsonProject", required = true) String jsonProject,
											@RequestPart(value="floor1Pic", required = false) MultipartFile floor1Pic,
											@RequestPart(value="floor2Pic", required = false) MultipartFile floor2Pic,
											@RequestPart(value="floor3Pic", required = false) MultipartFile floor3Pic,
											@RequestPart(value="floor4Pic", required = false) MultipartFile floor4Pic,
											@RequestPart(value="realtorPlanPic", required = false) MultipartFile realtorPlanPic,
											@RequestPart(value="projectPics", required = false) MultipartFile projectPic) {
		try {
			Project project = mapper.readValue(jsonProject, Project.class);
			if ((project.getId() != id) || (id <= 0)) {
				return new ResponseEntity<Project>(HttpStatus.BAD_REQUEST);
			}
			savedProject = projectService.update(project, language);
			if (Optional.ofNullable(savedProject).isPresent()) {
				if (Optional.ofNullable(floor1Pic).isPresent()) {
					fileProcessingService.saveProjectImages(savedProject.getId(), "floor1", floor1Pic);
				}
				if (Optional.ofNullable(floor2Pic).isPresent()) {
					fileProcessingService.saveProjectImages(savedProject.getId(), "floor2", floor2Pic);
				}
				if (Optional.ofNullable(floor3Pic).isPresent()) {
					fileProcessingService.saveProjectImages(savedProject.getId(), "floor3", floor3Pic);
				}
				if (Optional.ofNullable(floor4Pic).isPresent()) {
					fileProcessingService.saveProjectImages(savedProject.getId(), "floor4", floor4Pic);
				}
				if (Optional.ofNullable(realtorPlanPic).isPresent()) {
					fileProcessingService.saveProjectImages(savedProject.getId(), "plan", realtorPlanPic);
				}
				if (Optional.ofNullable(projectPic).isPresent()) {
					int picsCount = projectService.getProjectPicturesCount(savedProject.getId()) + 1;
					fileProcessingService.saveProjectImages(savedProject.getId(), String.valueOf(picsCount), projectPic);
				}
				return new ResponseEntity<Project>(savedProject, HttpStatus.OK);
			} else {
				return new ResponseEntity<Project>(HttpStatus.NOT_FOUND);
			}
		} catch (IOException e) {
			e.printStackTrace();
			return new ResponseEntity<Project>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@DeleteMapping (value = "/admin/project/{id}")
	public ResponseEntity<Void> remove(@PathVariable("id") int id) {
		if (!projectService.remove(id)) {
			return new ResponseEntity<Void>(HttpStatus.NOT_FOUND);
		} else {
			return new ResponseEntity<Void>(HttpStatus.NO_CONTENT);
		}
	}

	@DeleteMapping (value = "/admin/project/{id}/pic/{picId}")
	public ResponseEntity<Void> removePic(
										@PathVariable("id") int id,
										@PathVariable("picId") int picId) {
		if (fileProcessingService.removeProjectPic(id, picId)) {
			return new ResponseEntity<Void>(HttpStatus.NO_CONTENT);
		} else {
			return new ResponseEntity<Void>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@PutMapping(value = "/admin/project/{id}/pic/ordering", consumes = MediaType.APPLICATION_JSON_UTF8_VALUE, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public ResponseEntity<List<PicOrdering>> updatePicsOrderings(
															@PathVariable("id") int id,
															@RequestBody List<PicOrdering> picOrders) {
		if (Optional.ofNullable(picOrders).isPresent()) {
			if (fileProcessingService.updateProjectPicsOrderings(id, picOrders)) {
				return new ResponseEntity<List<PicOrdering>>(picOrders, HttpStatus.OK);
			} else {
				return new ResponseEntity<List<PicOrdering>>(HttpStatus.INTERNAL_SERVER_ERROR);
			}
		} else {
			return new ResponseEntity<List<PicOrdering>>(HttpStatus.BAD_REQUEST);
		}

	}

	@DeleteMapping (value = "/admin/project/{id}/plan/{type}")
	public ResponseEntity<Void> removePlan(
											@PathVariable("id") int id,
											@PathVariable("type") String type) {
		if (fileProcessingService.removeProjectPlan(id, type)) {
			return new ResponseEntity<Void>(HttpStatus.NO_CONTENT);
		} else {
			return new ResponseEntity<Void>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

}
