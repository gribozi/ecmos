package com.jsmart.ecmos.controller.admin;

import java.io.IOException;
import java.util.List;
import java.util.Optional;

import com.jsmart.ecmos.entity.Price;
import com.jsmart.ecmos.service.PriceService;
import com.jsmart.ecmos.service.technical.FileProcessingService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.multipart.MultipartFile;

import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * @author Nikolay Koretskyy
 *
 */
@Controller
public class PricesAdminController {

	@Autowired
	private PriceService priceService;

	@Autowired
	private FileProcessingService fileProcessingService;

	@Autowired
	private ObjectMapper mapper;

	@Value("${site.language}")
	private String language;

	private Price savedPrice;

	@GetMapping("/admin/prices")
	public String pricesList(Model model) {
		model.addAttribute("contentTpl", "prices");
		model.addAttribute("prices", priceService.getForAdmin(language));
		return "admin/page";
	}

	@PutMapping(value = "/admin/prices/ordering", consumes = MediaType.APPLICATION_JSON_UTF8_VALUE, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public ResponseEntity<List<Price>> updateOrderings(@RequestBody List<Price> prices) {
		if (Optional.ofNullable(prices).isPresent()) {
			List<Price> updatedPricesOrdering = priceService.updateOrderings(prices, language);
			if (Optional.ofNullable(prices).isPresent()) {
				return new ResponseEntity<List<Price>>(updatedPricesOrdering, HttpStatus.OK);
			} else {
				return new ResponseEntity<List<Price>>(HttpStatus.NOT_FOUND);
			}
		} else {
			return new ResponseEntity<List<Price>>(HttpStatus.BAD_REQUEST);
		}
	}

	@PostMapping(value = "/admin/prices", consumes = "multipart/form-data", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public ResponseEntity<Price> create(
										@RequestPart(value="jsonPrice", required = true) String jsonPrice,
										@RequestPart(value="priceFile", required = true) MultipartFile priceFile) {
		try {
			Price price = mapper.readValue(jsonPrice, Price.class);
			savedPrice = priceService.create(price, language);
			if (Optional.ofNullable(priceFile).isPresent()) {
				fileProcessingService.savePriceFile(savedPrice.getId(), priceFile, language);
			}
		} catch (IOException e) {
			e.printStackTrace();
			return new ResponseEntity<Price>(HttpStatus.INTERNAL_SERVER_ERROR);
		}

		if (Optional.ofNullable(savedPrice).isPresent()) {
			return new ResponseEntity<Price>(savedPrice, HttpStatus.CREATED);
		} else {
			return new ResponseEntity<Price>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@PutMapping(value = "/admin/prices/{id}", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public ResponseEntity<Price> update(
										@PathVariable("id") int id,
										@RequestPart(value="jsonPrice", required = true) String jsonPrice) {
		try {
			Price price = mapper.readValue(jsonPrice, Price.class);
			if ((price.getId() != id) || (id <= 0)) {
				return new ResponseEntity<Price>(HttpStatus.BAD_REQUEST);
			}
			savedPrice = priceService.update(price, language);
			if (Optional.ofNullable(savedPrice).isPresent()) {
				return new ResponseEntity<Price>(savedPrice, HttpStatus.OK);
			} else {
				return new ResponseEntity<Price>(HttpStatus.NOT_FOUND);
			}
		} catch (IOException e) {
			e.printStackTrace();
			return new ResponseEntity<Price>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@DeleteMapping (value = "/admin/prices/{id}")
	public ResponseEntity<Void> remove(
									@PathVariable("id") int id) {
		if (!priceService.remove(id, language)) {
			return new ResponseEntity<Void>(HttpStatus.NOT_FOUND);
		} else {
			return new ResponseEntity<Void>(HttpStatus.NO_CONTENT);
		}
	}

}
