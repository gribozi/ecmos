package com.jsmart.ecmos.controller.admin;

import com.jsmart.ecmos.service.technical.FormRequestService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

/**
 * @author Nikolay Koretskyy
 *
 */
@Controller
public class FormRequestsAdminController {

	@Autowired
	private FormRequestService formRequestService;

	@GetMapping("/admin/formrequests")
	public String formRequestsList(Model model) {
		model.addAttribute("contentTpl", "formrequests");
		model.addAttribute("formRequests", formRequestService.getForAdmin());
		return "admin/page";
	}

	@DeleteMapping(value = "/admin/formrequests/{id}")
	public ResponseEntity<Void> remove(@PathVariable("id") int id) {
		if (!formRequestService.remove(id)) {
			return new ResponseEntity<Void>(HttpStatus.NOT_FOUND);
		} else {
			return new ResponseEntity<Void>(HttpStatus.NO_CONTENT);
		}
	}

}
