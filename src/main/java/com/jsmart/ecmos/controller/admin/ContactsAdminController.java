package com.jsmart.ecmos.controller.admin;

import java.util.Optional;

import com.jsmart.ecmos.entity.Contact;
import com.jsmart.ecmos.service.ContactService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;

/**
 * @author Nikolay Koretskyy
 *
 */
@Controller
public class ContactsAdminController {

	@Autowired
	private ContactService contactService;

	@Value("${site.language}")
	private String language;

	@GetMapping("/admin/contacts")
	public String contcat(Model model) {
		model.addAttribute("contentTpl", "contact");
		model.addAttribute("contact", contactService.getOne(language));
		return "admin/page";
	}

	@PutMapping(value = "/admin/contacts", consumes = MediaType.APPLICATION_JSON_UTF8_VALUE, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public ResponseEntity<Contact> update(@RequestBody Contact contact) {
		if (Optional.ofNullable(contact).isPresent()) {
			Contact updatedContact = contactService.update(contact, language);
			if (Optional.ofNullable(updatedContact).isPresent()) {
				return new ResponseEntity<Contact>(updatedContact, HttpStatus.OK);
			} else {
				return new ResponseEntity<Contact>(HttpStatus.NOT_FOUND);
			}
		} else {
			return new ResponseEntity<Contact>(HttpStatus.BAD_REQUEST);
		}
	}

}
