package com.jsmart.ecmos.controller.admin;

import java.io.IOException;
import java.util.Optional;

import com.jsmart.ecmos.entity.Facility;
import com.jsmart.ecmos.service.FacilityService;
import com.jsmart.ecmos.service.technical.FileProcessingService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.multipart.MultipartFile;

import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * @author Sergey Khomich
 *
 */
@Controller
public class FacilityAdminController {

	@Autowired
	private FacilityService facilityService;

	@Autowired
	FileProcessingService fileProcessingService;

	@Autowired
	private ObjectMapper mapper;

	@Value("${site.language}")
	private String language;
	
	private Facility savedFacility;

	@GetMapping("/admin/facility")
	public String contcat(Model model) {
		model.addAttribute("contentTpl", "facility");
		model.addAttribute("facility", facilityService.getOne(language));
		return "admin/page";
	}

	@PutMapping(value = "/admin/facility", consumes = "multipart/form-data", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public ResponseEntity<Facility> update(	@RequestPart(value="jsonFacility") String jsonFacility,
											@RequestPart(value="facilityPic", required = false) MultipartFile facilityPic) {
		try {
			Facility facility = mapper.readValue(jsonFacility, Facility.class);
			savedFacility = facilityService.update(facility, language);
			if (Optional.ofNullable(savedFacility).isPresent()) {
				if (Optional.ofNullable(facilityPic).isPresent()) {
					fileProcessingService.saveFacilityImage(facilityPic);
				}
				return new ResponseEntity<Facility>(savedFacility, HttpStatus.OK);
			} else {
				return new ResponseEntity<Facility>(HttpStatus.NOT_FOUND);
			}
		} catch (IOException e) {
			e.printStackTrace();
			return new ResponseEntity<Facility>(savedFacility, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

}
