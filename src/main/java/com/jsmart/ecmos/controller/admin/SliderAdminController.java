package com.jsmart.ecmos.controller.admin;

import java.io.IOException;
import java.util.List;
import java.util.Optional;

import com.jsmart.ecmos.entity.Slide;
import com.jsmart.ecmos.service.SlideService;
import com.jsmart.ecmos.service.technical.FileProcessingService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.multipart.MultipartFile;

import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * @author Nikolay Koretskyy
 *
 */
@Controller
public class SliderAdminController {

	@Autowired
	private SlideService slideService;

	@Autowired
	private FileProcessingService fileProcessingService;

	@Autowired
	private ObjectMapper mapper;

	@Value("${site.language}")
	private String language;

	private Slide savedSlide;

	@GetMapping("/admin/slider")
	public String slidesList(Model model) {
		model.addAttribute("contentTpl", "slider");
		model.addAttribute("slides", slideService.getForAdmin(language));
		return "admin/page";
	}

	@PutMapping(value = "/admin/slider/ordering", consumes = MediaType.APPLICATION_JSON_UTF8_VALUE, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public ResponseEntity<List<Slide>> updateOrderings(@RequestBody List<Slide> slides) {
		if (Optional.ofNullable(slides).isPresent()) {
			List<Slide> updatedSlidesOrdering = slideService.updateOrderings(slides, language);
			if (Optional.ofNullable(slides).isPresent()) {
				return new ResponseEntity<List<Slide>>(updatedSlidesOrdering, HttpStatus.OK);
			} else {
				return new ResponseEntity<List<Slide>>(HttpStatus.NOT_FOUND);
			}
		} else {
			return new ResponseEntity<List<Slide>>(HttpStatus.BAD_REQUEST);
		}
	}

	@PostMapping(value = "/admin/slider", consumes = "multipart/form-data", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public ResponseEntity<Slide> create(
										@RequestPart(value="jsonSlide", required = true) String jsonSlide,
										@RequestPart(value="slidePic", required = true) MultipartFile slidePic) {
		try {
			Slide slide = mapper.readValue(jsonSlide, Slide.class);
			savedSlide = slideService.create(slide, language);
			if (Optional.ofNullable(slidePic).isPresent()) {
				fileProcessingService.saveSlideImage(savedSlide.getId(), slidePic);
			}
		} catch (IOException e) {
			e.printStackTrace();
			return new ResponseEntity<Slide>(HttpStatus.INTERNAL_SERVER_ERROR);
		}

		if (Optional.ofNullable(savedSlide).isPresent()) {
			return new ResponseEntity<Slide>(savedSlide, HttpStatus.CREATED);
		} else {
			return new ResponseEntity<Slide>(HttpStatus.INTERNAL_SERVER_ERROR);
		}

	}

	@PutMapping(value = "/admin/slider/{id}", consumes = "multipart/form-data", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public ResponseEntity<Slide> update(
										@PathVariable("id") int id,
										@RequestPart(value="jsonSlide", required = true) String jsonSlide,
										@RequestPart(value="slidePic", required = false) MultipartFile slidePic) {
		try {
			Slide slide = mapper.readValue(jsonSlide, Slide.class);
			if ((slide.getId() != id) || (id <= 0)) {
				return new ResponseEntity<Slide>(HttpStatus.BAD_REQUEST);
			}
			savedSlide = slideService.update(slide, language);
			if (Optional.ofNullable(savedSlide).isPresent()) {
				if (Optional.ofNullable(slidePic).isPresent()) {
					fileProcessingService.saveSlideImage(savedSlide.getId(), slidePic);
				}
				return new ResponseEntity<Slide>(savedSlide, HttpStatus.OK);
			} else {
				return new ResponseEntity<Slide>(HttpStatus.NOT_FOUND);
			}
		} catch (IOException e) {
			e.printStackTrace();
			return new ResponseEntity<Slide>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@DeleteMapping (value = "/admin/slider/{id}")
	public ResponseEntity<Void> remove(
									@PathVariable("id") int id) {
		if (!slideService.remove(id)) {
			return new ResponseEntity<Void>(HttpStatus.NOT_FOUND);
		} else {
			return new ResponseEntity<Void>(HttpStatus.NO_CONTENT);
		}
	}

}
