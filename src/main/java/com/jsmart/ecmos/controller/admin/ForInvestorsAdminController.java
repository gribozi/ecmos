package com.jsmart.ecmos.controller.admin;

import java.util.Optional;

import com.jsmart.ecmos.entity.ForInvestors;
import com.jsmart.ecmos.service.ForInvestorsService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;

/**
 * @author Nikolay Koretskyy
 *
 */
@Controller
public class ForInvestorsAdminController {

	@Autowired
	private ForInvestorsService forInvestorsService;

	@Value("${site.language}")
	private String language;

	@GetMapping("/admin/forinvestors")
	public String contcat(Model model) {
		model.addAttribute("contentTpl", "forinvestors");
		model.addAttribute("forInvestors", forInvestorsService.getOne(language));
		return "admin/page";
	}

	@PutMapping(value = "/admin/forinvestors", consumes = MediaType.APPLICATION_JSON_UTF8_VALUE, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public ResponseEntity<ForInvestors> update(@RequestBody ForInvestors forInvestors) {
		if (Optional.ofNullable(forInvestors).isPresent()) {
			ForInvestors updatedForInvestors = forInvestorsService.update(forInvestors, language);
			if (Optional.ofNullable(updatedForInvestors).isPresent()) {
				return new ResponseEntity<ForInvestors>(updatedForInvestors, HttpStatus.OK);
			} else {
				return new ResponseEntity<ForInvestors>(HttpStatus.NOT_FOUND);
			}
		} else {
			return new ResponseEntity<ForInvestors>(HttpStatus.BAD_REQUEST);
		}
	}

}
