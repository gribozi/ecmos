package com.jsmart.ecmos.controller.admin;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

/**
 * @author Nikolay Koretskyy
 *
 */
@Controller
public class AdminController {

	@GetMapping("/admin/login")
	public String login() {
		return "admin/login";
	}

	@GetMapping("/admin/panel")
	public String main() {
		return "admin/page";
	}

}
