package com.jsmart.ecmos.controller.admin;

import java.util.List;
import java.util.Optional;

import com.jsmart.ecmos.entity.Faq;
import com.jsmart.ecmos.service.FaqService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;

/**
 * @author Nikolay Koretskyy
 *
 */
@Controller
public class FaqsAdminController {

	@Autowired
	private FaqService faqService;

	@Value("${site.language}")
	private String language;

	private Faq savedFaq;

	@GetMapping("/admin/faqs")
	public String faqsList(Model model) {
		model.addAttribute("contentTpl", "faqs");
		model.addAttribute("faqs", faqService.getForAdmin(language));
		return "admin/page";
	}

	@PutMapping(value = "/admin/faqs/ordering", consumes = MediaType.APPLICATION_JSON_UTF8_VALUE, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public ResponseEntity<List<Faq>> updateOrderings(@RequestBody List<Faq> faqs) {
		if (Optional.ofNullable(faqs).isPresent()) {
			List<Faq> updatedFaqsOrdering = faqService.updateOrderings(faqs, language);
			if (Optional.ofNullable(faqs).isPresent()) {
				return new ResponseEntity<List<Faq>>(updatedFaqsOrdering, HttpStatus.OK);
			} else {
				return new ResponseEntity<List<Faq>>(HttpStatus.NOT_FOUND);
			}
		} else {
			return new ResponseEntity<List<Faq>>(HttpStatus.BAD_REQUEST);
		}
	}

	@PostMapping(value = "/admin/faqs", consumes = MediaType.APPLICATION_JSON_UTF8_VALUE, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public ResponseEntity<Faq> create(@RequestBody(required = true) Faq faq) {
		savedFaq = faqService.create(faq, language);
		if (Optional.ofNullable(savedFaq).isPresent()) {
			return new ResponseEntity<Faq>(savedFaq, HttpStatus.CREATED);
		} else {
			return new ResponseEntity<Faq>(HttpStatus.INTERNAL_SERVER_ERROR);
		}

	}

	@PutMapping(value = "/admin/faqs/{id}", consumes = MediaType.APPLICATION_JSON_UTF8_VALUE, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public ResponseEntity<Faq> update(@PathVariable("id") int id, @RequestBody(required = true) Faq faq) {
		if ((faq.getId() != id) || (id <= 0)) {
			return new ResponseEntity<Faq>(HttpStatus.BAD_REQUEST);
		}
		savedFaq = faqService.update(faq, language);
		if (Optional.ofNullable(savedFaq).isPresent()) {
			return new ResponseEntity<Faq>(savedFaq, HttpStatus.OK);
		} else {
			return new ResponseEntity<Faq>(HttpStatus.NOT_FOUND);
		}
	}

	@DeleteMapping (value = "/admin/faqs/{id}")
	public ResponseEntity<Void> remove(@PathVariable("id") int id) {
		if (!faqService.remove(id)) {
			return new ResponseEntity<Void>(HttpStatus.NOT_FOUND);
		} else {
			return new ResponseEntity<Void>(HttpStatus.NO_CONTENT);
		}
	}

}
