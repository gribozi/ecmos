package com.jsmart.ecmos.controller.admin;

import java.io.IOException;
import java.util.List;
import java.util.Optional;

import com.jsmart.ecmos.entity.ForTeamText;
import com.jsmart.ecmos.entity.Picture;
import com.jsmart.ecmos.service.PictureService;
import com.jsmart.ecmos.service.technical.FileProcessingService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.multipart.MultipartFile;

import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * @author Nikolay Koretskyy
 *
 */
@Controller
public class PicturesAdminController {

	@Autowired
	private PictureService pictureService;

	@Autowired
	private FileProcessingService fileProcessingService;

	@Autowired
	private ObjectMapper mapper;

	@Value("${site.language}")
	private String language;

	private Picture savedPicture;

	@GetMapping("/admin/pictures/{type}")
	public String picturesList(Model model, @PathVariable("type") String type) {
		model.addAttribute("contentTpl", "pictures");
		model.addAttribute("pictureType", type);
		model.addAttribute("pictures", pictureService.getByTypeForAdmin(type, language));
		model.addAttribute("forTeamText", pictureService.getOneTeamText(language));
		return "admin/page";
	}

	@PutMapping(value = "/admin/pictures/ordering", consumes = MediaType.APPLICATION_JSON_UTF8_VALUE, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public ResponseEntity<List<Picture>> updateOrderings(@RequestBody List<Picture> pictures) {
		if (Optional.ofNullable(pictures).isPresent()) {
			List<Picture> updatedPicturesOrdering = pictureService.updateOrderings(pictures, language);
			if (Optional.ofNullable(pictures).isPresent()) {
				return new ResponseEntity<List<Picture>>(updatedPicturesOrdering, HttpStatus.OK);
			} else {
				return new ResponseEntity<List<Picture>>(HttpStatus.NOT_FOUND);
			}
		} else {
			return new ResponseEntity<List<Picture>>(HttpStatus.BAD_REQUEST);
		}
	}

	@PostMapping(value = "/admin/pictures/{type}", consumes = "multipart/form-data", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public ResponseEntity<Picture> create(
										@PathVariable("type") String type,
										@RequestPart(value="jsonPicture", required = true) String jsonPicture,
										@RequestPart(value="picturePic", required = false) MultipartFile picturePic) {
		try {
			Picture picture = mapper.readValue(jsonPicture, Picture.class);
			savedPicture = pictureService.create(picture, type, language);
			if (Optional.ofNullable(picturePic).isPresent()) {
				fileProcessingService.savePictureImage(savedPicture.getId(), type, picturePic);
			}
		} catch (IOException e) {
			e.printStackTrace();
			return new ResponseEntity<Picture>(HttpStatus.INTERNAL_SERVER_ERROR);
		}

		if (Optional.ofNullable(savedPicture).isPresent()) {
			return new ResponseEntity<Picture>(savedPicture, HttpStatus.CREATED);
		} else {
			return new ResponseEntity<Picture>(HttpStatus.INTERNAL_SERVER_ERROR);
		}

	}

	@PutMapping(value = "/admin/pictures/{type}/{id}", consumes = "multipart/form-data", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public ResponseEntity<Picture> update(
										@PathVariable("type") String type,
										@PathVariable("id") int id,
										@RequestPart(value="jsonPicture", required = true) String jsonPicture,
										@RequestPart(value="picturePic", required = false) MultipartFile picturePic) {
		try {
			Picture picture = mapper.readValue(jsonPicture, Picture.class);
			if ((picture.getId() != id) || (id <= 0)) {
				return new ResponseEntity<Picture>(HttpStatus.BAD_REQUEST);
			}
			savedPicture = pictureService.update(picture, type, language);
			if (Optional.ofNullable(savedPicture).isPresent()) {
				if (Optional.ofNullable(picturePic).isPresent()) {
					fileProcessingService.savePictureImage(savedPicture.getId(), type, picturePic);
				}
				return new ResponseEntity<Picture>(savedPicture, HttpStatus.OK);
			} else {
				return new ResponseEntity<Picture>(HttpStatus.NOT_FOUND);
			}
		} catch (IOException e) {
			e.printStackTrace();
			return new ResponseEntity<Picture>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@DeleteMapping (value = "/admin/pictures/{type}/{id}")
	public ResponseEntity<Void> remove(
									@PathVariable("type") String type,
									@PathVariable("id") int id) {
		if (!pictureService.remove(id, type)) {
			return new ResponseEntity<Void>(HttpStatus.NOT_FOUND);
		} else {
			return new ResponseEntity<Void>(HttpStatus.NO_CONTENT);
		}
	}
	
	@PutMapping(value = "/admin/pictures/teamtext", consumes = MediaType.APPLICATION_JSON_UTF8_VALUE, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public ResponseEntity<ForTeamText> updateTeamText(@RequestBody ForTeamText forTeamText) {
		if (Optional.ofNullable(forTeamText).isPresent()) {
			ForTeamText updatedForTeamText = pictureService.updateTeamText(forTeamText, language);
			if (Optional.ofNullable(updatedForTeamText).isPresent()) {
				return new ResponseEntity<ForTeamText>(updatedForTeamText, HttpStatus.OK);
			} else {
				return new ResponseEntity<ForTeamText>(HttpStatus.NOT_FOUND);
			}
		} else {
			return new ResponseEntity<ForTeamText>(HttpStatus.BAD_REQUEST);
		}
	}
}
