package com.jsmart.ecmos;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author Nikolay Koretskyy
 *
 */
@SpringBootApplication
public class EcmosApplication {

	public static void main(String[] args) {
		SpringApplication.run(EcmosApplication.class, args);
	}
}
