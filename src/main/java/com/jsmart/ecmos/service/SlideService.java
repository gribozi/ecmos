package com.jsmart.ecmos.service;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import com.jsmart.ecmos.entity.Slide;
import com.jsmart.ecmos.entity.SlideContent;
import com.jsmart.ecmos.repository.LanguageRepository;
import com.jsmart.ecmos.repository.SlideRepository;
import com.jsmart.ecmos.service.technical.FileProcessingService;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author Sergey Khomich
 *
 */
@Service
public class SlideService {

	@Autowired
	private SlideRepository slideRepository;

	@Autowired
	private LanguageRepository languageRepository;

	@Autowired
	private FileProcessingService fileProcessingService;

	private static final Logger log = Logger.getLogger(SlideService.class);

	public Slide getOne(int id, String language) {
		log.info("Fetching slide with id " + id);
		Slide slide = slideRepository.getOne(id);
		if (Optional.ofNullable(slide).isPresent()) {
			List<SlideContent> slideContents = slide.getSlideContents()
					.stream()
					.filter((y) -> y.getLanguage().getName().equals(language))
					.collect(Collectors.toList());
			slide.setSlideContents(slideContents);
			return slide;
		} else {
			log.info("Slide with id " + id + " was not found");
			return null;
		}
	}

	public List<Slide> getAll(String language) {
		log.info("Fetching active slides");
		List<Slide> slides = slideRepository.findByActiveOrderByOrderingAsc(true);
		slides.forEach((x) -> {
			List<SlideContent> slideContents = x.getSlideContents()
					.stream()
					.filter((y) -> y.getLanguage().getName().equals(language))
					.collect(Collectors.toList());
			x.setSlideContents(slideContents);
		});
		return slides;
	}

	public List<Slide> getForAdmin(String language) {
		log.info("Fetching all slides");
		List<Slide> slides = slideRepository.findByOrderByOrderingAsc();
		slides.forEach((x) -> {
			List<SlideContent> slideContents = x.getSlideContents()
					.stream()
					.filter((y) -> y.getLanguage().getName().equals(language))
					.collect(Collectors.toList());
			x.setSlideContents(slideContents);
		});
		return slides;
	}

	public List<Slide> updateOrderings(List<Slide> slides, String language) {
		log.info("Updating orderings for slides");
		return slides
				.stream()
				.map((x) -> {
					Slide slide = slideRepository.getOne(x.getId());
					slide.setOrdering(x.getOrdering());
					return slideRepository.save(slide);
				})
				.collect(Collectors.toList());
	}

	public int getNextOrdering(){
		List<Slide> slides = slideRepository.findAll();
		Optional<Slide> maxOrderSlide = slides.stream().max(Comparator.comparing(Slide::getOrdering));
		if (maxOrderSlide.isPresent()) {
			return maxOrderSlide.get().getOrdering() + 1;
		} else {
			return 1;
		}
	}

	public Slide create (Slide slide, String language) {

		log.info("Creating slide " + slide.getSlideContents().get(0).getTextBig());

		slide.getSlideContents().get(0).setLanguage(languageRepository.findByName(language));

		if ((!Optional.ofNullable(slide.getOrdering()).isPresent()) || (slide.getOrdering() < 1)) {
			slide.setOrdering(this.getNextOrdering());
		}

		SlideContent slideContentForSave = slide.getSlideContents().get(0);
		slide.setSlideContents(null);
		Slide savedSlide = slideRepository.save(slide);
		List<SlideContent> slideContents = new ArrayList<>();
		slideContentForSave.setSlide(savedSlide);
		slideContents.add(slideContentForSave);
		savedSlide.setSlideContents(slideContents);
		savedSlide = slideRepository.save(savedSlide);

		log.info("Slide " + savedSlide.getSlideContents().get(0).getTextBig() + " was created");
		return savedSlide;
	}

	public Slide update(Slide slide, String language) {

		log.info("Updating slide " + slide.getSlideContents().get(0).getTextBig());

		Slide slideOld = this.getOne(slide.getId(), language);

		if (Optional.ofNullable(slideOld).isPresent()) {

			slide.getSlideContents().get(0).setLanguage(languageRepository.findByName(language));

			slide.getSlideContents().get(0).setId(slideOld.getSlideContents().get(0).getId());

			slide.getSlideContents().get(0).setSlide(slide);
			Slide slideUpdated = slideRepository.save(slide);
			log.info("Slide " + slideUpdated.getSlideContents().get(0).getTextBig() + " was updated");
			return slideUpdated;

		} else {
			log.info("Slide " + slide.getSlideContents().get(0).getTextBig() + " was not found");
			return null;
		}
	}

	public boolean remove(int id) {
		log.info("Fetching slide with id " + id);
		Slide slide = slideRepository.getOne(id);
		if (Optional.ofNullable(slide).isPresent()) {
			slideRepository.deleteById(id);
			fileProcessingService.removeSlideImage(id);
			log.info("Slide removed");
			return true;
		} else {
			log.info("Unable to delete. Slide with id " + id + " not found");
			return false;
		}
	}

}
