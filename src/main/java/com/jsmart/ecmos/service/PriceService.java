package com.jsmart.ecmos.service;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import com.jsmart.ecmos.entity.Price;
import com.jsmart.ecmos.entity.PriceContent;
import com.jsmart.ecmos.repository.LanguageRepository;
import com.jsmart.ecmos.repository.PriceRepository;
import com.jsmart.ecmos.service.technical.ExcelReadingService;

import com.jsmart.ecmos.service.technical.FileProcessingService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author Nikolay Koretskyy
 *
 */
@Service
public class PriceService {

	@Autowired
	private PriceRepository priceRepository;

	@Autowired
	private LanguageRepository languageRepository;

	@Autowired
	private FileProcessingService fileProcessingService;

	@Autowired
	private ExcelReadingService excelReadingService;

	private static final Logger log = Logger.getLogger(PriceService.class);

	public Price getOne(int id, String language) {
		log.info("Fetching price with id " + id);
		Price price = priceRepository.getOne(id);
		if (Optional.ofNullable(price).isPresent()) {
			List<PriceContent> priceContents = price.getPriceContents()
					.stream()
					.filter((y) -> y.getLanguage().getName().equals(language))
					.collect(Collectors.toList());
			price.setPriceContents(priceContents);
			return price;
		} else {
			log.info("Price with id " + id + " was not found");
			return null;
		}
	}

	public List<Price> getAll(String language) {
		log.info("Fetching active prices");
		List<Price> prices = priceRepository.findByActiveOrderByOrderingAsc(true);
		prices.forEach((x) -> {
			List<PriceContent> priceContents = x.getPriceContents()
					.stream()
					.filter((y) -> y.getLanguage().getName().equals(language))
					.collect(Collectors.toList());
			x.setPriceContents(priceContents);
			x.setPriceExcelContent(excelReadingService.getDataFromExcelFile(x.getId(), language));
		});
		return prices;
	}

	public List<Price> getForAdmin(String language) {
		log.info("Fetching all prices");
		List<Price> prices = priceRepository.findByOrderByOrderingAsc();
		prices.forEach((x) -> {
			List<PriceContent> priceContents = x.getPriceContents()
					.stream()
					.filter((y) -> y.getLanguage().getName().equals(language))
					.collect(Collectors.toList());
			x.setPriceContents(priceContents);
		});
		return prices;
	}

	public List<Price> updateOrderings(List<Price> prices, String language) {
		log.info("Updating orderings for prices");
		return prices
				.stream()
				.map((x) -> {
					Price price = priceRepository.getOne(x.getId());
					price.setOrdering(x.getOrdering());
					return priceRepository.save(price);
				})
				.collect(Collectors.toList());
	}

	public int getNextOrdering(){
		List<Price> prices = priceRepository.findAll();
		Optional<Price> maxOrderPrice = prices.stream().max(Comparator.comparing(Price::getOrdering));
		if (maxOrderPrice.isPresent()) {
			return maxOrderPrice.get().getOrdering() + 1;
		} else {
			return 1;
		}
	}

	public Price create (Price price, String language) {

		log.info("Creating price " + price.getPriceContents().get(0).getName());

		price.getPriceContents().get(0).setLanguage(languageRepository.findByName(language));

		if ((!Optional.ofNullable(price.getOrdering()).isPresent()) || (price.getOrdering() < 1)) {
			price.setOrdering(this.getNextOrdering());
		}

		PriceContent priceContentForSave = price.getPriceContents().get(0);
		price.setPriceContents(null);
		Price savedPrice = priceRepository.save(price);
		List<PriceContent> priceContents = new ArrayList<>();
		priceContentForSave.setPrice(savedPrice);
		priceContents.add(priceContentForSave);
		savedPrice.setPriceContents(priceContents);
		savedPrice = priceRepository.save(savedPrice);

		log.info("Price " + savedPrice.getPriceContents().get(0).getName() + " was created");
		return savedPrice;
	}

	public Price update(Price price, String language) {

		log.info("Updating price " + price.getPriceContents().get(0).getName());

		Price priceOld = this.getOne(price.getId(), language);

		if (Optional.ofNullable(priceOld).isPresent()) {

			price.getPriceContents().get(0).setLanguage(languageRepository.findByName(language));

			price.getPriceContents().get(0).setId(priceOld.getPriceContents().get(0).getId());

			price.getPriceContents().get(0).setPrice(price);
			Price priceUpdated = priceRepository.save(price);
			log.info("Price " + priceUpdated.getPriceContents().get(0).getName() + " was updated");
			return priceUpdated;

		} else {
			log.info("Price " + price.getPriceContents().get(0).getName() + " was not found");
			return null;
		}
	}

	public boolean remove(int id, String language) {
		log.info("Fetching price with id " + id);
		Price price = priceRepository.getOne(id);
		if (Optional.ofNullable(price).isPresent()) {
			priceRepository.deleteById(id);
			fileProcessingService.removePriceFile(id, language);
			log.info("Price removed");
			return true;
		} else {
			log.info("Unable to delete. Price with id " + id + " not found");
			return false;
		}
	}

}
