package com.jsmart.ecmos.service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import com.jsmart.ecmos.entity.Facility;
import com.jsmart.ecmos.entity.FacilityContent;
import com.jsmart.ecmos.repository.FacilityRepository;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author Sergey Khomich
 *
 */
@Service
public class FacilityService {

	@Autowired
	private FacilityRepository facilityRepository;

	private static final Logger log = Logger.getLogger(FacilityService.class);

	public Facility getOne(String language) {
		log.info("Fetching facility");
		Facility facility = facilityRepository.getOne(1);
		if (Optional.ofNullable(facility).isPresent()) {
			List<FacilityContent> facilityContents = facility.getFacilityContents()
					.stream()
					.filter((y) -> y.getLanguage().getName().equals(language))
					.collect(Collectors.toList());
			facility.setFacilityContents(facilityContents);
			return facility;
		} else {
			log.info("Facility was not found");
			return null;
		}
	}

	public Facility update(Facility facility, String language) {
		Facility facilityOld = this.getOne(language);
		if (Optional.ofNullable(facilityOld).isPresent()) {
			log.info("Updating facility");
			facility.setId(facilityOld.getId());
			facility.setActive(facilityOld.isActive());
			facility.setOrdering(facilityOld.getOrdering());
			facility.getFacilityContents().get(0).setFacility(facility);
			facility.getFacilityContents().get(0).setId(facilityOld.getFacilityContents().get(0).getId());
			facility.getFacilityContents().get(0).setLanguage(facilityOld.getFacilityContents().get(0).getLanguage());
			Facility updatedFacility = facilityRepository.save(facility);
			log.info("Facility was updated");
			return updatedFacility;
		} else {
			log.info("Facility was not found");
			return null;
		}
	}
}
