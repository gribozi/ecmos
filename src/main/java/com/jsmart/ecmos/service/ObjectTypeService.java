package com.jsmart.ecmos.service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import com.jsmart.ecmos.entity.ObjectType;
import com.jsmart.ecmos.entity.ObjectTypeContent;
import com.jsmart.ecmos.repository.LanguageRepository;
import com.jsmart.ecmos.repository.ObjectTypeRepository;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * @author Nikolay Koretskyy
 *
 */
@Service
public class ObjectTypeService {

	@Data
	@AllArgsConstructor
	private class ObjectsMenuItem {
		private int id;
		private String name;
		private String uri;
	}

	@Autowired
	private ObjectTypeRepository objectTypeRepository;

	@Autowired
	LanguageRepository languageRepository;

	private static final Logger log = Logger.getLogger(ObjectTypeService.class);

	public ObjectType getOne(String uri, String language) {
		log.info("Fetching objectType with uri " + uri);
		ObjectType objectType = objectTypeRepository.findByUri(uri);
		if (Optional.ofNullable(objectType).isPresent()) {
			List<ObjectTypeContent> objectTypeContents = objectType.getObjectTypeContents()
					.stream()
					.filter((y) -> y.getLanguage().getName().equals(language))
					.collect(Collectors.toList());
			objectType.setObjectTypeContents(objectTypeContents);
			return objectType;
		} else {
			log.info("ObjectType with uri " + uri + " was not found");
			return null;
		}
	}

	public List<ObjectsMenuItem> getObjectsMenu(String language) {
		log.info("Fetching active objectsMenuItems");
		List<ObjectsMenuItem> objectsMenuItems = objectTypeRepository.findByActiveOrderByOrderingAsc(true)
				.stream()
				.map((x) -> {
					ObjectTypeContent objectTypeContent = x.getObjectTypeContents()
							.stream()
							.filter((y) -> y.getLanguage().getName().equals(language))
							.findFirst()
							.get();
					return new ObjectsMenuItem(x.getId(), objectTypeContent.getName(), x.getUri());
				})
				.collect(Collectors.toList());
		return objectsMenuItems;
	}

	public List<ObjectType> getForAdmin(String language) {
		log.info("Fetching all objectTypes");
		List<ObjectType> objectTypes = objectTypeRepository.findByOrderByOrderingAsc();
		objectTypes.forEach((x) -> {
			List<ObjectTypeContent> objectTypeContents = x.getObjectTypeContents()
					.stream()
					.filter((y) -> y.getLanguage().getName().equals(language))
					.collect(Collectors.toList());
			x.setObjectTypeContents(objectTypeContents);
		});
		return objectTypes;
	}

	public List<ObjectType> updateOrderings(List<ObjectType> objectTypes, String language) {
		log.info("Updating orderings for objectTypes");
		return objectTypes
				.stream()
				.map((x) -> {
					ObjectType objectType = objectTypeRepository.getOne(x.getId());
					objectType.setOrdering(x.getOrdering());
					return objectTypeRepository.save(objectType);
				})
				.collect(Collectors.toList());
	}

}
