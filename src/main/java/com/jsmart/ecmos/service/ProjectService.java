package com.jsmart.ecmos.service;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import com.jsmart.ecmos.entity.ObjectType;
import com.jsmart.ecmos.entity.ObjectTypeContent;
import com.jsmart.ecmos.entity.Project;
import com.jsmart.ecmos.entity.ProjectContent;
import com.jsmart.ecmos.repository.LanguageRepository;
import com.jsmart.ecmos.repository.ObjectTypeRepository;
import com.jsmart.ecmos.repository.ProjectRepository;
import com.jsmart.ecmos.service.technical.FileProcessingService;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * @author Nikolay Koretskyy
 *
 */
@Service
public class ProjectService {

	@Data
	@AllArgsConstructor
	private class ObjectsMenuItem {
		private int id;
		private String name;
		private String uri;
	}

	@Autowired
	private ProjectRepository projectRepository;

	@Autowired
	private ObjectTypeRepository objectTypeRepository;

	@Autowired
	LanguageRepository languageRepository;

	@Autowired
	private FileProcessingService fileProcessingService;

	private static final Logger log = Logger.getLogger(ProjectService.class);

	public Project getOne(int id, String language) {
		log.info("Fetching project with id " + id);
		Project project = projectRepository.getOne(id);
		if (Optional.ofNullable(project).isPresent()) {
			List<ProjectContent> projectContents = project.getProjectContents()
					.stream()
					.filter((y) -> y.getLanguage().getName().equals(language))
					.collect(Collectors.toList());
			project.setProjectContents(projectContents);
			return project;
		} else {
			log.info("Project with id " + id + " was not found");
			return null;
		}
	}

	public List<Project> getByType(String type, String language) {
		log.info("Fetching active projects with type " + type);
		List<Project> projects = new ArrayList<Project>();
		switch (type) {
			case "projects":
				projects = projectRepository.findByTypeProjectAndActiveOrderByOrderingAsc(true, true);
				break;
			case "realtors":
				projects= projectRepository.findByTypeRealtorAndActiveOrderByOrderingAsc(true, true);
				break;
			case "objects":
					projects = projectRepository.findByTypeObjectAndActiveOrderByOrderingAsc(true, true);
				break;
			default:
				throw new IllegalArgumentException("Invalid parammetr: " + type);
		}
		projects.forEach((x) -> {
			List<ProjectContent> projectContents = x.getProjectContents()
					.stream()
					.filter((y) -> y.getLanguage().getName().equals(language))
					.collect(Collectors.toList());
			x.setProjectContents(projectContents);
		});
		return projects;
	}

	public List<Project> getObjectsByObjectType(ObjectType objectType, String language) {
		log.info("Fetching active objects with objectType " + objectType);
		List<Project> projects = new ArrayList<Project>();
		projects = projectRepository.findByTypeObjectAndActiveAndObjectTypeOrderByOrderingAsc(true, true, objectType);
		projects.forEach((x) -> {
			List<ProjectContent> projectContents = x.getProjectContents()
					.stream()
					.filter((y) -> y.getLanguage().getName().equals(language))
					.collect(Collectors.toList());
			x.setProjectContents(projectContents);
		});
		return projects;
	}

	public List<Project> getProjectsForIndexPage(String language) {
		log.info("Fetching active projects for index page with objectType");
		List<Project> projects = new ArrayList<Project>();
		projects = projectRepository.findByTypeProjectAndActiveAndIndexOutputOrderByOrderingAsc(true, true, true);
		projects.forEach((x) -> {
			List<ProjectContent> projectContents = x.getProjectContents()
					.stream()
					.filter((y) -> y.getLanguage().getName().equals(language))
					.collect(Collectors.toList());
			x.setProjectContents(projectContents);
		});
		return projects;
	}

	public List<Project> getProjectsFiltered(int floor, Integer minSquare, Integer maxSquare, String language) {
		List<Project> projects = this.getByType("projects", language);
		List<Project> projectsFiltered = projects.stream().filter((p -> {
			if (Optional.ofNullable(p.getSquare()).isPresent()) {
				if ((p.getSquare() >= minSquare) && (p.getSquare() <= maxSquare)) {

					if (floor == 0) {
						return true;
					}

					if ((floor == 1) && (Optional.ofNullable(p.getProjectContents().get(0).getFloor1description()).isPresent())
							&& (!Optional.ofNullable(p.getProjectContents().get(0).getFloor2description()).isPresent())
							&& (!Optional.ofNullable(p.getProjectContents().get(0).getFloor3description()).isPresent())
							&& (!Optional.ofNullable(p.getProjectContents().get(0).getFloor4description()).isPresent())) {
						return true;
					}
					if ((floor == 2) && (Optional.ofNullable(p.getProjectContents().get(0).getFloor1description()).isPresent())
							&& (Optional.ofNullable(p.getProjectContents().get(0).getFloor2description()).isPresent())
							&& (!Optional.ofNullable(p.getProjectContents().get(0).getFloor3description()).isPresent())
							&& (!Optional.ofNullable(p.getProjectContents().get(0).getFloor4description()).isPresent())) {
						return true;
					}
					if ((floor == 3) && (Optional.ofNullable(p.getProjectContents().get(0).getFloor1description()).isPresent())
							&& (Optional.ofNullable(p.getProjectContents().get(0).getFloor2description()).isPresent())
							&& (Optional.ofNullable(p.getProjectContents().get(0).getFloor3description()).isPresent())
							&& (!Optional.ofNullable(p.getProjectContents().get(0).getFloor4description()).isPresent())) {
						return true;
					}
					if ((floor == 4) && (Optional.ofNullable(p.getProjectContents().get(0).getFloor1description()).isPresent())
							&& (Optional.ofNullable(p.getProjectContents().get(0).getFloor2description()).isPresent())
							&& (Optional.ofNullable(p.getProjectContents().get(0).getFloor3description()).isPresent())
							&& (Optional.ofNullable(p.getProjectContents().get(0).getFloor4description()).isPresent())) {
						return true;
					}
				} else {
					return false;
				}
			} else {
				return false;
			}

			return false;
		})).collect(Collectors.toList());
		return projectsFiltered;
	}

	public ObjectType getObjectType(String uri, String language) {
		log.info("Fetching objectType with uri " + uri);
		ObjectType objectType = objectTypeRepository.findByUri(uri);
		if (Optional.ofNullable(objectType).isPresent()) {
			List<ObjectTypeContent> objectTypeContents = objectType.getObjectTypeContents()
					.stream()
					.filter((y) -> y.getLanguage().getName().equals(language))
					.collect(Collectors.toList());
			objectType.setObjectTypeContents(objectTypeContents);
			return objectType;
		} else {
			log.info("ObjectType with uri " + uri + " was not found");
			return null;
		}
	}

	public List<ObjectsMenuItem> getObjectsMenu(String language) {
		log.info("Fetching active objectsMenuItems");
		List<ObjectsMenuItem> objectsMenuItems = objectTypeRepository.findByActiveOrderByOrderingAsc(true)
				.stream()
				.map((x) -> {
					ObjectTypeContent objectTypeContent = x.getObjectTypeContents()
							.stream()
							.filter((y) -> y.getLanguage().getName().equals(language))
							.findFirst()
							.get();
					return new ObjectsMenuItem(x.getId(), objectTypeContent.getName(), x.getUri());
				})
				.collect(Collectors.toList());
		return objectsMenuItems;
	}

	public boolean isActionPeriod(Project project) {
		if (!Optional.ofNullable(project.getActionBegin()).isPresent()) {return false;}
		if (!Optional.ofNullable(project.getActionEnd()).isPresent()) {return false;}
		if (!Optional.ofNullable(project.getCoastAction()).isPresent()) {return false;}
		if (project.getActionBegin().isAfter(LocalDate.now())) {return false;}
		if (project.getActionEnd().isBefore(LocalDate.now())) {return false;}
		return true;
	}

	public boolean[] isActionPeriods(List<Project> projects) {
		boolean[] result = new boolean[projects.size()];
		int i = 0;
		for (Project project : projects) {
			result[i] = isActionPeriod(project);
			i++;
		}
		return result;
	}

	public Integer getProjectPicturesCount(Integer id) {
		if (Optional.ofNullable( ProjectService.class.getClassLoader().getResource("static/images/projects/" + id.toString() + "/pics/")).isPresent()) {
			return fileProcessingService.filesCount(ProjectService.class.getClassLoader().getResource("static/images/projects/" + id.toString() + "/pics/").toString().substring(5));
		} else {
			return 0;
		}
	}

	public List<Project> getByTypeForAdmin(String type, String language) {
		log.info("Fetching projects with type " + type);
		List<Project> projects = new ArrayList<Project>();
		switch (type) {
			case "projects":
				projects = projectRepository.findByTypeProject(true);
				break;
			case "realtors":
				projects= projectRepository.findByTypeRealtor(true);
				break;
			case "objects":
				projects = projectRepository.findByTypeObject(true);
				break;
			default:
				throw new IllegalArgumentException("Invalid parammetr: " + type);
		}
		projects.forEach((x) -> {
			List<ProjectContent> projectContents = x.getProjectContents()
					.stream()
					.filter((y) -> y.getLanguage().getName().equals(language))
					.collect(Collectors.toList());
			x.setProjectContents(projectContents);
		});
		return projects;
	}

	public List<Project> updateOrderings(List<Project> projects, String language) {
		log.info("Updating orderings for projects");
		return projects
				.stream()
				.map((x) -> {
					Project project = projectRepository.getOne(x.getId());
					project.setOrdering(x.getOrdering());
					return projectRepository.save(project);
				})
				.collect(Collectors.toList());
	}

	public int getNextOrdering() {
		List<Project> projects = projectRepository.findAll();
		Optional<Project> maxOrderProject = projects.stream().max(Comparator.comparing(Project::getOrdering));
		if (maxOrderProject.isPresent()) {
			return maxOrderProject.get().getOrdering() + 1;
		} else {
			return 1;
		}
	}

	public Project create(Project project, String language) {

		log.info("Creating project " + project.getProjectContents().get(0).getName());

		project.setOrdering(this.getNextOrdering());

		project.getProjectContents().get(0).setLanguage(languageRepository.findByName(language));

		if ("".equals(project.getProjectContents().get(0).getFloor1description())) {
			project.getProjectContents().get(0).setFloor1description(null);
		}
		if ("".equals(project.getProjectContents().get(0).getFloor2description())) {
			project.getProjectContents().get(0).setFloor2description(null);
		}
		if ("".equals(project.getProjectContents().get(0).getFloor3description())) {
			project.getProjectContents().get(0).setFloor3description(null);
		}
		if ("".equals(project.getProjectContents().get(0).getFloor4description())) {
			project.getProjectContents().get(0).setFloor4description(null);
		}

		ProjectContent projectContentForSave = project.getProjectContents().get(0);
		project.setProjectContents(null);
		Project savedProject = projectRepository.save(project);
		List<ProjectContent> projectContents = new ArrayList<>();
		projectContentForSave.setProject(savedProject);
		projectContents.add(projectContentForSave);
		savedProject.setProjectContents(projectContents);
		savedProject = projectRepository.save(savedProject);

		log.info("Project " + savedProject.getProjectContents().get(0).getName() + " was created");
		return savedProject;
	}

	public Project update(Project project, String language) {

		log.info("Updating project " + project.getProjectContents().get(0).getName());

		Project projectOld = this.getOne(project.getId(), language);

		if (Optional.ofNullable(projectOld).isPresent()) {

			project.setOrdering(projectOld.getOrdering());

			project.getProjectContents().get(0).setLanguage(languageRepository.findByName(language));

			project.getProjectContents().get(0).setId(projectOld.getProjectContents().get(0).getId());

			if ("".equals(project.getProjectContents().get(0).getFloor1description())) {
				project.getProjectContents().get(0).setFloor1description(null);
			}
			if ("".equals(project.getProjectContents().get(0).getFloor2description())) {
				project.getProjectContents().get(0).setFloor2description(null);
			}
			if ("".equals(project.getProjectContents().get(0).getFloor3description())) {
				project.getProjectContents().get(0).setFloor3description(null);
			}
			if ("".equals(project.getProjectContents().get(0).getFloor4description())) {
				project.getProjectContents().get(0).setFloor4description(null);
			}

			project.getProjectContents().get(0).setProject(project);
			Project projectUpdated = projectRepository.save(project);
			log.info("Project " + projectUpdated.getProjectContents().get(0).getName() + " was updated");
			return projectUpdated;

		} else {
			log.info("Project " + project.getProjectContents().get(0).getName() + " was not found");
			return null;
		}
	}

	public boolean remove(int id) {
		log.info("Fetching project with id " + id);
		Project project = projectRepository.getOne(id);
		if (Optional.ofNullable(project).isPresent()) {
			projectRepository.deleteById(id);
			fileProcessingService.removeAllProjectPics(id);
			log.info("Project removed");
			return true;
		} else {
			log.info("Unable to delete. Project with id " + id + " not found");
			return false;
		}
	}

}
