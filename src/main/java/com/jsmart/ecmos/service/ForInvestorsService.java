package com.jsmart.ecmos.service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import com.jsmart.ecmos.entity.ForInvestors;
import com.jsmart.ecmos.entity.ForInvestorsContent;
import com.jsmart.ecmos.repository.ForInvestorsRepository;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author Nikolay Koretskyy
 *
 */
@Service
public class ForInvestorsService {

	@Autowired
	private ForInvestorsRepository forInvestorsRepository;

	private static final Logger log = Logger.getLogger(ForInvestorsService.class);

	public ForInvestors getOne(String language) {
		log.info("Fetching forInvestors");
		ForInvestors forInvestors = forInvestorsRepository.getOne(1);
		if (Optional.ofNullable(forInvestors).isPresent()) {
			List<ForInvestorsContent> forInvestorsContents = forInvestors.getForInvestorsContents()
					.stream()
					.filter((y) -> y.getLanguage().getName().equals(language))
					.collect(Collectors.toList());
			forInvestors.setForInvestorsContents(forInvestorsContents);
			return forInvestors;
		} else {
			log.info("ForInvestors was not found");
			return null;
		}
	}

	public ForInvestors update(ForInvestors forInvestors, String language) {
		ForInvestors forInvestorsOld = this.getOne(language);
		if (Optional.ofNullable(forInvestorsOld).isPresent()) {
			log.info("Updating forInvestorss");
			forInvestors.setId(forInvestorsOld.getId());
			forInvestors.setActive(forInvestorsOld.isActive());
			forInvestors.setOrdering(forInvestorsOld.getOrdering());
			forInvestors.getForInvestorsContents().get(0).setForInvestors(forInvestors);
			forInvestors.getForInvestorsContents().get(0).setId(forInvestorsOld.getForInvestorsContents().get(0).getId());
			forInvestors.getForInvestorsContents().get(0).setLanguage(forInvestorsOld.getForInvestorsContents().get(0).getLanguage());
			ForInvestors updatedForInvestors = forInvestorsRepository.save(forInvestors);
			log.info("ForInvestorss was updated");
			return updatedForInvestors;
		} else {
			log.info("ForInvestorss was not found");
			return null;
		}
	}

}
