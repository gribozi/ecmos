package com.jsmart.ecmos.service.technical;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;

import com.jsmart.ecmos.entity.PicOrdering;

import org.apache.tomcat.util.http.fileupload.FileUtils;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

/**
 * @author Sergey Khomich
 *
 */
@Service
public class FileProcessingService {
	private BufferedOutputStream stream;
	private final String staticResourcesPath = FileProcessingService.class.getClassLoader().getResource("static/images/").toString().substring(5);
	private final String staticResourcesExcelPath = FileProcessingService.class.getClassLoader().getResource("static/prices/").toString().substring(5);

	public Integer filesCount(String toFolderPath) {
		if (Optional.ofNullable(toFolderPath).isPresent()) {
			return new File(toFolderPath).listFiles().length;
		} else {
			return 0;
		}
	}

	public void saveProjectImages(Integer projectId, String pathType, MultipartFile file) {
		String path = staticResourcesPath + "/projects/" + projectId.toString();
		File directoryForProject = new File(path);
		if (!directoryForProject.exists()) {
			directoryForProject.mkdir();
		}
		File directoryForProjectPlan = new File(path + "/plan/");
		if (!directoryForProjectPlan.exists()) {
			directoryForProjectPlan.mkdir();
		}
		File directoryForProjectPics = new File(path + "/pics/");
		if (!directoryForProjectPics.exists()) {
			directoryForProjectPics.mkdir();
		}

		if (!file.isEmpty()) {
			try {
				byte[] bytes = file.getBytes();
				if (pathType.equals("floor1") || pathType.equals("floor2") || pathType.equals("floor3") || pathType.equals("floor4") || pathType.equals("plan")) {
					stream = new BufferedOutputStream(new FileOutputStream(new File(directoryForProjectPlan + "/" + pathType + ".jpg")));
				} else {
					stream = new BufferedOutputStream(new FileOutputStream(new File(directoryForProjectPics + "/" + pathType + ".jpg")));
				}
				stream.write(bytes);
				stream.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	public boolean removeProjectPic(int projectId, int picId) {
		String path = staticResourcesPath + "/projects/" + projectId + "/pics/";
		int countOfpicsInFolder = this.filesCount(path);
		for (int i = picId + 1; i <= countOfpicsInFolder; i++) {
			if ((new File(path + i + ".jpg")).exists()) {
				Path pathFileForMove = Paths.get(path + i + ".jpg");
				try {
					File fileForDelete = new File(path + (i-1) + ".jpg");
					if (fileForDelete.exists()) {
						fileForDelete.delete();
					}
					Files.move(pathFileForMove, pathFileForMove.resolveSibling(i - 1 + ".jpg"));
				} catch (IOException e) {
					e.printStackTrace();
					return false;
				}
			}
		}
		File lastFileAfterMove = new File(path + countOfpicsInFolder + ".jpg");
		if (lastFileAfterMove.exists()) {
			lastFileAfterMove.delete();
		}
		return true;
	}

	public boolean removeAllProjectPics(int projectId) {
		String path = staticResourcesPath + "/projects/" + projectId;
		File projectPicsFolder = new File(path);
		if (projectPicsFolder.exists()) {
			try {
				FileUtils.deleteDirectory(projectPicsFolder);
			} catch (IOException e){
				e.printStackTrace();
				return false;
			}
		}
		return true;
	}

	public boolean updateProjectPicsOrderings(int projectId, List<PicOrdering> picOrders) {

		String path = staticResourcesPath + "/projects/" + projectId + "/pics/";

		if ((picOrders.get(picOrders.size() - 1).getOrdering() - picOrders.get(picOrders.size() - 1).getId()) > 1) {
			picOrders.sort(Comparator.comparingDouble(PicOrdering::getId));
		} else {
			picOrders.sort(Comparator.comparingDouble(PicOrdering::getId).reversed());
		}
		PicOrdering lastPic = picOrders.get(picOrders.size() - 1);
		try {
			Path pathFileForMove = Paths.get(path + lastPic.getId() + ".jpg");
			Files.move(pathFileForMove, pathFileForMove.resolveSibling("first.jpg"));
		} catch (IOException e) {
			e.printStackTrace();
			return false;
		}

		for (int i = 0; i <= picOrders.size() - 2; i++) {
			PicOrdering currentPic = picOrders.get(i);
			try {
				Path pathFileForMove = Paths.get(path + currentPic.getId() + ".jpg");
				Files.move(pathFileForMove, pathFileForMove.resolveSibling(currentPic.getOrdering() + ".jpg"));
			} catch (IOException e) {
				e.printStackTrace();
				return false;
			}
		}

		try {
			Path pathFileForMove = Paths.get(path + "first.jpg");
			Files.move(pathFileForMove, pathFileForMove.resolveSibling(lastPic.getOrdering() + ".jpg"));
			File fileForDelete = new File(path + "first.jpg");
			if (fileForDelete.exists()) {
				fileForDelete.delete();
			}
		} catch (IOException e) {
			e.printStackTrace();
			return false;
		}

		return true;
	}

	public boolean removeProjectPlan(int projectId, String type) {
		String path = staticResourcesPath + "/projects/" + projectId + "/plan/"+type+".jpg";
		File fileForDelete = new File(path);
		if (fileForDelete.exists()) {
			fileForDelete.delete();
		}
		return true;
	}

	public void savePictureImage(Integer pictureId, String type, MultipartFile file) {
		String path = staticResourcesPath + type;
		File directoryForPicture = new File(path);
		if (!file.isEmpty()) {
			try {
				byte[] bytes = file.getBytes();
				if (type.equals("whywe")) {
					stream = new BufferedOutputStream(new FileOutputStream(new File(directoryForPicture + "/" + pictureId + ".png")));
				} else {
					stream = new BufferedOutputStream(new FileOutputStream(new File(directoryForPicture + "/" + pictureId + ".jpg")));
				}
				stream.write(bytes);
				stream.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	public boolean removePictureImage(int pictureId, String type){
		String path = staticResourcesPath + "/" + type + "/" + pictureId;
		if (type.equals("whywe")) {
			path = path + ".png";
		} else {
			path = path + ".jpg";
		}
		File pictureFile = new File(path);
		if (pictureFile.exists()) {
			pictureFile.delete();
		}
		return true;
	}

	public void saveSlideImage(Integer slideId, MultipartFile file) {
		String path = staticResourcesPath + "slider";
		File directoryForPicture = new File(path);
		if (!file.isEmpty()) {
			try {
				byte[] bytes = file.getBytes();
				stream = new BufferedOutputStream(new FileOutputStream(new File(directoryForPicture + "/" + slideId + ".jpg")));
				stream.write(bytes);
				stream.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	public boolean removeSlideImage(int slideId){
		String path = staticResourcesPath + "slider/" + slideId + ".jpg";
		File slideFile = new File(path);
		if (slideFile.exists()) {
			slideFile.delete();
		}
		return true;
	}

	public void saveFacilityImage(MultipartFile file){
		String path = staticResourcesPath + "/facility/own-bg.jpg";
		if (!file.isEmpty()) {
			try {
				byte[] bytes = file.getBytes();
				stream = new BufferedOutputStream(new FileOutputStream(new File(path)));
				stream.write(bytes);
				stream.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	public void savePriceFile(int priceId, MultipartFile file, String language) {
		String path = staticResourcesExcelPath + language + "/price-" + priceId + ".xlsx";
		if (!file.isEmpty()) {
			try {
				byte[] bytes = file.getBytes();
				stream = new BufferedOutputStream(new FileOutputStream(new File(path)));
				stream.write(bytes);
				stream.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	public boolean removePriceFile(int priceId, String language) {
		String path = staticResourcesExcelPath + language + "/price-" + priceId + ".xlsx";
		File priceFile = new File(path);
		if (priceFile.exists()) {
			priceFile.delete();
		}
		return true;
	}

}
