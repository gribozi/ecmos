package com.jsmart.ecmos.service.technical;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

/**
 * @author Sergey Khomich
 *
 */
@Service
public class MailerService {

	@Autowired
	private JavaMailSender sender;

	@Autowired
	private TemplateEngine templateEngine;

	@Value("${mail-sender.from-email}")
	private String fromAdminConsoleEmail;

	@Async
	public void sendMessage(String to, String form, String text) {
		SimpleMailMessage message = new SimpleMailMessage();
		message.setFrom(fromAdminConsoleEmail);
		message.setTo(to);
		message.setSubject("Заявка из формы \"" + form + "\" с сайта ecmos.com.ua");
		message.setText(text);
		sender.send(message);
	}

	@Async
	public void sendMessageWithAtt(String to, String body) {
		boolean isHtml = true;
		Context context = new Context();
		context.setVariable("message", body);
		String text = templateEngine.process("mail_templates/autorespond", context);
		MimeMessage message = sender.createMimeMessage();
		MimeMessageHelper helper = new MimeMessageHelper(message);
		try {
			helper.setTo(to);
			helper.setFrom(fromAdminConsoleEmail);
			helper.setSubject("Спасибо, мы приняли вашу заявку");
			helper.setText(text, isHtml);
		} catch (MessagingException e) {
			e.printStackTrace();
		}
		sender.send(message);
	}

}

