package com.jsmart.ecmos.service.technical;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Optional;

import com.jsmart.ecmos.entity.PriceExcelContent;

import com.jsmart.ecmos.entity.PriceOption;
import com.jsmart.ecmos.entity.PriceStage;
import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.stereotype.Service;

/**
 * @author Nikolay Koretskyy
 *
 */
@Service
public class ExcelReadingService {

	private final String staticResourcesPath = FileProcessingService.class.getClassLoader().getResource("static/prices/").toString().substring(5);

	private XSSFWorkbook book;
	private XSSFSheet sheet;
	private XSSFRow row;
	private XSSFCell cell;
	private DataFormatter formatter = new DataFormatter();

	public PriceExcelContent getDataFromExcelFile(int priceId, String language) {

		PriceExcelContent priceExcelContent = new PriceExcelContent();

		String path = staticResourcesPath + "/" + language + "/price-" + priceId + ".xlsx";
		try (InputStream stream = new FileInputStream(path)) {
			book = (XSSFWorkbook) WorkbookFactory.create(stream);
			sheet = book.getSheet("Лист1");

			row = sheet.getRow(0);
			if (Optional.ofNullable(row).isPresent()) {
				cell = row.getCell(0);
				priceExcelContent.setPriceTile(cell.getStringCellValue());
			} else {
				priceExcelContent.setPriceTile("");
			}

			row = sheet.getRow(1);
			if (Optional.ofNullable(row).isPresent()) {
				cell = row.getCell(2);
				priceExcelContent.setBasePrice(cell.getNumericCellValue());
				cell = row.getCell(3);
				priceExcelContent.setComfortPrice(cell.getNumericCellValue());
				cell = row.getCell(4);
				priceExcelContent.setLuxPrice(cell.getNumericCellValue());
			} else {
				priceExcelContent.setBasePrice(0);
				priceExcelContent.setComfortPrice(0);
				priceExcelContent.setLuxPrice(0);
			}

			int rowStart = 3;
			int rowEnd = sheet.getLastRowNum();

			priceExcelContent.setPriceOptions(new ArrayList<>());
			for (int rowNum = rowStart; rowNum <= rowEnd; rowNum++) {
				row = sheet.getRow(rowNum);
				if (Optional.ofNullable(row).isPresent()) {
					if (Optional.ofNullable(row.getCell(0, Row.MissingCellPolicy.RETURN_BLANK_AS_NULL)).isPresent()) {
						PriceOption priceOption = new PriceOption();
						priceOption.setOptionTitle(formatter.formatCellValue(row.getCell(0)));
						priceOption.setPriceStages(new ArrayList<>());
						priceExcelContent.getPriceOptions().add(priceOption);
					}
					priceExcelContent.getPriceOptions().get(priceExcelContent.getPriceOptions().size() - 1).getPriceStages().add(processPriceStage(row));
				} else {
					continue;
				}
			}

		} catch (FileNotFoundException e) {
			e.printStackTrace();
			return null;
		} catch (EncryptedDocumentException e) {
			e.printStackTrace();
			return null;
		} catch (InvalidFormatException e) {
			e.printStackTrace();
			return null;
		} catch (IOException e) {
			e.printStackTrace();
			return null;
		}
		return priceExcelContent;
	}

	private PriceStage processPriceStage(Row row) {
		PriceStage priceStage = new PriceStage();
		if (Optional.ofNullable(row.getCell(1, Row.MissingCellPolicy.RETURN_BLANK_AS_NULL)).isPresent()) {
			priceStage.setStageTitle(formatter.formatCellValue(row.getCell(1)));
		}
		//TODO Check and fix this
		if (Optional.ofNullable(row.getCell(2, Row.MissingCellPolicy.RETURN_NULL_AND_BLANK)).isPresent()) {
			if (row.getCell(2).getStringCellValue().equals("+")) {
				priceStage.setInBase(true);
			} else {
				priceStage.setInBase(false);
			}
		}
		//TODO Check and fix this
		if (Optional.ofNullable(row.getCell(3, Row.MissingCellPolicy.RETURN_NULL_AND_BLANK)).isPresent()) {
			if (row.getCell(3).getStringCellValue().equals("+")) {
				priceStage.setInComfort(true);
			} else {
				priceStage.setInComfort(false);
			}
		}
		//TODO Check and fix this
		if (Optional.ofNullable(row.getCell(4, Row.MissingCellPolicy.RETURN_NULL_AND_BLANK)).isPresent()) {
			if (row.getCell(4).getStringCellValue().equals("+")) {
				priceStage.setInLux(true);
			} else {
				priceStage.setInLux(false);
			}
		}
		return priceStage;
	}

}
