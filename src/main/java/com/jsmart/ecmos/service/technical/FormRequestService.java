package com.jsmart.ecmos.service.technical;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

import com.jsmart.ecmos.entity.FormRequest;
import com.jsmart.ecmos.repository.FormRequestRepository;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

/**
 * @author Sergey Khomich
 *
 */
@Service
public class FormRequestService {

	@Autowired
	private MailerService mailerService;

	@Autowired
	private FormRequestRepository formRequestRepository;

	@Value("${mail-sender.to-email}")
	private String toAdminConsoleEmail;

	private static final Logger log = Logger.getLogger(FormRequestService.class);

	public void processingRequest(String form, String name, String phone, String email, String text){
		log.info("Saving formRequest from user to database");
		formRequestRepository.save(new FormRequest(LocalDate.now(), form, name, phone, email, text));
		log.info("Sending a message from site");
		mailerService.sendMessage(toAdminConsoleEmail, form, "Request from " + email + "    " + text);
		if (!"".equals(email)) {
			log.info("Sending an autorespond message to user");
			mailerService.sendMessageWithAtt(email, "Ecmos group");
		}
	}

	public List<FormRequest> getForAdmin() {
		log.info("Fetching all form requests");
		return formRequestRepository.findAll();
	}

	public boolean remove(int id) {
		log.info("Fetching formRequest with id " + id);
		FormRequest formRequest = formRequestRepository.getOne(id);
		if (Optional.ofNullable(formRequest).isPresent()) {
			formRequestRepository.deleteById(id);
			log.info("formRequest removed");
			return true;
		} else {
			log.info("Unable to delete. FormRequest with id " + id + " not found");
			return false;
		}
	}
}
