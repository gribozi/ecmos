package com.jsmart.ecmos.service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import com.jsmart.ecmos.entity.Contact;
import com.jsmart.ecmos.entity.Page;
import com.jsmart.ecmos.entity.PageContent;
import com.jsmart.ecmos.repository.LanguageRepository;
import com.jsmart.ecmos.repository.PageRepository;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @author Nikolay Koretskyy
 *
 */
@Service
public class PageService {

	@Data
	@AllArgsConstructor
	private class SiteMenuItem {
		private String name;
		private String uri;
	}

	@Data
	@EqualsAndHashCode(callSuper=false)
	private class PageWithContacts extends Page{
		private String phone;
		private String emailTo;
		private String emailFrom;
		private String smFb;
		private String smVk;
		private String smYt;
		private String smIn;

		public PageWithContacts(Page page, Contact contact) {
			this.phone = contact.getPhone();
			this.emailTo = contact.getEmailTo();
			this.smFb = contact.getSmFb();
			this.smVk = contact.getSmVk();
			this.smYt = contact.getSmYt();
			this.smIn = contact.getSmIn();
			this.setId(page.getId());
			this.setUri(page.getUri());
			this.setActive(page.isActive());
			this.setPageContents(page.getPageContents());
		}
	}

	@Autowired
	private PageRepository pageRepository;

	@Autowired
	LanguageRepository languageRepository;

	@Autowired
	private ContactService contactService;

	private static final Logger log = Logger.getLogger(PageService.class);

	public PageWithContacts getOne(String uri, String language) {
		log.info("Fetching page with uri " + uri);
		Page page = pageRepository.findByUri(uri);
		if (Optional.ofNullable(page).isPresent()) {
			List<PageContent> pageContents = page.getPageContents()
					.stream()
					.filter((y) -> y.getLanguage().getName().equals(language))
					.collect(Collectors.toList());
			page.setPageContents(pageContents);
			return new PageWithContacts(page, contactService.getOne(language));
		} else {
			log.info("Page with uri " + uri + " was not found");
			return null;
		}
	}

	public List<SiteMenuItem> getSiteMenu(String language) {
		log.info("Fetching active pages");
		List<SiteMenuItem> siteMenuItems = pageRepository.findByActiveOrderByOrderingAsc(true)
				.stream()
				.map((x) -> {
					PageContent pageContent = x.getPageContents()
							.stream()
							.filter((y) -> y.getLanguage().getName().equals(language))
							.findFirst()
							.get();
					return new SiteMenuItem(pageContent.getName(), x.getUri());
				})
				.collect(Collectors.toList());
		return siteMenuItems;
	}

}
