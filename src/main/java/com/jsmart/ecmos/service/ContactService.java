package com.jsmart.ecmos.service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import com.jsmart.ecmos.entity.Contact;
import com.jsmart.ecmos.entity.ContactContent;
import com.jsmart.ecmos.repository.ContactRepository;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author Sergey Khomich
 *
 */
@Service
public class ContactService {
	
	@Autowired
	private ContactRepository contactRepository;

	private static final Logger log = Logger.getLogger(ContactService.class);

	public Contact getOne(String language) {
		log.info("Fetching contacts");
		Contact contact = contactRepository.getOne(1);
		if (Optional.ofNullable(contact).isPresent()) {
			List<ContactContent> contactContents = contact.getContactContents()
					.stream()
					.filter((y) -> y.getLanguage().getName().equals(language))
					.collect(Collectors.toList());
			contact.setContactContents(contactContents);
			return contact;
		} else {
			log.info("Contacts was not found");
			return null;
		}
	}

	public Contact update(Contact contact, String language) {
		Contact contactOld = this.getOne(language);
		if (Optional.ofNullable(contactOld).isPresent()) {
			log.info("Updating contacts");
			contact.setId(contactOld.getId());
			contact.setActive(contactOld.isActive());
			contact.setOrdering(contactOld.getOrdering());
			contact.getContactContents().get(0).setContact(contact);
			contact.getContactContents().get(0).setId(contactOld.getContactContents().get(0).getId());
			contact.getContactContents().get(0).setLanguage(contactOld.getContactContents().get(0).getLanguage());
			Contact updatedContact = contactRepository.save(contact);
			log.info("Contacts was updated");
			return updatedContact;
		} else {
			log.info("Contacts was not found");
			return null;
		}
	}

}
