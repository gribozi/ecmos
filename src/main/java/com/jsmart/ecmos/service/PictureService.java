package com.jsmart.ecmos.service;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import com.jsmart.ecmos.entity.ForTeamText;
import com.jsmart.ecmos.entity.ForTeamTextContent;
import com.jsmart.ecmos.entity.Picture;
import com.jsmart.ecmos.entity.PictureContent;
import com.jsmart.ecmos.repository.ForTeamTextRepository;
import com.jsmart.ecmos.repository.LanguageRepository;
import com.jsmart.ecmos.repository.PictureRepository;
import com.jsmart.ecmos.service.technical.FileProcessingService;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author Nikolay Koretskyy
 *
 */
@Service
public class PictureService {

	@Autowired
	private PictureRepository pictureRepository;

	@Autowired
	private ForTeamTextRepository forTeamTextRepository;

	@Autowired
	private LanguageRepository languageRepository;

	@Autowired
	private FileProcessingService fileProcessingService;

	private static final Logger log = Logger.getLogger(PictureService.class);

	public Picture getOne(int id, String language) {
		log.info("Fetching picture with id " + id);
		Picture picture = pictureRepository.getOne(id);
		if (Optional.ofNullable(picture).isPresent()) {
			List<PictureContent> pictureContents = picture.getPictureContents()
					.stream()
					.filter((y) -> y.getLanguage().getName().equals(language))
					.collect(Collectors.toList());
			picture.setPictureContents(pictureContents);
			return picture;
		} else {
			log.info("Picture with id " + id + " was not found");
			return null;
		}
	}

	public List<Picture> getByType(String type, String language) {
		log.info("Fetching active pictures with type " + type);
		List<Picture> pictures = pictureRepository.findByTypeAndActiveOrderByOrderingAsc(type, true);
		pictures.forEach((x) -> {
			List<PictureContent> picturesContents = x.getPictureContents()
					.stream()
					.filter((y) -> y.getLanguage().getName().equals(language))
					.collect(Collectors.toList());
			x.setPictureContents(picturesContents);
		});
		return pictures;
	}

	public List<Picture> getByTypeForAdmin(String type, String language) {
		log.info("Fetching pictures with type " + type);
		List<Picture> pictures = pictureRepository.findByTypeOrderByOrderingAsc(type);
		pictures.forEach((x) -> {
			List<PictureContent> picturesContents = x.getPictureContents()
					.stream()
					.filter((y) -> y.getLanguage().getName().equals(language))
					.collect(Collectors.toList());
			x.setPictureContents(picturesContents);
		});
		return pictures;
	}

	public List<Picture> updateOrderings(List<Picture> pictures, String language) {
		log.info("Updating orderings for pictures");
		return pictures
				.stream()
				.map((x) -> {
					Picture picture = pictureRepository.getOne(x.getId());
					picture.setOrdering(x.getOrdering());
					return pictureRepository.save(picture);
				})
				.collect(Collectors.toList());
	}

	public int getNextOrdering(){
		List<Picture> piсtures =pictureRepository.findAll();
		Optional<Picture> maxOrderPicture = piсtures.stream().max(Comparator.comparing(Picture::getOrdering));
		if (maxOrderPicture.isPresent()) {
			return maxOrderPicture.get().getOrdering() + 1;
		} else {
			return 1;
		}
	}

	public Picture create (Picture picture, String type, String language) {

		log.info("Creating picture " + picture.getPictureContents().get(0).getPictureTitle());

		picture.setType(type);

		picture.getPictureContents().get(0).setLanguage(languageRepository.findByName(language));

		if ((!Optional.ofNullable(picture.getOrdering()).isPresent()) || (picture.getOrdering() < 1)) {
			picture.setOrdering(this.getNextOrdering());
		}

		PictureContent pictureContentForSave = picture.getPictureContents().get(0);
		picture.setPictureContents(null);
		Picture savedPicture = pictureRepository.save(picture);
		List<PictureContent> pictureContents = new ArrayList<>();
		pictureContentForSave.setPicture(savedPicture);
		pictureContents.add(pictureContentForSave);
		savedPicture.setPictureContents(pictureContents);
		savedPicture = pictureRepository.save(savedPicture);

		log.info("Picture " + savedPicture.getPictureContents().get(0).getPictureTitle() + " was created");
		return savedPicture;
	}

	public Picture update(Picture picture, String type, String language) {

		log.info("Updating picture " + picture.getPictureContents().get(0).getPictureTitle());

		Picture pictureOld = this.getOne(picture.getId(), language);

		if (Optional.ofNullable(pictureOld).isPresent()) {

			picture.setType(pictureOld.getType());

			picture.getPictureContents().get(0).setLanguage(languageRepository.findByName(language));

			if (pictureOld.getPictureContents().size() > 0) {
				picture.getPictureContents().get(0).setId(pictureOld.getPictureContents().get(0).getId());
			}

			picture.getPictureContents().get(0).setPicture(picture);
			Picture pictureUpdated = pictureRepository.save(picture);
			log.info("Picture " + pictureUpdated.getPictureContents().get(0).getPictureTitle() + " was updated");
			return pictureUpdated;

		} else {
			log.info("Picture " + picture.getPictureContents().get(0).getPictureTitle() + " was not found");
			return null;
		}
	}

	public boolean remove(int id, String type) {
		log.info("Fetching picture with id " + id);
		Picture picture = pictureRepository.getOne(id);
		if (Optional.ofNullable(picture).isPresent()) {
			pictureRepository.deleteById(id);
			fileProcessingService.removePictureImage(id, type);
			log.info("Picture removed");
			return true;
		} else {
			log.info("Unable to delete. Picture with id " + id + " not found");
			return false;
		}
	}

	public ForTeamText getOneTeamText(String language) {
		log.info("Fetching forTeamText");
		ForTeamText forTeamText = forTeamTextRepository.getOne(1);
		if (Optional.ofNullable(forTeamText).isPresent()) {
			List<ForTeamTextContent> forTeamTextContents = forTeamText.getForTeamTextContents()
					.stream()
					.filter((y) -> y.getLanguage().getName().equals(language))
					.collect(Collectors.toList());
			forTeamText.setForTeamTextContents(forTeamTextContents);
			return forTeamText;
		} else {
			log.info("ForTeamText was not found");
			return null;
		}
	}

	public ForTeamText updateTeamText(ForTeamText forTeamText, String language) {
		ForTeamText forTeamTextOld = this.getOneTeamText(language);
		if (Optional.ofNullable(forTeamTextOld).isPresent()) {
			log.info("Updating forTeamTexts");
			forTeamText.setId(forTeamTextOld.getId());
			forTeamText.setActive(forTeamTextOld.isActive());
			forTeamText.setOrdering(forTeamTextOld.getOrdering());
			forTeamText.getForTeamTextContents().get(0).setForTeamText(forTeamText);
			forTeamText.getForTeamTextContents().get(0).setId(forTeamTextOld.getForTeamTextContents().get(0).getId());
			forTeamText.getForTeamTextContents().get(0).setLanguage(forTeamTextOld.getForTeamTextContents().get(0).getLanguage());
			ForTeamText updatedForTeamText = forTeamTextRepository.save(forTeamText);
			log.info("ForTeamTexts was updated");
			return updatedForTeamText;
		} else {
			log.info("ForTeamTexts was not found");
			return null;
		}
	}
}
