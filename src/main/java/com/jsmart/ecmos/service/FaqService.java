package com.jsmart.ecmos.service;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import com.jsmart.ecmos.entity.Faq;
import com.jsmart.ecmos.entity.FaqContent;
import com.jsmart.ecmos.repository.FaqRepository;
import com.jsmart.ecmos.repository.LanguageRepository;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author Nikolay Koretskyy
 *
 */
@Service
public class FaqService {

	@Autowired
	private FaqRepository faqRepository;

	@Autowired
	private LanguageRepository languageRepository;

	private static final Logger log = Logger.getLogger(FaqService.class);

	public Faq getOne(int id, String language) {
		log.info("Fetching faq with id " + id);
		Faq faq = faqRepository.getOne(id);
		if (Optional.ofNullable(faq).isPresent()) {
			List<FaqContent> faqContents = faq.getFaqContents()
					.stream()
					.filter((y) -> y.getLanguage().getName().equals(language))
					.collect(Collectors.toList());
			faq.setFaqContents(faqContents);
			return faq;
		} else {
			log.info("Faq with id " + id + " was not found");
			return null;
		}
	}

	public List<Faq> getAll(String language) {
		log.info("Fetching active faqs");
		List<Faq> faqs = faqRepository.findByActiveOrderByOrderingAsc(true);
		faqs.forEach((x) -> {
			List<FaqContent> faqContents = x.getFaqContents()
					.stream()
					.filter((y) -> y.getLanguage().getName().equals(language))
					.collect(Collectors.toList());
			x.setFaqContents(faqContents);
		});
		return faqs;
	}

	public List<Faq> getForAdmin(String language) {
		log.info("Fetching all faqs");
		List<Faq> faqs = faqRepository.findByOrderByOrderingAsc();
		faqs.forEach((x) -> {
			List<FaqContent> picturesContents = x.getFaqContents()
					.stream()
					.filter((y) -> y.getLanguage().getName().equals(language))
					.collect(Collectors.toList());
			x.setFaqContents(picturesContents);
		});
		return faqs;
	}

	public List<Faq> updateOrderings(List<Faq> faqs, String language) {
		log.info("Updating orderings for faqs");
		return faqs
				.stream()
				.map((x) -> {
					Faq faq = faqRepository.getOne(x.getId());
					faq.setOrdering(x.getOrdering());
					return faqRepository.save(faq);
				})
				.collect(Collectors.toList());
	}

	public int getNextOrdering() {
		List<Faq> faqs = faqRepository.findAll();
		Optional<Faq> maxOrderFaq = faqs.stream().max(Comparator.comparing(Faq::getOrdering));
		if (maxOrderFaq.isPresent()) {
			return maxOrderFaq.get().getOrdering() + 1;
		} else {
			return 1;
		}
	}

	public Faq create (Faq faq, String language) {

		log.info("Creating faq " + faq.getFaqContents().get(0).getQuestion());

		faq.getFaqContents().get(0).setLanguage(languageRepository.findByName(language));

		faq.setOrdering(this.getNextOrdering());

		FaqContent faqContentForSave = faq.getFaqContents().get(0);
		faq.setFaqContents(null);
		Faq savedFaq = faqRepository.save(faq);
		List<FaqContent> faqContents = new ArrayList<>();
		faqContentForSave.setFaq(savedFaq);
		faqContents.add(faqContentForSave);
		savedFaq.setFaqContents(faqContents);
		savedFaq = faqRepository.save(savedFaq);

		log.info("Faq " + savedFaq.getFaqContents().get(0).getQuestion() + " was created");
		return savedFaq;
	}

	public Faq update(Faq faq, String language) {

		log.info("Updating faq " + faq.getFaqContents().get(0).getQuestion());

		Faq faqOld = this.getOne(faq.getId(), language);

		if (Optional.ofNullable(faqOld).isPresent()) {

			faq.getFaqContents().get(0).setLanguage(languageRepository.findByName(language));

			faq.getFaqContents().get(0).setId(faqOld.getFaqContents().get(0).getId());

			faq.getFaqContents().get(0).setFaq(faq);
			Faq faqUpdated = faqRepository.save(faq);
			log.info("Faq " + faqUpdated.getFaqContents().get(0).getQuestion() + " was updated");
			return faqUpdated;

		} else {
			log.info("Faq " + faq.getFaqContents().get(0).getQuestion() + " was not found");
			return null;
		}
	}

	public boolean remove(int id) {
		log.info("Fetching faq with id " + id);
		Faq faq = faqRepository.getOne(id);
		if (Optional.ofNullable(faq).isPresent()) {
			faqRepository.deleteById(id);
			log.info("Faq removed");
			return true;
		} else {
			log.info("Unable to delete. Faq with id " + id + " not found");
			return false;
		}
	}

}
